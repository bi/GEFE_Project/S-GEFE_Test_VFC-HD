# VFC-HD_Base
This is a template project for developments with the VFC-HD.

Tha application part of this project contains the instantiation of few example status and control registers, an instantiation of the logic required for the GBT-FPGA implementation in the VFC-HD, and an implementation of the DDR3 controller.


## Creating your on project starting from this template
The first step to create a new project, in this example named NewProject, based on this template is to clone this git and initialize its submodules. This is done executing the following command:

```git clone -–recursive https://gitlab.cern.ch/bi/VFC-HD_Base NewProject```

The directory NewProject is created into your file system. The next step is to link it to your desired remote, that here we assume to be a freshly created repo in the bi group called NewProject.

```git remote set-url origin https://gitlab.cern.ch/bi/NewProject```


Now you can push into the new git to initialize it and start working:

```git push```

## Notes
This procedure unlinked your project from the base one but kept its history.
The submodules used as library, the VFC-HD_System and BI_HDL_CORES, are still kept as submodules and can be updated to the latest version at any moment.

