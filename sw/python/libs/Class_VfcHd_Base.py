## Parent class import:
import sys
sys.path.append('../../libs/VFC-HD_System/sw/python/')
from Class_VfcHd_System import *

## Additional Python modules:
import re

## Child class declaration:
class VfcHd_Base(VfcHd_System):

    '############################################################################################################'
    '#### Note!! This is a CHILD class of the VfcHd_System class.                                            ####'
    '####        All attributes and methods related to the Application module of the VFC-HD firmware must be ####'
    '####        declared here.                                                                              ####'
    '############################################################################################################'

    DriverRegisters = { # '<register_name>': <register_address> (the address is the offset in the application space as it would be in the CCDB)
        # DDR3 memory
        'app_ddr3_page_selector': 0x4,
        'app_ddr3_master': 0x100000,
        'app_ddr3_slave': 0x200000
    };

    def __init__(self, Lun, IntLevel = 3, IntVector = 0xA0, RORA="ROAK", Timeout=1000, Verbose=False):
        VfcHd_System.__init__(self, "vfc_hd_base", Lun, IntLevel, IntVector, RORA, Timeout, Verbose)

    #=====================#
    # virtual CCDB driver #
    #=====================#

    def Read(self, RegName, Offset=0):
        if RegName in self.DriverRegisters:
            Offset += self.DriverRegisters[RegName]/4
            RegName = "AppMemSpace"
        return VfcHd_System.Read(self, RegName, Offset)

    def Write(self, RegName, DataLw, Offset=0):
        if RegName in self.DriverRegisters:
            Offset += self.DriverRegisters[RegName]/4
            RegName = "AppMemSpace"
        return VfcHd_System.Write(self, RegName, DataLw, Offset)

    def ReadMem(self, RegName, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        if RegName in self.DriverRegisters:
            Offset += self.DriverRegisters[RegName]/4
            RegName = "AppMemSpace"
        return VfcHd_System.ReadMem(self, RegName, NbLongWords, Offset, Dma, Time)


    #============================#
    # GBT Reference Clock Scheme #
    #============================#

    # PllRef (Si5338) I2C access:
    def ReadPllRefStat(self):
        return VfcHd_System.Read(self, "GbtRefClkSchCtrl", 0)

    def WritePllRefCtrl(self, DataLw):
        return VfcHd_System.Write(self, "GbtRefClkSchCtrl", DataLw, 1)

    def ReadPllRefCtrl(self):
        return VfcHd_System.Read(self, "GbtRefClkSchCtrl", 1)

    def I2cPllRefStartBit(self):
        TimeOut = 0
        self.WritePllRefCtrl(0x01000000)
        while (self.ReadPllRefStat() & 0xff000000):
            TimeOut = TimeOut + 1
            if TimeOut == 0xFF:
                return -1

    def I2cPllRefSelectSlave(self, ReadWrite):
        if (ReadWrite == 'read'):
            ReadWriteBit = 1
        elif  (ReadWrite == 'write'):
            ReadWriteBit = 0
        else :
            print  "Only the options 'read' and 'write' are valid"
            return "ERROR"
        Data = 0x02000000 + (0x70<<1) + ReadWriteBit
        self.WritePllRefCtrl(Data)
        while (self.ReadPllRefStat() & 0xff000000):
            pass
        if (self.ReadPllRefStat() & 0x100):
            print "No module acknowledge the I2C transaction sending the slave address"
            return 1
        else:
            return 0

    def I2cPllRefWriteByte(self, DataByte):
        self.WritePllRefCtrl(0x02000000+DataByte) # Sending the byte address
        while (self.ReadPllRefStat() & 0xff000000):
            pass
        if (self.ReadPllRefStat() & 0x100):
            print "No module acknowledge the I2C transaction on the byte address"
            return 1
        else:
            return 0

    def I2cPllRefReadByte(self, Last=0):
        if (Last==1 or Last==0):
            self.WritePllRefCtrl(0x03000000+Last)
            while (self.ReadPllRefStat() & 0xff000000):
                pass
        else:
            print  "Only 0 and 1 are valid options"
            return "ERROR"
        Data = self.ReadPllRefStat() & 0xff
        return Data

    def I2cPllRefStopBit(self):
        self.WritePllRefCtrl(0x04000000)
        while (self.ReadPllRefStat() & 0xff000000):
            pass

    def I2cPllRefWrite(self, RegAddress, DataByte):
        StartBit = self.I2cPllRefStartBit()
        if StartBit == -1:
            print "I2C start bit time-out..."
            return
        self.I2cPllRefSelectSlave('write')
        self.I2cPllRefWriteByte(RegAddress)
        self.I2cPllRefWriteByte(DataByte)
        self.I2cPllRefStopBit()

    def I2cPllRefRead(self, RegAddress):
        StartBit = self.I2cPllRefStartBit()
        if StartBit == -1:
            print "I2C start bit time-out..."
            return
        self.I2cPllRefSelectSlave('write')
        self.I2cPllRefWriteByte(RegAddress)
        self.I2cPllRefStopBit()
        self.I2cPllRefStartBit()
        self.I2cPllRefSelectSlave('read')
        DataByte = self.I2cPllRefReadByte(1)
        self.I2cPllRefStopBit()
        return DataByte

    def I2cPllRefRdModWr(self, RegAddress, DataByte, Mask=0xFF, Verbose=False):
        if Mask == 0x00:
            if Verbose == True:
                print "-> - Register %d skipped" % RegAddress
            else:
                pass
        else:
            if Mask == 0xFF:
                NewDataByte        = DataByte
                if Verbose == True:
                    print "-> - Register %d fully written" % RegAddress
            else:
                PrevDataByte       = self.I2cPllRefRead(RegAddress)
                PrevDataByteMasked = PrevDataByte & ~Mask
                DataByteMasked     = DataByte     &  Mask
                NewDataByte        = PrevDataByteMasked + DataByteMasked
                if Verbose == True:
                    print "-> - Register %d RdModWr:" % RegAddress
                    print "-> - + Data         :", HexN(DataByte)[6:8]
                    print "-> - + Mask         :", HexN(Mask)[6:8]
                    print "-> - + Previous data:", HexN(PrevDataByte)[6:8]
                    print "-> - + New data     :", HexN(NewDataByte)[6:8]
            self.I2cPllRefWrite(RegAddress, NewDataByte)

    # PllRef (Si5338) setting:
    def PllRefSetting(self, RegConfigFile, Verbose):
        # Note!! A registers configuration file generated using "ClockBuilder Desktop"
        #        (C/C++ header file) must be provided.
        Eof         = 0
        RegAddrBuff = []
        RegDataBuff = []
        MaskBuff    = []
        LossOfLock  = 0xFF
        TimeOut     = 0
        print "-> Setting VFC-HD PllRef (Si5330)..."
        if (Verbose == True):
            print "->"
            print "-> Disabling all outputs (Reg230 = 0x1F)..."
        self.I2cPllRefRdModWr(230, 0x1F, 0xFF, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 230
        RegDataRead = HexN(self.I2cPllRefRead(230))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (230, RegDataRead)
            print "->"
            print "-> Pausing LOL (Reg241 = 0xE5)..."
        self.I2cPllRefRdModWr(241, 0xE5, 0xFF, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 241
        RegDataRead = HexN(self.I2cPllRefRead(241))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (241, RegDataRead)
            print "->"
        print "-> Selected header file with configuration registers values:", RegConfigFile
        if (Verbose == True):
            print "-> Parsing header file content..."
        RegsConfigFile = open(RegConfigFile, 'r')
        if (Verbose == True):
            print "-> Obtaining: Register Address, Data and Mask iteratively..."
        while Eof == 0:
            RegsConfigLine = RegsConfigFile.readline()
            if RegsConfigLine[0:13] == "//End of file":
                Eof = 1
                if (Verbose == True):
                    print "-> End Of File reached"
                RegsConfigFile.close()
            elif RegsConfigLine[0] == "{":
                RegsConfig = re.search("([a-z 0-9]*),([a-z 0-9]*),([a-z 0-9]*)", RegsConfigLine, re.I)
                RegAddr    = int(RegsConfig.group(1), 10)
                RegData    = int(RegsConfig.group(2), 16)
                Mask       = int(RegsConfig.group(3), 16)
                RegAddrBuff.append(RegAddr)
                RegDataBuff.append(RegData)
                MaskBuff.append(Mask)
            else:
                pass
        if (Verbose == True):
            print "->"
            print "-> Writing configuration registers iteratively..."
        for i in range(0,len(RegAddrBuff)):
            if (Verbose == True):
                print "-> -"
                print "-> - Writing register %d: Data = %s | Mask = %s" % (RegAddrBuff[i], HexN(RegDataBuff[i])[6:8], HexN(MaskBuff[i])[6:8])
            self.I2cPllRefRdModWr(RegAddrBuff[i], RegDataBuff[i], MaskBuff[i], Verbose)
            if (Verbose == True):
                print "-> - Reading register %d..." % RegAddrBuff[i]
            RegDataRead = HexN(self.I2cPllRefRead(RegAddrBuff[i]))[6:8]
            if (Verbose == True):
                print "-> - Content of register %d:     %s" % (RegAddrBuff[i], RegDataRead)
        if (Verbose == True):
            print "->"
            print "-> Validating input clock status (Reg218[2] = 0)..."
        while ((LossOfLock != 0x00) and (TimeOut != 0xFF)):
            RegDataRead = self.I2cPllRefRead(218)
            LossOfLock  = (RegDataRead & 0x04) # Comment: Checking LOS_CLKIN
            TimeOut     = TimeOut + 1
            if TimeOut == 0xFF:
                LossOfLockStatus = "-> - Loss Of Lock Time Out!!!"
            else:
                LossOfLockStatus = "-> - Input clock present"
        if (Verbose == True):
            print LossOfLockStatus
            print "->"
            print "-> Configuring PLL for locking (Reg49[7] = 0)..."
        self.I2cPllRefRdModWr(49, 0x00, 0x80, Verbose)
        self.I2cPllRefWrite(49, RegDataRead)
        if (Verbose == True):
            print "-> - Reading register %d..." % 49
        RegDataRead = HexN(self.I2cPllRefRead(49))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (49, RegDataRead)
            print "->"
            print "-> Asserting soft reset (Reg246[1] = 1)..."
        self.I2cPllRefRdModWr(246, 0x02, 0xFF, Verbose)
        if (Verbose == True):
            print "-> - Waiting 25ms for Pll Ref (Si5338) to soft reset..."
        sleep(0.25)
        if (Verbose == True):
            print "->"
            print "-> Restarting Loss Of Lock (Reg49[7] = 0)..."
        self.I2cPllRefRdModWr(241, 0x65, 0xFF, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 241
        RegDataRead = HexN(self.I2cPllRefRead(241))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (RegAddrBuff[i], RegDataRead)
            print "->"
            print "-> Confirming PLL lock status (Reg218[2] = 0)..."
        LossOfLock = 0xFF
        while ((LossOfLock != 0x00) and (TimeOut != 0xFF)):
            RegDataRead = self.I2cPllRefRead(218)
            LossOfLock  = (RegDataRead & 0x15) # Comment: Checking SYS_CAL, LOS_CLKIN and PLL_LOL
            TimeOut     = TimeOut + 1
            if TimeOut == 0xFF:
                LossOfLockStatus = "-> - PLL lock Time Out!!!"
            else:
                LossOfLockStatus = "-> - PLL locked"
        if (Verbose == True):
            print LossOfLockStatus
            print "->"
            print "-> Copying FCAL values to active registers..."
            print "-> -"
            print "-> - Copying from Reg235 to Reg45..."
        RegNewData = self.I2cPllRefRead(235)
        self.I2cPllRefWrite(45, RegNewData)
        if (Verbose == True):
            print "-> - Reading register %d..." % 45
        RegDataRead = HexN(self.I2cPllRefRead(45))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (45, RegDataRead)
            print "-> -"
            print "-> - Copying from Reg236 to Reg46..."
        RegNewData = self.I2cPllRefRead(236)
        self.I2cPllRefWrite(46, RegNewData)
        if (Verbose == True):
            print "-> - Reading register %d..." % 46
        RegDataRead = HexN(self.I2cPllRefRead(46))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (46, RegDataRead)
            print "-> -"
            print "-> - Copying Reg237[1:0] to Reg47[1:0]..."
        RegSource  = self.I2cPllRefRead(237) & 0x03
        RegDest    = self.I2cPllRefRead( 47) & 0xFC
        RegNewData = RegSource + RegDest
        self.I2cPllRefWrite(47, RegNewData)
        if (Verbose == True):
            print "-> - Reading register %d..." % 47
        RegDataRead = HexN(self.I2cPllRefRead(47))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (47, RegDataRead)
            print "-> -"
            print "-> - Setting Reg47[7:2] 000101b..."
        self.I2cPllRefRdModWr(47, 0x14, 0xFC, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 47
        RegDataRead = HexN(self.I2cPllRefRead(47))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (47, RegDataRead)
            print "-> "
            print "-> Setting PLL to use FCAL values (Reg49[7] = 1)..."
        self.I2cPllRefRdModWr(49, 0x80, 0x80, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 49
        RegDataRead = HexN(self.I2cPllRefRead(49))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (49, RegDataRead)
            print "->"
            print "-> Note!! Down-Spread feature not used. Skipping this setting step..."
            print "->"
            print "-> Enabling output 0 (Reg230 = 0x0E)..."
        self.I2cPllRefRdModWr(230, 0x0E, 0xFF, Verbose)
        if (Verbose == True):
            print "-> - Reading register %d..." % 230
        RegDataRead = HexN(self.I2cPllRefRead(230))[6:8]
        if (Verbose == True):
            print "-> - Content of register %d: %s" % (230, RegDataRead)
            print "->"
        print "-> VFC-HD PllRef (Si5338) setting done."

    # GBT reference clock control:
    def GbtClkSchRefClkSel(self, RefClock):
        if (RefClock == "BST"):
            return VfcHd_System.RdModWr(self, "GbtRefClkSchCtrl", 0x1, 0x1, 2)
        elif (RefClock == "FreeRunning"):
            return VfcHd_System.RdModWr(self, "GbtRefClkSchCtrl", 0x0, 0x1, 2)
        else:
            print  "RefClk must be set to \"BST\" or \"FreeRunning\""
            return "ERROR"

    def GbtClkSchSyncTxFrameClkPll(self):
        return VfcHd_System.RdModWr(self, "GbtRefClkSchCtrl", 0x2, 0x2, 2)

    # GBT reference clock status:
    def ReadBstSfpPresent(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**0)  >> 0

    def ReadBstSfpTxFault(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**1)  >> 1

    def ReadBstSfpLos(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**2)  >> 2

    def ReadBstSfpStatusTxDisable(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**3)  >> 3

    def ReadBstSfpStatusRateSelect(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**4)  >> 4

    def ReadBstOn(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**5)  >> 5

    def ReadGbtClkSchTxFrameClkPllLock(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**8)  >> 8

    def ReadGbtClkSchFreeRunningPllLock(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**9)  >> 9

    def ReadGbtClkSchBstAsRefClk(self):
        return (VfcHd_System.Read(self, "GbtRefClkSchCtrl", 3) & 2**10) >> 10

    #============================#
    # GBT Bank (Application SFP) #
    #============================#

    # GBT Bank ID:
    def ReadAppSfpGbtBankId(self):
        GbtBankId = VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & 0x1
        if (GbtBankId == 0):
            return "Application SFP GBT Bank"
        else:
            return "FMC GBT Bank"

    # Application SFPs control:
    def AppSfpTxDisable(self, AppSfp, Tx):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            if (Tx == "On"):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", 0*(2**(AppSfp-1)), 2**(AppSfp-1), 0)
            elif (Tx == "Off"):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", 1*(2**(AppSfp-1)), 2**(AppSfp-1), 0)
            else:
                print  "Tx must be set to \"On\" or \"Off\""
                return "ERROR"
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpStatusTxDisable(self, AppSfp):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 0) & (2**((AppSfp-1)+12))) >> ((AppSfp-1)+12)
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpRateSelect(self, AppSfp, Rate):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            if ((Rate == 0) or (Rate == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", Rate*(2**((AppSfp-1)+4)), 2**((AppSfp-1)+4), 0)
            else:
                print  "Rate must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpStatusRateSelect(self, AppSfp):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 0) & (2**((AppSfp-1)+16))) >> ((AppSfp-1)+16)
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpPresent(self, AppSfp):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 0) & (2**(AppSfp-1))) >> (AppSfp-1)
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpTxFault(self, AppSfp):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 0) & (2**((AppSfp-1)+4))) >> ((AppSfp-1)+4)
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpLos(self, AppSfp):
        if ((1 <= AppSfp) and (AppSfp <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 0) & (2**((AppSfp-1)+8))) >> ((AppSfp-1)+8)
        else:
            print  "AppSfp must be set from \"1\" to \"4\""
            return "ERROR"

    # GBT Bank control:
    def AppSfpGbtReset(self, Reset):
        if ((Reset == 0) or (Reset == 1)):
            return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", Reset, 0x1, 1)
        else:
            print  "Reset must be set to \"0\" or \"1\""
            return "ERROR"

    def ReadAppSfpGbtReset(self):
        return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & 0x1)

    def AppSfpGbtTxReset(self, GbtLink, Reset):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Reset == 0) or (Reset == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", Reset*(2**((GbtLink-1)+4)), 2**((GbtLink-1)+4), 1)
            else:
                print  "Reset must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtTxReset(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+4))) >> ((GbtLink-1)+4)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtRxReset(self, GbtLink, Reset):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Reset == 0) or (Reset == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", Reset*(2**((GbtLink-1)+8)), 2**((GbtLink-1)+8), 1)
            else:
                print  "Reset must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtRxReset(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+8))) >> ((GbtLink-1)+8)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtTxPolInv(self, GbtLink, PolInv):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((PolInv == 0) or (PolInv == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", PolInv*(2**((GbtLink-1)+12)), 2**((GbtLink-1)+12), 1)
            else:
                print  "PolInv must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtTxPolInv(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+12))) >> ((GbtLink-1)+12)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtRxPolInv(self, GbtLink, PolInv):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((PolInv == 0) or (PolInv == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", PolInv*(2**((GbtLink-1)+16)), 2**((GbtLink-1)+16), 1)
            else:
                print  "PolInv must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtRxPolInv(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+16))) >> ((GbtLink-1)+16)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtLoopBackEn(self, GbtLink, LoopBackEn):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((LoopBackEn == 0) or (LoopBackEn == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", LoopBackEn*(2**((GbtLink-1)+20)), 2**((GbtLink-1)+20), 1)
            else:
                print  "LoopBackEn must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtLoopBackEn(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+20))) >> ((GbtLink-1)+20)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtTxWordClkMonEn(self, TxWordClkMonEn):
        if ((TxWordClkMonEn == 0) or (TxWordClkMonEn == 1)):
            return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", TxWordClkMonEn, 2**24, 1)
        else:
            print  "TxWordClkMonEn must be set to \"0\" or \"1\""
            return "ERROR"

    def ReadAppSfpGbtTxWordClkMonEn(self):
        return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**24))

    def AppSfpGbtTxIsDataSel(self, GbtLink, TxIsDataSel):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((TxIsDataSel == 0) or (TxIsDataSel == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtCtrlReg", TxIsDataSel*(2**((GbtLink-1)+28)), 2**((GbtLink-1)+28), 1)
            else:
                print  "TxIsDataSel must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtTxIsDataSel(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtCtrlReg", 1) & (2**((GbtLink-1)+28))) >> ((GbtLink-1)+28)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtMgtTxReady(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & (2**((GbtLink-1)+4))) >> ((GbtLink-1)+4)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtMgtRxReady(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & (2**((GbtLink-1)+8))) >> ((GbtLink-1)+8)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtTxReady(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & (2**((GbtLink-1)+12))) >> ((GbtLink-1)+12)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtRxReady(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & (2**((GbtLink-1)+16))) >> ((GbtLink-1)+16)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtRxIsDataFlag(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtStatReg", 1) & (2**((GbtLink-1)+20))) >> ((GbtLink-1)+20)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    # Elinks alignment control:
    def AppSfpGbtElinkAlignData(self, GbtLink, TxOrRx, AlignMask):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if (len(bin(AlignMask)[2:]) <= 120):
                    RegisterName  = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                    AlignMaskReg5 = (AlignMask & 0xFFFFC0000000000000000000000000) >> 102
                    AlignMaskReg4 = (AlignMask & 0x00003FFFFFFF000000000000000000) >>  72
                    AlignMaskReg3 = (AlignMask & 0x000000000000FFFFFFFC0000000000) >>  42
                    AlignMaskReg2 = (AlignMask & 0x00000000000000000003FFFFFFF000) >>  12
                    AlignMaskReg1 = (AlignMask & 0x000000000000000000000000000FFF) <<  18
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg5, 0x0003FFFF, 5)
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg4, 0x3FFFFFFF, 4)
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg3, 0x3FFFFFFF, 3)
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg2, 0x3FFFFFFF, 2)
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg1, 0x3FFC0000, 1)
                else:
                    print  "Align Mask must be 120 bits: 3 control bits x 40 Data Elinks"
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignData(self, GbtLink, TxOrRx):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                AlignMask = 0
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 5) & 0x0003FFFF) << 102)
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 4) & 0x3FFFFFFF) <<  72)
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 3) & 0x3FFFFFFF) <<  42)
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 2) & 0x3FFFFFFF) <<  12)
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 1) & 0x3FFC0000) >>  18)
                return HexN(AlignMask)
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtElinkAlignWbData(self, GbtLink, TxOrRx, AlignMask):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if (len(bin(AlignMask)[2:]) <= 48):
                    RegisterName  = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                    AlignMaskReg1 = (AlignMask & 0xFFFFC0000000) >>  30
                    AlignMaskReg0 = (AlignMask & 0x00003FFFFFFF) >>   0
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg1, 0x0003FFFF, 1)
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg0, 0x3FFFFFFF, 0)
                else:
                    print  "Align Mask must be 48 bits: 3 control bits x 16 WideBus Data Elinks"
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignWbData(self, GbtLink, TxOrRx):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                AlignMask = 0
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 1) & 0x0003FFFF) << 30)
                AlignMask = AlignMask + ((VfcHd_System.Read(self, RegisterName, 0) & 0x3FFFFFFF) <<  0)
                return HexN(AlignMask)
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtElinkAlignScEc(self, GbtLink, TxOrRx, AlignMask):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if (len(bin(AlignMask)[2:]) <= 3):
                    RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                    AlignMaskReg5 = AlignMask << 18
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg5, 0x001C0000, 5)
                else:
                    print  "Align Mask must be 3 bits: 3 control bits x 1 Slow Control External Control Elink"
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignScEc(self, GbtLink, TxOrRx):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                AlignMask = (VfcHd_System.Read(self, RegisterName, 5) & 0x001C0000) >> 18
                return HexN(AlignMask)[7:8]
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtElinkAlignScIc(self, GbtLink, TxOrRx, AlignMask):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if (len(bin(AlignMask)[2:]) <= 3):
                    RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                    AlignMaskReg5 = AlignMask << 21
                    VfcHd_System.RdModWr(self, RegisterName, AlignMaskReg5, 0x00E00000, 5)
                else:
                    print  "Align Mask must be 3 bits: 3 control bits x 1 Slow Control Internal Control Elink"
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignScIc(self, GbtLink, TxOrRx):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                AlignMask = (VfcHd_System.Read(self, RegisterName, 5) & 0x00E00000) >> 21
                return HexN(AlignMask)[7:8]
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtElinkAlignDebug(self, GbtLink, TxOrRx, Elink, DlyA, DlyB, SwapAB):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if ((0 <= Elink) and (Elink <= 57)):
                    if ((DlyA == 0) or (DlyA == 1)) and ((DlyB == 0) or (DlyB == 1)) and ((SwapAB == 0) or (SwapAB == 1)):
                        RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                        if   ( 0 <= Elink) and (Elink <=  9):
                            RegOffSet = 0
                            BitOffSet = 2**(Elink*3)
                        elif (10 <= Elink) and (Elink <= 19):
                            RegOffSet = 1
                            BitOffSet = 2**((Elink-10)*3)
                        elif (20 <= Elink) and (Elink <= 29):
                            RegOffSet = 2
                            BitOffSet = 2**((Elink-20)*3)
                        elif (30 <= Elink) and (Elink <= 39):
                            RegOffSet = 3
                            BitOffSet = 2**((Elink-30)*3)
                        elif (40 <= Elink) and (Elink <= 49):
                            RegOffSet = 4
                            BitOffSet = 2**((Elink-40)*3)
                        elif (50 <= Elink) and (Elink <= 57):
                            RegOffSet = 5
                            BitOffSet = 2**((Elink-50)*3)
                        Command = SwapAB*(2**2) + DlyB*(2**1) + DlyA*(2**0)
                        return VfcHd_System.RdModWr(self, RegisterName, Command*BitOffSet, 0x7*BitOffSet, RegOffSet)
                    else:
                        print  "DlyA, DlyB and SwapAB must be set to \"0\" or \"1\""
                        return "ERROR"
                else:
                    print  "Elink must be set from \"0\" to \"57\""
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignDebug(self, GbtLink, TxOrRx, Elink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if (TxOrRx == "Tx") or (TxOrRx == "Rx"):
                if ((0 <= Elink) and (Elink <= 57)):
                    RegisterName = "AppSfpGbt" + TxOrRx + "AlignReg" + str(GbtLink)
                    if   ( 0 <= Elink) and (Elink <=  9):
                        RegOffSet = 0
                        BitOffSet = 2**(Elink*3)
                    elif (10 <= Elink) and (Elink <= 19):
                        RegOffSet = 1
                        BitOffSet = 2**((Elink-10)*3)
                    elif (20 <= Elink) and (Elink <= 29):
                        RegOffSet = 2
                        BitOffSet = 2**((Elink-20)*3)
                    elif (30 <= Elink) and (Elink <= 39):
                        RegOffSet = 3
                        BitOffSet = 2**((Elink-30)*3)
                    elif (40 <= Elink) and (Elink <= 49):
                        RegOffSet = 4
                        BitOffSet = 2**((Elink-40)*3)
                    elif (50 <= Elink) and (Elink <= 57):
                        RegOffSet = 5
                        BitOffSet = 2**((Elink-50)*3)
                    return VfcHd_System.Read(self, RegisterName, RegOffSet) & (0x7*BitOffSet)
                else:
                    print  "Elink must be set from \"0\" to \"57\""
                    return "ERROR"
            else:
                print  "TxOrRx must be set to \"Tx\" or \"Rx\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def AppSfpGbtElinkAlignDebugData(self, GbtLink, TxOrRx, DataElink, DlyA, DlyB, SwapAB):
        if ((0 <= DataElink) and (DataElink <= 39)):
            Elink = DataElink+16
            self.AppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, Elink, DlyA, DlyB, SwapAB)
        else:
            print  "DataElink must be set from \"0\" to \"39\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignDebugData(self, GbtLink, TxOrRx, DataElink):
        if ((0 <= DataElink) and (DataElink <= 39)):
            Elink = DataElink+16
            self.ReadAppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, Elink)
        else:
            print  "DataElink must be set from \"0\" to \"39\""
            return "ERROR"

    def AppSfpGbtElinkAlignDebugWbData(self, GbtLink, TxOrRx, WbDataElink, DlyA, DlyB, SwapAB):
        if ((0 <= WbDataElink) and (WbDataElink <= 15)):
            Elink = WbDataElink
            self.AppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, Elink, DlyA, DlyB, SwapAB)
        else:
            print  "WbDataElink must be set from \"0\" to \"15\""
            return "ERROR"

    def ReadAppSfpGbtElinkAlignDebugWbData(self, GbtLink, TxOrRx, WbDataElink):
        if ((0 <= WbDataElink) and (WbDataElink <= 15)):
            Elink = WbDataElink
            self.ReadAppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, Elink)
        else:
            print  "WbDataElink must be set from \"0\" to \"15\""
            return "ERROR"

    def AppSfpGbtElinkAlignDebugScEc(self, GbtLink, TxOrRx, DlyA, DlyB, SwapAB):
        self.AppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, 56, DlyA, DlyB, SwapAB)

    def ReadAppSfpGbtElinkAlignDebugScEc(self, GbtLink, TxOrRx):
        self.ReadAppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, 56)

    def AppSfpGbtElinkAlignDebugScIc(self, GbtLink, TxOrRx, DlyA, DlyB, SwapAB):
        self.AppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, 57, DlyA, DlyB, SwapAB)

    def ReadAppSfpGbtElinkAlignDebugScIc(self, GbtLink, TxOrRx):
        self.ReadAppSfpGbtElinkAlignDebug(GbtLink, TxOrRx, 57)

    #=======================#
    # GBT Example User Data #
    #=======================#

    # Example User Data control:
    def TxIsDataSelAppSfpGbtUserData(self, GbtLink, TxIsDataSel):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((TxIsDataSel == 0) or (TxIsDataSel == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", TxIsDataSel*(2**(GbtLink-1)), 2**(GbtLink-1), 0)
            else:
                print  "TxIsDataSel must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadTxIsDataSelAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (2**(GbtLink-1))) >> (GbtLink-1)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def TestPatternSelAppSfpGbtUserData(self, TestPatternSel):
        if ((0 <= TestPatternSel) or (TestPatternSel <= 2)):
            return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", TestPatternSel*(2**4), 0x30, 0)
        else:
            print  "TestPatternSel must be set from \"0\" to \"2\""
            return "ERROR"

    def ReadTestPatternSelAppSfpGbtUserData(self):
        return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (0x3*(2**4))) >> 4

    def ErrInjAppSfpGbtUserData(self, ErrInj):
        if ((ErrInj == 0 ) or (ErrInj == 1)):
            return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", ErrInj*(2**8), 0x100, 0)
        else:
            print  "SingleErrInj must be set to \"0\" or \"1\""
            return "ERROR"

    def ReadSingleErrInjAppSfpGbtUserData(self):
        return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & 2**8) >> 8

    def ScEcCheckEnAppSfpGbtUserData(self, GbtLink, Enable):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Enable == 0) or (Enable == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", Enable*(2**((GbtLink-1)+12)), 2**((GbtLink-1)+12), 0)
            else:
                print  "Enable must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadScEcCheckEnAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (2**((GbtLink-1)+12))) >> ((GbtLink-1)+12)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def TogglingDataCheckSyncAppSfpGbtUserData(self, GbtLink, Sync):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Sync == 0) or (Sync == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", Sync*(2**((GbtLink-1)+16)), 2**((GbtLink-1)+16), 0)
            else:
                print  "Sync must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadTogglingDataCheckSyncAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (2**((GbtLink-1)+16))) >> ((GbtLink-1)+16)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ResetGbtRxRdyLostFlagAppSfpGbtUserData(self, GbtLink, Reset):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Reset == 0) or (Reset == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", Reset*(2**((GbtLink-1)+20)), 2**((GbtLink-1)+20), 0)
            else:
                print  "Reset must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadResetGbtRxRdyLostFlagAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (2**((GbtLink-1)+20))) >> ((GbtLink-1)+20)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ResetRxDataErrFlagAppSfpGbtUserData(self, GbtLink, Reset):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            if ((Reset == 0) or (Reset == 1)):
                return VfcHd_System.RdModWr(self, "AppSfpGbtUsrDatCtrlReg", Reset*(2**((GbtLink-1)+24)), 2**((GbtLink-1)+24), 0)
            else:
                print  "Reset must be set to \"0\" or \"1\""
                return "ERROR"
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadResetRxDataErrFlagAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatCtrlReg", 0) & (2**((GbtLink-1)+24))) >> ((GbtLink-1)+24)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    # Example User Data status:
    def ReadGbtRxReadyAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatStatReg", 0) & (2**(GbtLink-1))) >> (GbtLink-1)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadRxIsDataFlagAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatStatReg", 0) & (2**((GbtLink-1)+4))) >> ((GbtLink-1)+4)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadGbtRxRdyLostFlagAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatStatReg", 1) & (2**(GbtLink-1))) >> (GbtLink-1)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    def ReadRxDataErrFlagAppSfpGbtUserData(self, GbtLink):
        if ((1 <= GbtLink) and (GbtLink <= 4)):
            return (VfcHd_System.Read(self, "AppSfpGbtUsrDatStatReg", 1) & (2**((GbtLink-1)+4))) >> ((GbtLink-1)+4)
        else:
            print  "GbtLink must be set from \"1\" to \"4\""
            return "ERROR"

    # Example User Data BERT:
    def ReadRxDataErrorCntAppSfpGbtUserData(self, GbtLink):
        ReadBuffer = VfcHd_System.ReadMem(self, "AppSfpGbtUsrDatBertReg", 2, 2*(GbtLink-1), 0, 0)
        FrameCnt   = (ReadBuffer[1]*(2**32)) + ReadBuffer[0]
        return FrameCnt

    def ReadRxDataFrameCntAppSfpGbtUserData(self, GbtLink):
        ReadBuffer = VfcHd_System.ReadMem(self, "AppSfpGbtUsrDatBertReg", 2, (2*(GbtLink-1))+8, 0, 0)
        ErrCnt     = (ReadBuffer[1]*(2**32)) + ReadBuffer[0]
        return ErrCnt
