##-----------------------------------------------------------------------------
## Title      : Python drive: Common functions
## Project    : FMC DEL 1ns 2cha (FFPG)
## URL        : http://www.ohwr.org/projects/fmc-del-1ns-2cha
##-----------------------------------------------------------------------------
## File       : CommonFunctions.py
## Author(s)  : Jan Pospisil <j.pospisil@cern.ch>
##              Michael Betz <Michael.Betz@cern.ch>
## Company    : CERN (BE-BI-QP)
## Created    : 2016-09-06
## Last update: 2016-09-06
## Standard   : Python
##-----------------------------------------------------------------------------
## Description: Small common functions used in other classes
##-----------------------------------------------------------------------------
## Copyright (c) 2016 CERN (BE-BI-QP)
##-----------------------------------------------------------------------------
## GNU LESSER GENERAL PUBLIC LICENSE
##-----------------------------------------------------------------------------
## This source file is free software; you can redistribute it and/or modify it
## under the terms of the GNU Lesser General Public License as published by the
## Free Software Foundation; either version 2.1 of the License, or (at your
## option) any later version. This source is distributed in the hope that it
## will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
## of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details. You should have
## received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-----------------------------------------------------------------------------
## Revisions  :
## Date        Version  Author        Comment
## 2016-09-06  1.0      Jan Pospisil  Derived from the initial example driver
##-----------------------------------------------------------------------------

import sys
import math

class ConColors:
    BLACK = "\033[30m"
    RED = "\033[31m"
    GREEN = "\033[32m"
    YELLOW = "\033[33m"
    BLUE = "\033[34m" # better readable with BOLD
    MAGENTA = "\033[35m"
    CYAN = "\033[36m"
    WHITE = "\033[37m"
    NONE = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"

def toBin(x, count=8):
    """ Integer to binary. Count is number of padding bits """ 
    return "".join(map(lambda y:str((x>>y)&1), range(count-1, -1, -1)))
    
def hexDump( dataArray, bitsPerWord = 32, binary = False, columns = 8 ):
    """
    the ultimate hexDump function
    first column is byte-wise address
    """
    i = 0
    for word in dataArray:
        if i%columns == 0:
            if i != 0:
                sys.stdout.write("\n")
            sys.stdout.write( "%4x: "%(i*(bitsPerWord/8)) )
        if word != 0:
            sys.stdout.write(ConColors.BOLD)
        if binary:
            sys.stdout.write( toBin(word, bitsPerWord) + " " )    # binary format
        else:
            sys.stdout.write( ("%%0%dx "%(bitsPerWord/4))%word )  # hex format
        sys.stdout.write(ConColors.NONE)
        i += 1
    sys.stdout.write("\n")

def PrintBit(value, bit, label, ifSet = "", ifNotSet = ""):
    print("  "+"["+str(bit)+"] "+label+":")
    if value & (1<<bit):
        print(ConColors.GREEN+"    1: "+ifSet+ConColors.NONE)
    else:
        print(ConColors.RED+"    0: "+ifNotSet+ConColors.NONE)

def PrintBits(value, bitFrom, bitTo, label, descriptions=()):
    print("  "+"["+str(bitTo)+":"+str(bitFrom)+"] "+label+":")

    value &= (1<<bitTo+1)-1
    value >>= bitFrom

    color = ConColors.YELLOW
    if value > len(descriptions)-1:
        color = ConColors.RED
        description = "<unknown>"
    else:
        description = descriptions[value]

    print(color+"    "+str(value)+": "+description+ConColors.NONE)

def ErrorMsg(Text):
    print(ConColors.RED+ConColors.BOLD+"!!! Error: "+ConColors.NONE+ConColors.RED+Text+ConColors.NONE)

def WarningMsg(Text):
    print(ConColors.YELLOW+ConColors.BOLD+"Warning: "+ConColors.NONE+ConColors.YELLOW+Text+ConColors.NONE)

def InfoMsg(Text):
    print(ConColors.CYAN+ConColors.BOLD+"Info: "+ConColors.NONE+Text)

def OkMsg(Text):
    print(ConColors.GREEN+ConColors.BOLD+"OK: "+ConColors.NONE+Text)

def Oscilo(Data, FSampling, Height = 10, HOffset = 0, HScale = 1, AdData = [], AdDataLabel = [], XLabel = "t [ns]"):
    # prepare
    count = len(Data)
    osciloRow = range(Height)
    for h in range(Height):
        osciloRow[h] = list(' '*count)

    # fill in waveform
    for sample in range(count):
        value = int((Data[sample] + HOffset) * HScale)
        value += Height/2
        if value >= Height:
            osciloRow[Height-1][sample] = ConColors.RED+'^'+ConColors.NONE
        elif value < 0:
            osciloRow[0][sample] = ConColors.RED+'v'+ConColors.NONE
        else:
            osciloRow[value][sample] = ConColors.CYAN+'*'+ConColors.NONE

    # print vertical axis and waveform
    print('')
    for i in range(Height-1, -1, -1):
        if (i == Height-1) or (i == 0) or (i == (Height/2)-1):
            preS = '---+'
        else:
            preS = '   |'
        print(ConColors.YELLOW+ConColors.BOLD+preS+ConColors.NONE+''.join(osciloRow[i]))

    # print additional data
    for i in range(len(AdData)):
        data = AdData[i]
        assert len(data) == count, "AdData has different size!"
        row = list(' '*count)
        for sample in range(count):
            if data[sample] == 1:
                row[sample] = '!'
        print(AdDataLabel[i]+':'+ConColors.RED+ConColors.BOLD+''.join(row)+ConColors.NONE)

    # print horizontal axis
    preS = '    '
    ticks = int(math.ceil(count/10.0))
    tickStep = 10*1.0/FSampling
    tickStep = tickStep*1e9 # to [ns]
    tickLabels = range(ticks+1)
    for i in range(ticks+1):
        tickLabels[i] = ('%2.2f' % (i*tickStep)).ljust(10, ' ')
    print(ConColors.YELLOW+ConColors.BOLD+preS+'|----'*(ticks*2)+'|')
    print(preS+'|         '*ticks+'|')
    print(preS+''.join(tickLabels))
    print(preS+'    '+XLabel)
    print(ConColors.NONE)
