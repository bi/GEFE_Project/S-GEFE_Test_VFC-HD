##============================================================================================##
####################################   Script Information   ####################################
##============================================================================================##
##
## Company: CERN (<Department/Group/Section>)
##
## File Name: <File Name>
##
## File versions history:
##
##       DATE          VERSION      AUTHOR      DESCRIPTION
##     - <dd/mm/yy>    <Version>    <Author>    - <Description>.
##     - <dd/mm/yy>    <Version>    <Author>    - First script definition.
##
## Python Version: 2.7.11
##
## Description:
##
##     <Description>
##
##============================================================================================##
##============================================================================================##

## Importing application specific VFC-HD class:
from libs.Class_VfcHd_Base import *
from libs.CommonFunctions import *
import random

## Opening VME connection with VFC-HD:
Args      = sys.argv
ArgLength = len(Args)
Verbose = False
if ArgLength == 2:
    Lun   = int(Args[1], 10)
    VfcHd = VfcHd_Base(Lun, 3, 164, "ROAK", 200, Verbose)
else:
    print
    print "Error!! LUN must be provided..."
    print
    sys.exit()

##============================================================================================##
##======================================== Script Body =======================================##
##============================================================================================##

#===================== VME/WishBone transactions (with default values) ========================#
#                                                                                              #
# Write(RegName, DataLw, Offset=0):                                                            #
# WriteMem(RegName, DataLw, Offset=0, Dma=0, Time=0):                                          #
# Read(RegName, Offset=0):                                                                     #
# ReadMem(RegName, NbLongWords=-1, Offset=0, Dma=0, Time=0):                                   #
# RdModWr(RegName, DataLw, Mask, Offset=0)  # In the Mask, set to 1 the bits to be modified    #
# SetBit(RegName, Bit, Offset=0):                                                              #
# ClearBit(RegName, Bit, Offset=0):                                                            #
#                                                                                              #
#==============================================================================================#

print
print "#################################################################"
print "-> VFC-HD System has -Release ID- :", VfcHd.ReadSysRevId()
print "-> VFC-HD System Code:", VfcHd.ReadSysInfo()
print "#################################################################"
print "-> Application has -Release ID-   :", VfcHd.ReadAppRevId()
print "-> Application Code:", VfcHd.ReadAppInfo()
print "#################################################################"
print

ReadMemBuffer  = []
WriteMemBuffer = []


def TestDdr3(Bus, RegName, count, progressReportEach, errorsDisplayLimit):
    errors = 0
    errorsDisplayed = 0
    for i in range(0, count):
        if (i%progressReportEach) == 0:
            print('  %d %% ...' % (100*i/count))
        pattern = random.getrandbits(32)
        pattern = i
        Bus.Write(RegName, pattern, i)
        data = Bus.Read(RegName, i)
        # InfoMsg('Pattern: 0x%08x, Address: 0x%08x, DDR3 data: 0x%08x' % (pattern, i, data))
        if data != pattern:
            errors += 1
            if errorsDisplayed < errorsDisplayLimit:
                ErrorMsg('Data read from address [0x%08x] = 0x%08x != 0x%08x' % (i, data, pattern))
                errorsDisplayed += 1
            elif errorsDisplayed == errorsDisplayLimit:
                WarningMsg('More data mismatch occured, report skipped.')
                errorsDisplayed += 1
    print('Found %d errors (%d %%).' % (errors, 100*errors/count))


def TestDdr3_new(Bus, Log2PageDepth, MemoryAddressBits):
    print("Testing the data bits")
    for i in range(0,16):
        Bus.Write("app_ddr3_master", 2**i, i)
        Bus.Write("app_ddr3_slave", 2**i, i)
    for i in range(0,16):
        data = Bus.Read("app_ddr3_master",i)
        if data!=2**i :
            print("Data bit %d of DDR3a not working: read %x" % (i, data))
    for i in range(0,16):
        data = Bus.Read("app_ddr3_slave", i)
        if data!=2**i :
            print("Data bit %d of DDR3b not working: read %x" % (i, data))

    print("Testing the address bits")
    for i in range(0,MemoryAddressBits):
        DDR3address = 2**i
        SelectedPage = DDR3address / 2**Log2PageDepth
        PageAddress = DDR3address % 2**Log2PageDepth
        Bus.Write("app_ddr3_page_selector", SelectedPage)
        Bus.Write("app_ddr3_master", i, PageAddress)
        Bus.Write("app_ddr3_slave", 1024+i, PageAddress)
    Bus.Write("app_ddr3_page_selector", 0)
    Bus.Write("app_ddr3_master", 0, 0)
    Bus.Write("app_ddr3_slave", 0, 0)
    for i in range(0,MemoryAddressBits):
        DDR3address = 2**i
        SelectedPage = DDR3address / 2**Log2PageDepth
        PageAddress = DDR3address % 2**Log2PageDepth
        Bus.Write("app_ddr3_page_selector", SelectedPage)
        data = Bus.Read("app_ddr3_master",PageAddress)
        if data!=i :
            print("Address bit %d of DDR3a not working: read %x" % (i, data))
    for i in range(0,MemoryAddressBits):
        DDR3address = 2**i
        SelectedPage = DDR3address / 2**Log2PageDepth
        PageAddress = DDR3address % 2**Log2PageDepth
        Bus.Write("app_ddr3_page_selector", SelectedPage)
        data = Bus.Read("app_ddr3_slave",PageAddress)
        if data!=i+1024 :
            print("Address bit %d of DDR3b not working: read %x" % (i, data))

    print("Testing the page selection mechanism")
    for SelectedPage in range(0, 3):
        Bus.Write("app_ddr3_page_selector", SelectedPage)
        print('Writing page %d' % (SelectedPage))
        for i in range(0, 2**Log2PageDepth):
        #for i in range(0, 9):
            pattern = 2*SelectedPage*2**Log2PageDepth + i
            Bus.Write("app_ddr3_master", pattern       , i)
            Bus.Write("app_ddr3_slave" , 0x8000+pattern, i)
    for SelectedPage in range(0, 3):
        Bus.Write("app_ddr3_page_selector", SelectedPage)
        print('Reading page %d' % (SelectedPage))
        for i in range(0, 2**Log2PageDepth):
        #for i in range(0, 9):
            pattern = 2*SelectedPage*2**Log2PageDepth + i
            data = Bus.Read("app_ddr3_master", i)
            if data != pattern:
                print("Error in page %d at address %d of DDRa: data  %x != pattern %x" % (SelectedPage, i, data, pattern))
        for i in range(0, 2**Log2PageDepth):
        #for i in range(0, 9):
            pattern = 2*SelectedPage*2**Log2PageDepth + i
            data = Bus.Read("app_ddr3_slave", i)
            pattern = (0x8000+pattern)
            if data != pattern:
                print("Error in page %d at address %d of DDRb: data  %x != pattern %x" % (SelectedPage, i, data, pattern))

print "-> Single read & write example:"
print "-> ----------------------------"
print "->"
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print "-> Single write (0x01234567) to Control register 0..."
VfcHd.Write("ExampleControlReg", 0x01234567, 0)
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print "->"
print "-> Single read to Control register 2..."
print "-> Control register 2 content:", HexN(VfcHd.Read("ExampleControlReg", 2))
print "-> Single write (0x89ABCDEF) to Control register 2..."
VfcHd.Write("ExampleControlReg", 0x89ABCDEF, 2)
print "-> Single read to Control register 2..."
print "-> Control register 2 content:", HexN(VfcHd.Read("ExampleControlReg", 2))
print
print "-> Memory read & write example:"
print "-> ----------------------------"
print "->"
print "-> Memory read to Status registers..."
ReadMemBuffer = VfcHd.ReadMem("ExampleStatusReg")
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of  Status register %d: %s" % (i, HexN(ReadMemBuffer[i]))
print "->"
print "-> Memory read to Control register..."
ReadMemBuffer = VfcHd.ReadMem("ExampleControlReg")
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))
print "-> Memory write [0x11111111,0x22222222,0x33333333,0x44444444] to Control registers..."
WriteMemBuffer = [0x11111111,0x22222222,0x33333333,0x44444444]
VfcHd.WriteMem("ExampleControlReg", WriteMemBuffer)
print "-> Memory read to Control register..."
ReadMemBuffer = VfcHd.ReadMem("ExampleControlReg")
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))
print
print "-> Block read & write example:"
print "-> ---------------------------"
print "->"
print "-> Block read to Status registers 1 & 2..."
ReadMemBuffer = VfcHd.ReadMem("ExampleStatusReg", 2, 1)
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of  Status register %d: %s" % ((i+1), HexN(ReadMemBuffer[i]))
print "->"
print "-> Block read to Control registers 2 & 3..."
ReadMemBuffer = VfcHd.ReadMem("ExampleControlReg", 2, 2)
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of Control register %d: %s" % ((i+2), HexN(ReadMemBuffer[i]))
print "-> Block write [0x00112233,0x44556677] to Control registers 2 & 3..."
WriteMemBuffer = [0x00112233,0x44556677]
VfcHd.WriteMem("ExampleControlReg", WriteMemBuffer, 2)
print "-> Block read to Control registers 2 & 3..."
ReadMemBuffer = VfcHd.ReadMem("ExampleControlReg", 2, 2)
for i in range(0,len(ReadMemBuffer)):
    print "-> Content of Control register %d: %s" % ((i+2), HexN(ReadMemBuffer[i]))
print
print "-> Read-Modify-Write (RmW) example:"
print "-> --------------------------------"
print "->"
print "-> Memory write [0xAAAAAAAA,0xBBBBBBBB,0xCCCCCCCC,0xDDDDDDDD] to Control registers..."
WriteMemBuffer = [0xAAAAAAAA,0xBBBBBBBB,0xCCCCCCCC,0xDDDDDDDD]
VfcHd.WriteMem("ExampleControlReg", WriteMemBuffer)
print "-> RmW [0x00000000,0x000000FF] to Control register 0..."
VfcHd.RdModWr("ExampleControlReg", 0x00000000, 0x000000FF,0)
print "-> RmW [0x00000000,0x0000FF00] to Control register 1..."
VfcHd.RdModWr("ExampleControlReg", 0x00000000, 0x0000FF00,1)
print "-> RmW [0x00000000,0x00FF0000] to Control register 2..."
VfcHd.RdModWr("ExampleControlReg", 0x00000000, 0x00FF0000,2)
print "-> RmW [0xFFFF0000,0x00FFFF00] to Control register 3..."
VfcHd.RdModWr("ExampleControlReg", 0xFFFF0000, 0x00FFFF00,3)
print "->"
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print "->"
print "-> Single read to Control register 1..."
print "-> Control register 1 content:", HexN(VfcHd.Read("ExampleControlReg", 1))
print "->"
print "-> Single read to Control register 2..."
print "-> Control register 2 content:", HexN(VfcHd.Read("ExampleControlReg", 2))
print "->"
print "-> Single read to Control register 3..."
print "-> Control register 3 content:", HexN(VfcHd.Read("ExampleControlReg", 3))
print
print "-> Set/Clear Bit example:"
print "-> ----------------------"
print "->"
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print "->"
print "-> Set bit 0 to Control register 0..."
VfcHd.SetBit("ExampleControlReg", 0)
print "->"
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print "->"
print "-> Clear bit 0 to Control register 0..."
VfcHd.ClearBit("ExampleControlReg", 0)
print "->"
print "-> Single read to Control register 0..."
print "-> Control register 0 content:", HexN(VfcHd.Read("ExampleControlReg", 0))
print
print "-> =========================="
print "-> Base script execution done"
print "-> =========================="
print
sleep(0.5)
'''
print "-> Testing DDR3:"
print "-> -------------"
print "->"
count = 262144 # full test
# count = 128 # short test
progressReportEach = 50000
errorsDisplayLimit = 10
print("Testing DDR3 A (Master)")
TestDdr3(VfcHd, 'app_ddr3_master', count, progressReportEach, errorsDisplayLimit)
print("Testing DDR3 B (Slave)")
TestDdr3(VfcHd, 'app_ddr3_slave', count, progressReportEach, errorsDisplayLimit)
print("DDR3 test done.")
print("")
'''
TestDdr3_new(VfcHd, 10, 28)
