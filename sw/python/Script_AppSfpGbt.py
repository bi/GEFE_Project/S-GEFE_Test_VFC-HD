##============================================================================================##
####################################   Script Information   ####################################
##============================================================================================##
##
## Company: CERN (BE-BI-QP)
##
## File Name: Script_AppSfpGbt
##
## File versions history:
##
##   DATE        VERSION    AUTHOR             DESCRIPTION
## - 15/10/17    1.1        M. Barros Marin    - Added BERT.
##                                             - Removed internal pattern generator/checker.
## - 04/04/17    1.0        M. Barros Marin    First script definition.
##
## Python Version: 2.7.11
##
## Description:
##
##     Script for controlling the Application SFP GBT-FPGA of the VFC-HD.
##
##============================================================================================##
##============================================================================================##

## Importing application specific VFC-HD class:
from libs.Class_VfcHd_Base import *

## Opening VME connection with VFC-HD:
Args      = sys.argv
ArgLength = len(Args)
if ArgLength == 2:
    Lun   = int(Args[1], 10)
    VfcHd = VfcHd_Base(Lun, 3, 164, "ROAK", 200, False)
else:
    print
    print "Error!! LUN must be provided..."
    print
    sys.exit()

##============================================================================================##
##======================================== Script Body =======================================##
##============================================================================================##

print
print "#################################################################"
print "-> VFC-HD System has -Release ID- :", VfcHd.ReadSysRevId()
print "-> VFC-HD System Code:", VfcHd.ReadSysInfo()
print "#################################################################"
print "-> Application has -Release ID-   :", VfcHd.ReadAppRevId()
print "-> Application Code:", VfcHd.ReadAppInfo()
print "#################################################################"
print

print "##############################################################"
print "##############################################################"
print "################# Application SFPs GBT-FPGA ##################"
print "##############################################################"
print "##############################################################"
print

##==== Parameters to be modified by the user ====##

# GBT Clocks Scheme:
RefClk                  = "FreeRunning" # BST or FreeRunning
PllRefSettingEn         = 1
PllRefConfigFile        = "./libs/PllRef_GbtFpga_NoBstClkSch.h" # PllRef_GbtFpga_BstClkSch.h or PllRef_GbtFpga_NoBstClkSch.h
PllRefConfigVerbose     = False

# GBT Configuration:
InvTxPolarity           = 1
InvRxPolarity           = 1
LoopBack                = 0

# GBT Resets Scheme:
GbtReset                = 1 # GbtReset must be asserted after power up for the GBT Bank to operate (then it can be de-asserted)
GbtTxResets             = [0, 0, 0, 0]
GbtRxResets             = [0, 0, 0, 0]

# GBT User Data Example:
TxIsDataSel             = [0, 0, 0, 0]
UserDataTxPattern       = "Static" # Disabled, Static, Counter, Toggling
UserDataPatGenChkEn     = 1
UserDataScEcCheckEn     = [1, 1, 1, 1]
TogglingDataCheckSync   = [0, 0, 0, 0]
UserDataPatChkReset     = 1
UserDataErrInj          = 0

# Elinks Alignment (3 bits per Elink: SwapAB, DelayB, DelayA):
ElinksAlignMaskTxData   = [0x492492492492492492492492492492, 0x492492492492492492492492492492, 0x492492492492492492492492492492, 0x492492492492492492492492492492] # Use with FPGA-GEFE communication
#ElinksAlignMaskTxData   = [0x000000000000000000000000000000, 0x000000000000000000000000000000, 0x000000000000000000000000000000, 0x000000000000000000000000000000] # Use with FPGA-FPGA communication
ElinksAlignMaskRxData   = [0x000000000000000000000000000000, 0x000000000000000000000000000000, 0x000000000000000000000000000000, 0x000000000000000000000000000000]
ElinksAlignMaskTxWbData = [0x000000000000, 0x000000000000, 0x000000000000, 0x000000000000]
ElinksAlignMaskRxWbData = [0x000000000000, 0x000000000000, 0x000000000000, 0x000000000000]
ElinksAlignMaskTxScEc   = [0x2, 0x2, 0x2, 0x2] # Use with FPGA-GEFE communication
#ElinksAlignMaskTxScEc   = [0x0, 0x0, 0x0, 0x0] # Use with FPGA-FPGA communication
ElinksAlignMaskRxScEc   = [0x0, 0x0, 0x0, 0x0]
ElinksAlignMaskTxScIc   = [0x0, 0x0, 0x0, 0x0]
ElinksAlignMaskRxScIc   = [0x0, 0x0, 0x0, 0x0]

##===============================================##

print "##############################"
print "# GBT Reference Clock Scheme #"
print "##############################"
print
print "-> Reading status of BST SFP (GX L5))..."
print "-> SFP Present:               ", VfcHd.ReadBstSfpPresent()
print "-> TX Fault:                  ", VfcHd.ReadBstSfpTxFault()
print "-> Loss Of Signal (LOS):      ", VfcHd.ReadBstSfpLos()
print "-> TX Disable:                ", VfcHd.ReadBstSfpStatusTxDisable()
print "-> Rate Select:               ", VfcHd.ReadBstSfpStatusRateSelect()
print "->"
print "-> BST CDR On:                ", VfcHd.ReadBstOn()
print "->"
print "-> Setting reference clock for GBT Bank as:", RefClk
VfcHd.GbtClkSchRefClkSel(RefClk)
if RefClk == "BST":
    print "-> Reading status of BST as GBT Bank reference clock..."
    print "-> BST as GBT Bank reference clock:", VfcHd.ReadGbtClkSchBstAsRefClk()
if PllRefSettingEn == 1:
    print "->"
    VfcHd.PllRefSetting(PllRefConfigFile, PllRefConfigVerbose)
print "->"
print "-> Reading status of PLLs..."
if RefClk == "BST":
    print "-> FreeRunning PLL:           ", VfcHd.ReadGbtClkSchFreeRunningPllLock()
print "-> TX_FRAMECLK PLL:           ", VfcHd.ReadGbtClkSchTxFrameClkPllLock()
print

print "############################"
print "# Application SFPs Control #"
print "############################"
print
for i in range(1,5):
    print "-> Application SFP %d Tx Disable  set to: 0 (Enabled)" % i
    VfcHd.AppSfpTxDisable(i, "On")
    print "-> Application SFP %d Rate Select set to: 1 (Lane Rate > 4.25 GBps)" % i
    VfcHd.AppSfpRateSelect(i, 1)
print

print "#########################"
print "# Serial Lanes Polarity #"
print "#########################"
print
for i in range(1,5):
    print "-> TX Polarity Inversion of SFP %d set to: %d" % (i, InvTxPolarity)
    VfcHd.AppSfpGbtTxPolInv(i, InvTxPolarity)
    print "-> RX Polarity Inversion of SFP %d set to: %d" % (i, InvRxPolarity)
    VfcHd.AppSfpGbtRxPolInv(i, InvRxPolarity)
print

print "############################"
print "# Internal Serial LoopBack #"
print "############################"
print
for i in range(1,5):
    print "-> Serial LoopBack of SFP %d set to: %d" % (i, LoopBack)
    VfcHd.AppSfpGbtLoopBackEn(i, LoopBack)
print

# To be added
#print "######################"
#print "# TX_WORDCLK Monitor #"
#print "######################"
#print
#print "-> TX_WORDCLK Monitor Enable set to:", TxWordClkMonitorEn
#VfcHd.AppSfpGbtTxWordClkMonEn(TxWordClkMonitorEn)
#print
# To be added

print "#############"
print "# GBT Reset #"
print "#############"
print
if GbtReset == 1:
    print "-> Resetting GBT Bank..."
    VfcHd.AppSfpGbtReset(1)
    sleep(0.5)
    VfcHd.AppSfpGbtReset(0)
    sleep(5) # Note!! There is a delay or 4 seconds in the reset controller of the GateWare
else:
    print "-> Skipping GBT Bank Reset..."
print "->"
print "-> GBT TX Resets set to:"
for i in range(1,5):
    print "-> GBT Link %d: %d" % (i, GbtTxResets[i-1])
    VfcHd.AppSfpGbtTxReset(i, GbtTxResets[i-1])
sleep(0.5)
print "->"
print "-> GBT RX Resets set to:"
for i in range(1,5):
    print "-> GBT Link %d: %d" % (i, GbtRxResets[i-1])
    VfcHd.AppSfpGbtRxReset(i, GbtRxResets[i-1])
sleep(0.5)
print

print "##############"
print "# TX is Data #"
print "##############"
print
print "-> TxIsDataSel set to:"
for i in range(1,5):
    print "-> GBT Link %d: %d" % (i, TxIsDataSel[i-1])
    VfcHd.TxIsDataSelAppSfpGbtUserData(i, TxIsDataSel[i-1])
sleep(0.5)
print

print "##############"
print "# GBT Status #"
print "##############"
print
print "-> GBT ID:", VfcHd.ReadAppSfpGbtBankId()
print "->"
print "-> Reading status of GBT Link 1 (App. SFP1) (GX L0)..."
print "-> SFP Present:                ", VfcHd.ReadAppSfpPresent(1)
print "-> TX Fault:                   ", VfcHd.ReadAppSfpTxFault(1)
print "-> Loss Of Signal (LOS):       ", VfcHd.ReadAppSfpLos(1)
print "-> TX Disable:                 ", VfcHd.ReadAppSfpStatusTxDisable(1)
print "-> Rate Select:                ", VfcHd.ReadAppSfpStatusRateSelect(1)
print "-> ----------------------------"
print "-> MGT TX   Ready:             ", VfcHd.ReadAppSfpGbtMgtTxReady(1)
print "-> MGT RX   Ready:             ", VfcHd.ReadAppSfpGbtMgtRxReady(1)
print "-> GBT TX   Ready:             ", VfcHd.ReadAppSfpGbtTxReady(1)
print "-> GBT RX   Ready:             ", VfcHd.ReadAppSfpGbtRxReady(1)
print "-> ---------------------------"
print "-> RX is Data Flag:            ", VfcHd.ReadAppSfpGbtRxIsDataFlag(1)
print "->"
print "-> Reading status of GBT Link 2 (App. SFP2) (GX L1)..."
print "-> SFP Present:                ", VfcHd.ReadAppSfpPresent(2)
print "-> TX Fault:                   ", VfcHd.ReadAppSfpTxFault(2)
print "-> Loss Of Signal (LOS):       ", VfcHd.ReadAppSfpLos(2)
print "-> TX Disable:                 ", VfcHd.ReadAppSfpStatusTxDisable(2)
print "-> Rate Select:                ", VfcHd.ReadAppSfpStatusRateSelect(2)
print "-> ----------------------------"
print "-> MGT TX   Ready:             ", VfcHd.ReadAppSfpGbtMgtTxReady(2)
print "-> MGT RX   Ready:             ", VfcHd.ReadAppSfpGbtMgtRxReady(2)
print "-> GBT TX   Ready:             ", VfcHd.ReadAppSfpGbtTxReady(2)
print "-> GBT RX   Ready:             ", VfcHd.ReadAppSfpGbtRxReady(2)
print "-> ---------------------------"
print "-> RX is Data Flag:            ", VfcHd.ReadAppSfpGbtRxIsDataFlag(2)
print "->"
print "-> Reading status of GBT Link 3 (App. SFP3) (GX L2)..."
print "-> SFP Present:                ", VfcHd.ReadAppSfpPresent(3)
print "-> TX Fault:                   ", VfcHd.ReadAppSfpTxFault(3)
print "-> Loss Of Signal (LOS):       ", VfcHd.ReadAppSfpLos(3)
print "-> TX Disable:                 ", VfcHd.ReadAppSfpStatusTxDisable(3)
print "-> Rate Select:                ", VfcHd.ReadAppSfpStatusRateSelect(3)
print "-> ----------------------------"
print "-> MGT TX   Ready:             ", VfcHd.ReadAppSfpGbtMgtTxReady(3)
print "-> MGT RX   Ready:             ", VfcHd.ReadAppSfpGbtMgtRxReady(3)
print "-> GBT TX   Ready:             ", VfcHd.ReadAppSfpGbtTxReady(3)
print "-> GBT RX   Ready:             ", VfcHd.ReadAppSfpGbtRxReady(3)
print "-> ---------------------------"
print "-> RX is Data Flag:            ", VfcHd.ReadAppSfpGbtRxIsDataFlag(3)
print "->"
print "-> Reading status of GBT Link 4 (App. SFP4) (GX L3)..."
print "-> SFP Present:                ", VfcHd.ReadAppSfpPresent(4)
print "-> TX Fault:                   ", VfcHd.ReadAppSfpTxFault(4)
print "-> Loss Of Signal (LOS):       ", VfcHd.ReadAppSfpLos(4)
print "-> TX Disable:                 ", VfcHd.ReadAppSfpStatusTxDisable(4)
print "-> Rate Select:                ", VfcHd.ReadAppSfpStatusRateSelect(4)
print "-> ----------------------------"
print "-> MGT TX   Ready:             ", VfcHd.ReadAppSfpGbtMgtTxReady(4)
print "-> MGT RX   Ready:             ", VfcHd.ReadAppSfpGbtMgtRxReady(4)
print "-> GBT TX   Ready:             ", VfcHd.ReadAppSfpGbtTxReady(4)
print "-> GBT RX   Ready:             ", VfcHd.ReadAppSfpGbtRxReady(4)
print "-> ---------------------------"
print "-> RX is Data Flag:            ", VfcHd.ReadAppSfpGbtRxIsDataFlag(4)
print

print "####################"
print "# Elinks Alignment #"
print "####################"
print
print "-> Setting Elinks Alignment..."
for i in range(1,5):
    VfcHd.AppSfpGbtElinkAlignData  (i, "Tx", ElinksAlignMaskTxData[i-1])
    VfcHd.AppSfpGbtElinkAlignData  (i, "Rx", ElinksAlignMaskRxData[i-1])
    VfcHd.AppSfpGbtElinkAlignWbData(i, "Tx", ElinksAlignMaskTxWbData[i-1])
    VfcHd.AppSfpGbtElinkAlignWbData(i, "Rx", ElinksAlignMaskRxWbData[i-1])
    VfcHd.AppSfpGbtElinkAlignScEc  (i, "Tx", ElinksAlignMaskTxScEc[i-1])
    VfcHd.AppSfpGbtElinkAlignScEc  (i, "Rx", ElinksAlignMaskRxScEc[i-1])
    VfcHd.AppSfpGbtElinkAlignScIc  (i, "Tx", ElinksAlignMaskTxScIc[i-1])
    VfcHd.AppSfpGbtElinkAlignScIc  (i, "Rx", ElinksAlignMaskRxScIc[i-1])
print "->"
print "-> Reading Elinks Alignment Setting..."
for i in range(1,5):
    print "-> GBT Link %d - TX Data Elinks:         %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignData(i, "Tx"))
    print "-> GBT Link %d - RX Data Elinks:         %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignData(i, "Rx"))
    print "-> GBT Link %d - TX WideBus Data Elinks: %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignWbData(i, "Tx"))
    print "-> GBT Link %d - RX WideBus Data Elinks: %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignWbData(i, "Rx"))
    print "-> GBT Link %d - TX Sc Ec Elinks:        %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignScEc(i, "Tx"))
    print "-> GBT Link %d - RX Sc Ec Elinks:        %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignScEc(i, "Rx"))
    print "-> GBT Link %d - TX Sc Ic Elinks:        %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignScIc(i, "Tx"))
    print "-> GBT Link %d - RX Sc Ic Elinks:        %s" % (i, VfcHd.ReadAppSfpGbtElinkAlignScIc(i, "Rx"))
print

if UserDataPatGenChkEn == 1:
    print "#######################################"
    print "# User Data Pattern Generator/Checker #"
    print "#######################################"
    print
    print "-> Setting User Data SC IC Check..."
    VfcHd.ScEcCheckEnAppSfpGbtUserData(1, UserDataScEcCheckEn[0])
    print "-> GBT Link 1 (App. SFP1) (GX L0) set to:", VfcHd.ReadScEcCheckEnAppSfpGbtUserData(1)
    VfcHd.ScEcCheckEnAppSfpGbtUserData(2, UserDataScEcCheckEn[1])
    print "-> GBT Link 2 (App. SFP2) (GX L1) set to:", VfcHd.ReadScEcCheckEnAppSfpGbtUserData(2)
    VfcHd.ScEcCheckEnAppSfpGbtUserData(3, UserDataScEcCheckEn[2])
    print "-> GBT Link 3 (App. SFP3) (GX L2) set to:", VfcHd.ReadScEcCheckEnAppSfpGbtUserData(3)
    VfcHd.ScEcCheckEnAppSfpGbtUserData(4, UserDataScEcCheckEn[3])
    print "-> GBT Link 4 (App. SFP4) (GX L3) set to:", VfcHd.ReadScEcCheckEnAppSfpGbtUserData(4)
    print
    print "-> Setting User Data TX Pattern Generator..."
    if UserDataTxPattern == "Disabled":
        TxPatternNumber = 0
    elif UserDataTxPattern == "Counter":
        TxPatternNumber = 1
    elif UserDataTxPattern == "Static":
        TxPatternNumber = 2
    elif UserDataTxPattern == "Toggling":
        TxPatternNumber = 3
    VfcHd.TestPatternSelAppSfpGbtUserData(TxPatternNumber)
    TxPatternBuf = VfcHd.ReadTestPatternSelAppSfpGbtUserData()
    if TxPatternBuf == 0:
        TxPatternName = "Disabled"
    elif TxPatternBuf == 1:
        TxPatternName = "Counter"
    elif TxPatternBuf == 2:
        TxPatternName = "Static"
    elif TxPatternBuf == 3:
        TxPatternName = "Toggling"
    print "-> User Data TX Pattern Generator set to:", TxPatternName
    print
    print "-> Setting User Data Toggling Check Synch..."
    VfcHd.TogglingDataCheckSyncAppSfpGbtUserData(1, TogglingDataCheckSync[0])
    print "-> GBT Link 1 (App. SFP1) (GX L0) set to:", VfcHd.ReadTogglingDataCheckSyncAppSfpGbtUserData(1)
    VfcHd.TogglingDataCheckSyncAppSfpGbtUserData(2, TogglingDataCheckSync[1])
    print "-> GBT Link 2 (App. SFP2) (GX L1) set to:", VfcHd.ReadTogglingDataCheckSyncAppSfpGbtUserData(2)
    VfcHd.TogglingDataCheckSyncAppSfpGbtUserData(3, TogglingDataCheckSync[2])
    print "-> GBT Link 3 (App. SFP3) (GX L2) set to:", VfcHd.ReadTogglingDataCheckSyncAppSfpGbtUserData(3)
    VfcHd.TogglingDataCheckSyncAppSfpGbtUserData(4, TogglingDataCheckSync[3])
    print "-> GBT Link 4 (App. SFP4) (GX L3) set to:", VfcHd.ReadTogglingDataCheckSyncAppSfpGbtUserData(4)
    if UserDataErrInj == 1:
        print
        print "-> Inserting Single Error in TX Data..."
        VfcHd.ErrInjAppSfpGbtUserData(1)
        VfcHd.ErrInjAppSfpGbtUserData(0)
    if UserDataPatChkReset == 1:
        print
        print "-> Resetting Error Flags..."
        for i in range(1,5):
            VfcHd.ResetGbtRxRdyLostFlagAppSfpGbtUserData(i, 1)
            VfcHd.ResetGbtRxRdyLostFlagAppSfpGbtUserData(i, 0)
            VfcHd.ResetRxDataErrFlagAppSfpGbtUserData(i, 1)
            VfcHd.ResetRxDataErrFlagAppSfpGbtUserData(i, 0)
    sleep(1)
    print
    print "-> GBT Link 1 (App. SFP1) (GX L0):"
    print "->"
    print "-> Reading pattern checker..."
    print "-> GBT RX Ready Lost Flag:     ", VfcHd.ReadGbtRxRdyLostFlagAppSfpGbtUserData(1)
    print "-> GBT RX Data Error Seen Flag:", VfcHd.ReadRxDataErrFlagAppSfpGbtUserData(1)
    print "->"
    print "-> Reading BERT..."
    Errors = VfcHd.ReadRxDataErrorCntAppSfpGbtUserData(1)
    print "-> Errors:                     ",Errors
    Frames = VfcHd.ReadRxDataFrameCntAppSfpGbtUserData(1)
    print "-> Frames Sent:                ",Frames
    Bert = (Errors+1.0)/(Frames+1.0)
    print "-> BERT:                       ",Bert
    print
    print "-> GBT Link 2 (App. SFP2) (GX L1):"
    print "->"
    print "-> Reading pattern checker..."
    print "-> GBT RX Ready Lost Flag:     ", VfcHd.ReadGbtRxRdyLostFlagAppSfpGbtUserData(2)
    print "-> GBT RX Data Error Seen Flag:", VfcHd.ReadRxDataErrFlagAppSfpGbtUserData(2)
    print "->"
    print "-> Reading BERT..."
    Errors = VfcHd.ReadRxDataErrorCntAppSfpGbtUserData(2)
    print "-> Errors:                     ",Errors
    Frames = VfcHd.ReadRxDataFrameCntAppSfpGbtUserData(2)
    print "-> Frames Sent:                ",Frames
    Bert = (Errors+1.0)/(Frames+1.0)
    print "-> BERT:                       ",Bert
    print
    print "-> GBT Link 3 (App. SFP3) (GX L2):"
    print "->"
    print "-> Reading pattern checker..."
    print "-> GBT RX Ready Lost Flag:     ", VfcHd.ReadGbtRxRdyLostFlagAppSfpGbtUserData(3)
    print "-> GBT RX Data Error Seen Flag:", VfcHd.ReadRxDataErrFlagAppSfpGbtUserData(3)
    print "->"
    print "-> Reading BERT..."
    Errors = VfcHd.ReadRxDataErrorCntAppSfpGbtUserData(3)
    print "-> Errors:                     ",Errors
    Frames = VfcHd.ReadRxDataFrameCntAppSfpGbtUserData(3)
    print "-> Frames Sent:                ",Frames
    Bert = (Errors+1.0)/(Frames+1.0)
    print "-> BERT:                       ",Bert
    print
    print "-> GBT Link 4 (App. SFP4) (GX L3):"
    print "->"
    print "-> Reading pattern checker..."
    print "-> GBT RX Ready Lost Flag:     ", VfcHd.ReadGbtRxRdyLostFlagAppSfpGbtUserData(4)
    print "-> GBT RX Data Error Seen Flag:", VfcHd.ReadRxDataErrFlagAppSfpGbtUserData(4)
    print "->"
    print "-> Reading BERT..."
    Errors = VfcHd.ReadRxDataErrorCntAppSfpGbtUserData(4)
    print "-> Errors:                     ",Errors
    Frames = VfcHd.ReadRxDataFrameCntAppSfpGbtUserData(4)
    print "-> Frames Sent:                ",Frames
    Bert = (Errors+1.0)/(Frames+1.0)
    print "-> BERT:                       ",Bert
    print
