//============================================================================================//
//################################   Test Bench Information   ################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: tb_BaseProject.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 22/06/16      1.0          A. Boccardi        First test bench definition.
//     - 13/11/17      1.1          J. Pospisil        Minor improvements; updated addresses
//
// Language: SystemVerilog
//
// Module Under Test: VFC-HD
//
// Description:
//
//     System level simulation of the VFC-HD.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module tb_BaseProject;
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

int c_VmeAccessVerbose = 1; // not a parameter to allow overwrite the value temporarily
localparam int c_VmeSlot = 4;

// Base Address:
localparam c_VfcBaseAddress = 32'h0100_0000 * c_VmeSlot;

// System Memory Addresses:
localparam  c_Sys_MemSpace     = 32'h0000_0000,  // Memory space of the full system block
            c_Sys_SysRevId     = 32'h0000_0000,  // System code Revision ID (YYMMDDxx)
            c_Sys_SysInfo      = 32'h0000_0004,  // Info of the System part (12 chars)
            c_Sys_AppRevId     = 32'h0000_0010,  // Application code Revision ID (YYMMDDxx)
            c_Sys_AppInfo      = 32'h0000_0014,  // Info of the Application part (44 chars)
            c_Sys_IntStatus    = 32'h0000_0040,  // Interrupt Status
            c_Sys_IntConfig    = 32'h0000_0044,  // Interrupt Configuration
            c_Sys_IntMask      = 32'h0000_0048,  // Interrupt source mask reg. (1 to disable the source)
            c_Sys_IntVector    = 32'h0000_004C,  // Interrupt Vector
            c_Sys_IntLevel     = 32'h0000_0050,  // Interrupt Level
            c_Sys_UidStatus    = 32'h0000_0060,  // Unique Id and Temperature controller Status
            c_Sys_UidCommand   = 32'h0000_0064,  // Unique Id and Temperature controller Command reg
            c_Sys_CtrlReg      = 32'h0000_0070,  // System Control Register (App Reset)
            c_Sys_I2cMAECntrl  = 32'h0000_0080,  // I2C mux and IO expander arbiter control reg
            c_Sys_ApSfp1Status = 32'h0000_00A0,  // Application SFP 1 status
            c_Sys_ApSfp2Status = 32'h0000_00A4,  // Application SFP 2 status
            c_Sys_ApSfp3Status = 32'h0000_00A8,  // Application SFP 3 status
            c_Sys_ApSfp4Status = 32'h0000_00AC,  // Application SFP 4 status
            c_Sys_EthSfpStatus = 32'h0000_00B0,  // Ethernet SFP status
            c_Sys_BstSfpStatus = 32'h0000_00B4,  // BST SFP status
            c_Sys_SpiStatus    = 32'h0000_00C0,  // Spi Master status (Vadj and ADC)
            c_Sys_SpiConfig1   = 32'h0000_00C4,  // Spi Master Config 1 (Vadj and ADC)
            c_Sys_SpiConfig2   = 32'h0000_00C8,  // Spi Master Config 1 (Vadj and ADC)
            c_Sys_SpiShiftOut  = 32'h0000_00CC,  // Spi Master Shift Out reg (Vadj and ADC)
            c_Sys_SpiShiftIn   = 32'h0000_00D0,  // Spi Master Shift In reg (Vadj and ADC)
            c_Sys_BstStatus    = 32'h0000_0800,  // Status of the SFP and CDR of the BST
            c_Sys_BstMessage   = 32'h0000_0C00,  // Full message received from the BST Master
            c_Sys_ConfSector   = 32'h0002_0000,  // Reconfiguration sector address
            c_Sys_ConfCommand  = 32'h0002_0004,  // Reconfiguration command
            c_Sys_ConfStatus   = 32'h0002_0008,  // Reconfiguration status
            c_Sys_ConfTrigger  = 32'h0002_000C,  // Reconfiguration trigger source
            c_Sys_ConfWdTimeou = 32'h0002_0010,  // Reconfiguration watchdog timeout
            c_Sys_ConfWdEnable = 32'h0002_0014,  // Reconfiguration watchdog enable
            c_Sys_ConfBoot     = 32'h0002_0018,  // Reconfiguration boot address
            c_Sys_ConfAnf      = 32'h0002_001C,  // Reconfiguration AnF flag
            c_Sys_ConfEeprom   = 32'h0003_0000;  // Reconfiguration EEPROM access

// Application Memory Addresses:
localparam c_AppAddrOffset     = 32'h0080_0000;                    // Address offset of the Application

localparam c_App_CtrlRegOffset             = c_AppAddrOffset + 32'h0000_0000 * 4; // WB address => *4
localparam c_App_StatRegOffset             = c_AppAddrOffset + 32'h0000_0004 * 4;
localparam c_App_EthSfpStatRegOffset       = c_AppAddrOffset + 32'h0000_0008 * 4;
localparam c_App_GbtRefClkSchOffset        = c_AppAddrOffset + 32'h0000_000C * 4;
localparam c_App_AppSfpGbtUsrDatCtrlOffset = c_AppAddrOffset + 32'h0000_0010 * 4;
localparam c_App_AppSfpGbtUsrDatStatOffset = c_AppAddrOffset + 32'h0000_0014 * 4;
localparam c_App_AppSfpGbtUsrDatBertOffset = c_AppAddrOffset + 32'h0000_0020 * 4;
localparam c_App_GbtCtrlOffset             = c_AppAddrOffset + 32'h0000_0080 * 4;
localparam c_App_I2cToWbOffset             = c_AppAddrOffset + 32'h0000_1000 * 4;
localparam c_App_Ddr3aOffset               = c_AppAddrOffset + 32'h0004_0000 * 4;
localparam c_App_Ddr3bOffset               = c_AppAddrOffset + 32'h0008_0000 * 4;

//==== Wires & Regs ====//
wire As_n;
wire [5:0] AM_b6;
wire [31:1] A_b31;
wire LWord;
wire [1:0] Ds_nb2;
wire Wr_n;
wire [31:0] D_b32;
wire DtAck_n;
wire [7:1] Irq_nb7;
wire Iack_n;
wire [20:1] IackIn_nb;
wire [20:1] IackOut_nb;
wire SysResetN_irn;
wire SysClk_k;
wire [ 4:0] Ga_nmb5 [20:1];
wire [20:1] Gap_nbm;
logic PushButton = 0;
wire [4:1] GpIoLemo_b4 = 0;
logic [7:0] LastVmeAccessExitCode_b8;
logic [31:0] LastVmeReadData_b32;
logic [7:0] DipSw_b8 = 8'h0;

//=====================================  Status & Control  ====================================//

task Report(string Text);
    $display("%s (@ %t)", Text, $time);
endtask;

task Read (input [31:0] Address_b32);
    i_VmeBus.ReadA32D32(c_VfcBaseAddress + Address_b32, LastVmeReadData_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
    assert (LastVmeAccessExitCode_b8 == i_VmeBus.c_ExitCode_OK) else
        $error("VME read error! Last VME read transaction returned non-zero exit code: 32'h%h", LastVmeAccessExitCode_b8);
endtask

task Write (input [31:0] Address_b32, input [31:0] Data_b32);
    i_VmeBus.WriteA32D32(c_VfcBaseAddress + Address_b32, Data_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
    assert (LastVmeAccessExitCode_b8 == i_VmeBus.c_ExitCode_OK) else
        $error("VME write error! Last VME write transaction returned non-zero exit code: 32'h%h", LastVmeAccessExitCode_b8);
endtask

task RMW (input [31:0] Address_b32, input [31:0] Mask_b32, input [31:0] Data_b32);
    // read, modify, write
    // Mask_b32 - which part of Data_b32 should be written (the rest will stay untouched)
    automatic logic [31:0] TmpData_b32;
    Read(Address_b32);
    TmpData_b32 = LastVmeReadData_b32 & (~Mask_b32);
    TmpData_b32 = TmpData_b32 | (Mask_b32 & Data_b32);
    Write(Address_b32, TmpData_b32);
endtask

task Reset();
    i_VmeBus.VmeReset(500, c_VmeAccessVerbose);
    repeat(20)@(posedge SysClk_k);
endtask

task CheckRegister(input [31:0] Address_b32, input [31:0] CorrectData);
    Read(Address_b32);
    assert (LastVmeReadData_b32 == CorrectData) else
        $error("Register check failed! [32'h%h] is 32'h%h and should be 32'h%h", Address_b32, LastVmeReadData_b32, CorrectData);
endtask

task TestSystem();
    Report("-> System Simulation");
    Read(c_Sys_SysRevId);
    $display("Gateware revision: %02x (%02x/%02x/%02x)", LastVmeReadData_b32[7:0], LastVmeReadData_b32[15:8], LastVmeReadData_b32[23:16], LastVmeReadData_b32[31:24]);
    Read(c_Sys_IntMask);
    Write(c_Sys_IntMask, 32'habcd_0001);
    Read(c_Sys_IntMask);
endtask

`define DDR3A_INIT_DONE 32'h00000001
`define DDR3A_CAL_SUCCESS 32'h00000002
`define DDR3A_CAL_FAIL 32'h00000004
`define DDR3B_INIT_DONE 32'h00000008
`define DDR3B_CAL_SUCCESS 32'h00000010
`define DDR3B_CAL_FAIL 32'h00000020
task TestDdr3();
    $display("%c[1;31m",27);
    Report("Testing DD3 chip A - Master");
    $display("%c[0m",27);

    $display("%c[1;31m",27);
    Report("DD3 Master: wait until ready");
    $display("%c[0m",27);
    do begin
        #50000;
        Read(c_App_StatRegOffset);
    end while((LastVmeReadData_b32 & `DDR3A_INIT_DONE) == 0);
    assert ((LastVmeReadData_b32 & (`DDR3A_CAL_SUCCESS | `DDR3A_CAL_FAIL)) == `DDR3A_CAL_SUCCESS)
        else $error("DDR Master calibration failed!");

    Report("DD3 Master: W/R test");
    for (int i=0 ; i<16; i=i+1) begin
        Write(c_App_Ddr3aOffset +  i*4, 32'hABCD0000+i);
        CheckRegister(c_App_Ddr3aOffset +  i*4, 32'hABCD0000+i);
    end
    for (int i=0 ; i<16; i=i+1) begin
        Write(c_App_Ddr3aOffset +  i*4, 32'hABCD0000+i);
    end
    for (int i=0 ; i<16; i=i+1) begin
        CheckRegister(c_App_Ddr3aOffset +  i*4, 32'hABCD0000+i);
    end

    repeat(50)@(posedge SysClk_k);
/*
    Report("Testing DD3 chip B - Slave");

    Report("DD3 Slave: wait until ready");
    do begin
        #50000;
        Read(c_App_StatRegOffset);
    end while((LastVmeReadData_b32 & `DDR3B_INIT_DONE) == 0);
    assert ((LastVmeReadData_b32 & (`DDR3B_CAL_SUCCESS | `DDR3B_CAL_FAIL)) == `DDR3B_CAL_SUCCESS)
        else $error("DDR Slave calibration failed!");

    Report("DD3 Slave: W/R test");
    Write(c_App_Ddr3bOffset + 12, 32'hBABE0001);
    CheckRegister(c_App_Ddr3bOffset + 12, 32'hBABE0001);

    repeat(50)@(posedge SysClk_k);
*/
    Report("DDR3: test end");
endtask

task TestApplication();
    Report("-> Application Simulation");
    Read(c_Sys_AppRevId);
    $display("Gateware revision: %02x (%02x/%02x/%02x)", LastVmeReadData_b32[7:0], LastVmeReadData_b32[15:8], LastVmeReadData_b32[23:16], LastVmeReadData_b32[31:24]);

    Read(c_App_CtrlRegOffset);
    Read(c_App_CtrlRegOffset+4'h4);
    Read(c_App_CtrlRegOffset+4'h8);
    Read(c_App_CtrlRegOffset+4'hC);

    repeat(1000)@(posedge SysClk_k);

    TestDdr3();

    #10000;
    $stop();
endtask

//==== Stimulus & Display ====//
defparam    i_VfcHdSlot1.i_Ddr1.DEBUG=0;
defparam    i_VfcHdSlot1.i_Ddr2.DEBUG=0;

initial begin
    Reset();
    TestSystem();
    TestApplication();
end

always begin
    #1ms Report("Testbench timeouted!");
    $stop();
end



//=== VME bus and transaction models ====//
VmeBusModule #(.g_NSlots(20), .g_VerboseDefault(1))
    i_VmeBus(
        .As_n,
        .AM_b6,
        .A_b31,
        .LWord,
        .Ds_nb2,
        .Wr_n,
        .D_b32,
        .DtAck_n,
        .Irq_nb7,
        .Iack_n,
        .IackIn_nb,
        .IackOut_nb,
        .SysResetN_irn,
        .SysClk_k,
        .Ga_nmb5,
        .Gap_nbm);

//==== VFC ====//
VfcHd_v3_0
    i_VfcHdSlot1(
        // VME interface:
        .As_in(As_n),
        .AM_ib6(AM_b6),
        .A_iob31(A_b31),
        .LWord_io(LWord),
        .Ds_inb2(Ds_nb2),
        .Wr_in(Wr_n),
        .D_iob32(D_b32),
        .DtAck_on(DtAck_n),
        .Irq_onb7(Irq_nb7),
        .Iack_in(Iack_n),
        .IackIn_in(IackIn_nb[c_VmeSlot]),
        .IackOut_on(IackOut_nb[c_VmeSlot]),
        .SysResetN_irn(SysResetN_irn),
        .SysClk_ik(SysClk_k),
        .Ga_ionb5(Ga_nmb5[c_VmeSlot]),
        .Gap_ion(Gap_nbm[c_VmeSlot]),
        // FMC Connector:
        .FmcLaP_iob34(),
        .FmcLaN_iob34(),
        .FmcHaP_iob24(),
        .FmcHaN_iob24(),
        .FmcHbP_iob22(),
        .FmcHbN_iob22(),
        .FmcPrsntM2c_in(1'b1),
        .FmcTck_ok(),
        .FmcTms_o(),
        .FmcTdi_o(),
        .FmcTdo_i(1'b0),
        .FmcTrstL_orn(),
        .FmcScl_iok(),
        .FmcSda_io(),
        .FmcPgM2c_i(1'b1),
        .FmcPgC2m_o(),
        .FmcClk0M2cCmos_ik(1'b0),
        .FmcClk1M2cCmos_ik(1'b0),
        .FmcClk2Bidir_iok(),
        .FmcClk3Bidir_iok(),
        .FmcClkDir_i(1'b0),
        .FmcDpC2m_ob10(),
        .FmcDpM2c_ib10(10'h0),
        .FmcGbtClk0M2c_ik(1'b0),
        .FmcGbtClk1M2c_ik(1'b0),
        .FmcGa0_o(),
        .FmcGa1_o(),
        // Miscellaneous:
        .DipSw_ib8(DipSw_b8),
        .PushButton_i(PushButton),
        .GpIoLemo_iob4(GpIoLemo_b4));


endmodule
