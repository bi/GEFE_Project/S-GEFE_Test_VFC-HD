#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Manoel Barros Marin (CERN BE-BI-QP) 02/03/2016
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Cleaning the transcript window:
.main clear

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

do optimize.do
vsim -gui _design_optimized {*}$VSIM_OPT
do wave.do
do ../../../../../libs/BI_HDL_Cores/cores_for_simulation/vme_bus_module/InteractiveVmeTransactions.tcl
run -all
