//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: VfcHdApplication.sv
//
// Language: SystemVerilog 2013
//
// Targeted device:
//
//     - Vendor:  Intel FPGA (Altera)
//     - Model:   Arria V GX
//
// Description:
//
//     Application specific gateware targeted to the VFC-HD v3
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module VfcHdApplication
//====================================  Global Parameters  ===================================//
#(  parameter      g_ApplicationVersion_b8      = 8'h00, // Comment: Updated by script during implementation
                   g_ApplicationReleaseDay_b8   = 8'h00, //
                   g_ApplicationReleaseMonth_b8 = 8'h00, //
                   g_ApplicationReleaseYear_b8  = 8'h00, //
    parameter      g_AppInfo_b352 = "LAB_DEV%VFC-HD_BASE%PROJECT_TEMPLATE%DDR3") // Comment: Up to 352 bits -> 44 ASCII Characters (44 Bytes)
//========================================  I/O ports  =======================================//
(
    //==== External Ports ====//

    // BST SFP MGT Lanes:
    input          BstSfpRx_i,   // Comment: Differential signal
    output         BstSfpTx_o,   // Comment: Differential signal
    // WhiteRabbit/Ethernet SFP MGT Lanes:
    input          EthSfpRx_i,   // Comment: Differential
    output         EthSfpTx_o,   // Comment: Differential
    // Application SFP MGT Lanes:
    input  [  4:1] AppSfpRx_ib4, // Comment: Differential signal
    output [  4:1] AppSfpTx_ob4, // Comment: Differential signal
    // DDR3:
    output         Ddr3aCk_ok,
    output         Ddr3aCk_okn,
    output         Ddr3aCke_o,
    output         Ddr3aReset_orn,
    output         Ddr3aRas_on,
    output         Ddr3aCas_on,
    output         Ddr3aCs_on,
    output         Ddr3aWe_on,
    output         Ddr3aOdt_o,
    output [  2:0] Ddr3aBa_ob3,
    output [ 15:0] Ddr3aAdr_ob16,
    output [  1:0] Ddr3aDm_ob2,
    inout  [  1:0] Ddr3aDqs_iob2,
    inout  [  1:0] Ddr3aDqs_iob2n,
    inout  [ 15:0] Ddr3aDq_iob16,
    //--
    output         Ddr3bCk_ok,
    output         Ddr3bCk_okn,
    output         Ddr3bCke_o,
    output         Ddr3bReset_orn,
    output         Ddr3bRas_on,
    output         Ddr3bCas_on,
    output         Ddr3bCs_on,
    output         Ddr3bWe_on,
    output         Ddr3bOdt_o,
    output [  2:0] Ddr3bBa_ob3,
    output [ 15:0] Ddr3bAdr_ob16,
    output [  1:0] Ddr3bDm_ob2,
    inout  [  1:0] Ddr3bDqs_iob2,
    inout  [  1:0] Ddr3bDqs_iob2n,
    inout  [ 15:0] Ddr3bDq_iob16,
    // TestIo MMCX Connectors:
    inout          TestIo1_io,
    inout          TestIo2_io,
    // FMC HPC Connector:
    output         VfmcEnable_oen,
    input          FmcPrsntM2c_in,
    input          FmcPgM2c_i,
    output         FmcPgC2m_o,
    output         FmcTck_ok,
    output         FmcTms_o,
    output         FmcTdi_o,
    input          FmcTdo_i,
    output         FmcTrstL_orn,
    inout          FmcScl_iok,
    inout          FmcSda_io,
    input          FmcClkDir_i,
    input          FmcClk0M2cCmos_ik,
    input          FmcClk1M2cCmos_ik,
    input          FmcClk2Bidir_iok,      // Comment: Differential signal
//  output         FmcClk2Bidir_iok,      // Comment: Differential signal
    input          FmcClk3Bidir_iok,      // Comment: Differential signal
//  output         FmcClk3Bidir_iok,      // Comment: Differential signal
    input          FmcGbtClk0M2cLeft_ik,  // Comment: Differential signal
    input          FmcGbtClk1M2cLeft_ik,  // Comment: Differential signal
    input          FmcGbtClk0M2cRight_ik, // Comment: Differential signal
    input          FmcGbtClk1M2cRight_ik, // Comment: Differential signal
    inout  [ 33:0] FmcLaP_iob34,
    inout  [ 33:0] FmcLaN_iob34,
    inout  [ 23:0] FmcHaP_iob24,
    inout  [ 23:0] FmcHaN_iob24,
    inout  [ 21:0] FmcHbP_iob22,
    inout  [ 21:0] FmcHbN_iob22,
    output [  9:0] FmcDpC2m_ob10, // Comment: Differential signal
    input  [  9:0] FmcDpM2c_ib10, // Comment: Differential signal
    // Clocks Scheme:
    output         OeSi57x_oe,
    input          Si57xClk_ik,
    input  [  3:0] ClkFb_ikb4,
    output [  3:0] ClkFb_okb4,
    input          Clk20VCOx_ik,
    output         PllDac20Sync_o,
    output         PllDac25Sync_o,
    output         PllDacSclk_ok,
    output         PllDacDin_o,
    inout          PllRefSda_ioz,
    inout          PllRefScl_iokz,
    input          PllRefInt_i,
    output         PllSourceMuxOut_ok,
    input          PllRefClkOut_ik,   // Comment: Differential reference for the MGT lines
    input          GbitTrxClkRefR_ik, // Comment: Differential reference for the MGT lines (~125MHz)
    // DIP Switch (SW1):
    input  [  8:1] DipSw_ib8,
    // P2 RTM:
    inout  [ 19:0] P2DataP_iob20, //Comment: The 0 is a clock capable input
    inout  [ 19:0] P2DataN_iob20,
    // P0 Timing:
    input  [  7:0] P0HwHighByte_ib8,
    input  [  7:0] P0HwLowByte_ib8,
    output         DaisyChain1Cntrl_o,
    output         DaisyChain2Cntrl_o,
    input          VmeP0BunchClk_ik,
    input          VmeP0Tclk_ik,
    // GPIO LEMO Connectors:
    inout  [  4:1] GpIo_iob4,
    // Miscellaneous:
    input          PushButtonN_in,
    // WhiteRabbit PROM
    inout          WrPromSda_io,
    output         WrPromScl_ok,

    //==== System-Application interface ====//

    // Reference Clock for the WishBone Interface
    input          Sys125MhzClk_ik,
    // Application & Release IDs:
    output [  7:0] AppVersion_ob8, AppReleaseDay_ob8, AppReleaseMonth_ob8, AppReleaseYear_ob8,
    output [351:0] AppInfo_ob352,
    // Resets Scheme:
    input          AppReset_iqr,
    // WishBone Bus Interface:
    input          WbMasterCyc_i,
    input          WbMasterStb_i,
    input  [ 24:0] WbMasterAdr_ib25,  // Comment: The actual width of the address bus is set to 21 for the moment: top bits stuck to 0
    input          WbMasterWr_i,
    input  [ 31:0] WbMasterDat_ib32,
    output [ 31:0] WbMasterDat_ob32,
    output         WbMasterAck_o,
    output         WbSlaveCyc_o,
    output         WbSlaveStb_o,
    output [ 24:0] WbSlaveAdr_ob25,
    output         WbSlaveWr_o,
    output [ 31:0] WbSlaveDat_ob32,
    input  [ 31:0] WbSlaveDat_ib32,
    input          WbSlaveAck_i,
    // LED Control:
    output [  8:1] Led_ob8,
    // BST Interface:
    input          BstOn_i,
    input          BstClk_ik,
    input          BunchClkFlag_i,
    input          TurnClkFlag_i,
    output [ 11:0] TurnClkFlagDly_ob12,
    input  [  7:0] BstByteAddress_ib8,
    input  [  7:0] BstByte_ib8,
    input          BstByteStrobe_i,
    input          BstByteError_i,
    // Interrupt Request:
    output [ 23:0] InterruptRequest_ob24,
    // GPIO Direction Control:
    output         GpIo1DirOut_o,  // Comment: Output -> 1'b1 | Input -> 1'b0
    input          GpIo1DirOutStatus_i,
    output         GpIo2DirOut_o,  // Comment: Output -> 1'b1 | Input -> 1'b0
    input          GpIo2DirOutStatus_i,
    output         GpIo34DirOut_o, // Comment: Output -> 1'b1 | Input -> 1'b0
    input          GpIo34DirOutStatus_i,
    output         GpIo1EnTerm_o,  // Comment: Enable -> 1'b1 | Disable -> 1'b0
    input          GpIo1EnTermStatus_i,
    output         GpIo2EnTerm_o,  // Comment: Enable -> 1'b1 | Disable -> 1'b0
    input          GpIo2EnTermStatus_i,
    output         GpIo3EnTerm_o,  // Comment: Enable -> 1'b1 | Disable -> 1'b0
    input          GpIo3EnTermStatus_i,
    output         GpIo4EnTerm_o,  // Comment: Enable -> 1'b1 | Disable -> 1'b0
    input          GpIo4EnTermStatus_i,
    // BLMIn P0 value:
    input  [  7:0] BlmIn_ib8,
    // AppSfp1 Control:
    input          AppSfp1Present_iq,
    input          AppSfp1TxFault_iq,
    input          AppSfp1Los_iq,
    output         AppSfp1TxDisable_o,
    output         AppSfp1RateSelect_o,
    input          StatusAppSfp1TxDisable_iq,
    input          StatusAppSfp1RateSelect_iq,
    // AppSfp2 Control:
    input          AppSfp2Present_iq,
    input          AppSfp2TxFault_iq,
    input          AppSfp2Los_iq,
    output         AppSfp2TxDisable_o,
    output         AppSfp2RateSelect_o,
    input          StatusAppSfp2TxDisable_iq,
    input          StatusAppSfp2RateSelect_iq,
    // AppSfp3 Control:
    input          AppSfp3Present_iq,
    input          AppSfp3TxFault_iq,
    input          AppSfp3Los_iq,
    output         AppSfp3TxDisable_o,
    output         AppSfp3RateSelect_o,
    input          StatusAppSfp3TxDisable_iq,
    input          StatusAppSfp3RateSelect_iq,
    // AppSfp4 Control:
    input          AppSfp4Present_iq,
    input          AppSfp4TxFault_iq,
    input          AppSfp4Los_iq,
    output         AppSfp4TxDisable_o,
    output         AppSfp4RateSelect_o,
    input          StatusAppSfp4TxDisable_iq,
    input          StatusAppSfp4RateSelect_iq,
    // BstSfp Control:
    input          BstSfpPresent_iq,
    input          BstSfpTxFault_iq,
    input          BstSfpLos_iq,
    output         BstSfpTxDisable_o,
    output         BstSfpRateSelect_o,
    input          StatusBstSfpTxDisable_iq,
    input          StatusBstSfpRateSelect_iq,
    // EthSfp Control:
    input          EthSfpPresent_iq,
    input          EthSfpTxFault_iq,
    input          EthSfpLos_iq,
    output         EthSfpTxDisable_o,
    output         EthSfpRateSelect_o,
    input          StatusEthSfpTxDisable_iq,
    input          StatusEthSfpRateSelect_iq,
    // CDR Control:
    input          CdrLos_iq,
    input          CdrLol_iq,
    //==== WishBone interface for the I2C slaves on the Muxes ====//
    output         I2cWbCyc_o,
    output         I2cWbStb_o,
    output         I2cWbWe_o,
    output  [11:0] I2cWbAdr_ob12,
    output  [ 7:0] I2cWbDat_ob8,
    input   [ 7:0] I2cWbDat_ib8,
    input          I2cWbAck_i,
    //==== OCT control sharing ====//
    input   [15:0] OctSeriesControl_ib16,
    input   [15:0] OctParallelControl_ib16
);

//=======================================  Declarations  =====================================//

genvar       i, j;
// Clocks Scheme:
wire         a_Clk_k;
// LEDs:
reg  [ 24:0] LedCounter_c25 = 0;
reg  [  8:1] Led_b8         = 8'h01;
// WishBone Address Decoder:
wire         WbStbCtrlReg, WbAckCtrlReg;
wire [ 31:0] WbDatCtrlReg_b32;
wire         WbStbStatReg, WbAckStatReg;
wire [ 31:0] WbDatStatReg_b32;
wire         WbStbEthSfpStatReg, WbAckEthSfpStatReg;
wire [ 31:0] WbDatEthSfpStatReg_b32;
wire         WbStbGbtRefClkSch, WbAckGbtRefClkSch;
wire [ 31:0] WbDatGbtRefClkSch_b32;
wire         WbStbAppSfpGbtUserDataCtrlReg, WbAckAppSfpGbtUserDataCtrlReg;
wire [ 31:0] WbDatAppSfpGbtUserDataCtrlReg_b32;
wire         WbStbAppSfpGbtUserDataStatReg, WbAckAppSfpGbtUserDataStatReg;
wire [ 31:0] WbDatAppSfpGbtUserDataStatReg_b32;
wire         WbStbAppSfpGbtUserDataBertReg, WbAckAppSfpGbtUserDataBertReg;
wire [ 31:0] WbDatAppSfpGbtUserDataBertReg_b32;
wire         WbStbGbtCtrl, WbAckGbtCtrl;
wire [ 31:0] WbDatGbtCtrl_b32;
wire         WbMasterI2cStb, WbMasterI2cAck;
wire [  7:0] WbMasterI2cDat_b8;
wire         WbMasterDdr3aStb, WbMasterDdr3aAck;
wire [ 31:0] WbMasterDdr3aDat_b32;
wire         WbMasterDdr3bStb, WbMasterDdr3bAck;
wire [ 31:0] WbMasterDdr3bDat_b32;
// Control Registers:
wire [ 31:0] CtrlReg0Value_b32;
wire [ 31:0] CtrlReg1Value_b32;
wire [ 31:0] CtrlReg2Value_b32;
// Status Registers
logic [31:0] Ddr3StatusReg_b32;
// WhiteRabbit:
wire         EthSfpIdValid;
wire [127:0] EthSfpPN_b128;
wire         EthSfpWbMasterCyc, EthSfpWbMasterStb, EthSfpWbMasterAck;
wire [ 11:0] EthSfpWbMasterAddr_b12;
wire [  7:0] EthSfpWbMasterDataMiSo_b8;
// GBT-FPGA:
wire         GbtTxFrameClk40MHz_k; // Comment: Note!! TX logic of ALL GBT Links must be clocked by GbtTxFrameClk40Mhz_k (a_GbtUserClk_k)
wire         a_GbtUserClk_k;       // Comment: (a_GbtUserClk_k = GbtTxFrameClk40MHz_k)
wire         GbtReset_r;
//--
wire [  4:1] GbtRxFrameClk40Mhz_kb4; // Comment: Note!! RX logic of each GBT Link may be clocked by its dedicated GbtRxFrameClk40Mhz_kb4 or in some cases by GbtTxFrameClk40MHz_k (a_GbtUserClk_k)
wire [  1:0] TxDataScIcAppSfpGbtUserData_4b2 [4:1];
wire [  1:0] TxDataScEcAppSfpGbtUserData_4b2 [4:1];
wire [ 79:0] TxDataAppSfpGbtUserData_4b80    [4:1];
wire [  1:0] RxDataScIcAppSfpGbtUserData_4b2 [4:1];
wire [  1:0] RxDataScEcAppSfpGbtUserData_4b2 [4:1];
wire [ 79:0] RxDataAppSfpGbtUserData_4b80    [4:1];
wire [  4:1] GbtRxReadyAppSfpGbtUserData_b4;
wire [  4:1] TxIsDataSelAppSfpGbtUserData_b4;
wire [  4:1] RxIsDataFlagAppSfpGbtUserData_b4;
wire         TxMatchFlagAppSfpGbtUserData;
reg  [  4:1] RxMatchFlagAppSfpGbtUserData_b4;
//--
wire [  1:0] TestPatterSelAppSfpGbtUserData_b2;
wire [ 83:0] TxDataAppSfpGbtUserData_b84;
wire         ErrInjAppSfpGbtUserData;
reg  [  1:0] ErrInjAppSfpGbtUserData_qb2;
reg          ErrInjAppSfpGbtUserData_q;
reg  [ 83:0] TxDataErrInjAppSfpGbtUserData_qb84;
//--
wire [  4:1] ScEcCheckEnAppSfpGbtUserData_b4;
wire [  4:1] TogglingDataCheckSyncAppSfpGbtUserData_b4;
wire [  4:1] ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4;
wire [  4:1] ResetGbtRxDataErrorsAppSfpGbtUserData_b4;
wire [  4:1] GbtRxRdyLostFlagAppSfpGbtUserData_b4;
wire [  4:1] RxDataErrFlagAppSfpGbtUserData_b4;
wire [ 63:0] RxDataErrorCntAppSfpGbtUserData_64b4 [4:1];
wire [ 63:0] RxDataFrameCntAppSfpGbtUserData_64b4 [4:1];
//--
wire [ 31:0] AppSfpGbtUserDataCtrlRegs_b32;
reg  [ 31:0] AppSfpGbtUserDataCtrlRegs_x2b32 [1:0];
reg  [ 31:0] AppSfpGbtUserDataCtrlRegs_qb32;
//--
wire [ 31:0] AppSfpGbtUserDataStatRegs_4b32 [3:0];
reg  [  1:0] GbtRxReadyAppSfpGbtUserData_x2b4 [4:1];
reg  [  4:1] GbtRxReadyAppSfpGbtUserData_qb4;
reg  [  1:0] RxIsDataFlagAppSfpGbtUserData_x2b4 [4:1];
reg  [  4:1] RxIsDataFlagAppSfpGbtUserData_qb4;
reg  [  1:0] GbtRxRdyLostFlagAppSfpGbtUserData_4xb2 [4:1];
reg  [  4:1] GbtRxRdyLostFlagAppSfpGbtUserData_qb4;
reg  [  1:0] RxDataErrFlagAppSfpGbtUserData_4xb2 [4:1];
reg  [  4:1] RxDataErrFlagAppSfpGbtUserData_qb4;
//--
wire [ 31:0] AppSfpGbtUserDataBertRegs_16b32 [15:0];
// Signals Forwarding:
reg          TestIo1;
reg          TestIo2;

//===================================  Application Logic  ====================================//

//==== Application Revision ID & Information ====//

assign AppVersion_ob8      = g_ApplicationVersion_b8     [7:0];
assign AppReleaseDay_ob8   = g_ApplicationReleaseDay_b8  [7:0];
assign AppReleaseMonth_ob8 = g_ApplicationReleaseMonth_b8[7:0];
assign AppReleaseYear_ob8  = g_ApplicationReleaseYear_b8 [7:0];
assign AppInfo_ob352       = {g_AppInfo_b352, {352-$size(g_AppInfo_b352){1'b0}}};

//==== Fixed Assignments ====//

assign OeSi57x_oe     = 1'b1;
assign VfmcEnable_oen = 1'b1; // FMC voltages disabled
assign FmcPgC2m_o     = ~VfmcEnable_oen;

//==== Clocks Scheme ====//

assign a_Clk_k        = Sys125MhzClk_ik;
assign a_GbtUserClk_k = GbtTxFrameClk40MHz_k;

//==== LEDs ====//

always @(posedge a_Clk_k) LedCounter_c25 <= #1 LedCounter_c25 + 1'b1;
always @(posedge a_Clk_k) if (&LedCounter_c25) Led_b8 <= #1 {Led_b8[1], Led_b8[8:2]};
assign Led_ob8 = {Led_b8[8:5], Led_b8[1], Led_b8[2], Led_b8[3], Led_b8[4]};

//==== WishBone Address Decoder ====//

AddrDecoderWbApp i_AddrDecoderWbApp (
    .Clk_ik                           (a_Clk_k),
    .Adr_ib21                         (WbMasterAdr_ib25[20:0]),
    .Stb_i                            (WbMasterStb_i),
    .Dat_oqb32                        (WbMasterDat_ob32),
    .Ack_oq                           (WbMasterAck_o),
    //--
    .DatCtrlReg_ib32                  (WbDatCtrlReg_b32),
    .AckCtrlReg_i                     (WbAckCtrlReg),
    .StbCtrlReg_oq                    (WbStbCtrlReg),
    //--
    .DatStatReg_ib32                  (WbDatStatReg_b32),
    .AckStatReg_i                     (WbAckStatReg),
    .StbStatReg_oq                    (WbStbStatReg),
    //--
    .DatEthSfpStatReg_ib32            (WbDatEthSfpStatReg_b32),
    .AckEthSfpStatReg_i               (WbAckEthSfpStatReg),
    .StbEthSfpStatReg_oq              (WbStbEthSfpStatReg),
    //--
    .DatGbtRefClkSch_ib32             (WbDatGbtRefClkSch_b32),
    .AckGbtRefClkSch_i                (WbAckGbtRefClkSch),
    .StbGbtRefClkSch_oq               (WbStbGbtRefClkSch),
    //--
    .DatAppSfpGbtUserDataCtrlReg_ib32 (WbDatAppSfpGbtUserDataCtrlReg_b32),
    .AckAppSfpGbtUserDataCtrlReg_i    (WbAckAppSfpGbtUserDataCtrlReg),
    .StbAppSfpGbtUserDataCtrlReg_oq   (WbStbAppSfpGbtUserDataCtrlReg),
    //--
    .DatAppSfpGbtUserDataStatReg_ib32 (WbDatAppSfpGbtUserDataStatReg_b32),
    .AckAppSfpGbtUserDataStatReg_i    (WbAckAppSfpGbtUserDataStatReg),
    .StbAppSfpGbtUserDataStatReg_oq   (WbStbAppSfpGbtUserDataStatReg),
    //--
    .DatAppSfpGbtUserDataBertReg_ib32 (WbDatAppSfpGbtUserDataBertReg_b32),
    .AckAppSfpGbtUserDataBertReg_i    (WbAckAppSfpGbtUserDataBertReg),
    .StbAppSfpGbtUserDataBertReg_oq   (WbStbAppSfpGbtUserDataBertReg),
    //--
    .DatGbtCtrl_ib32                  (WbDatGbtCtrl_b32),
    .AckGbtCtrl_i                     (WbAckGbtCtrl),
    .StbGbtCtrl_oq                    (WbStbGbtCtrl),
    //--
    .DatI2cToWb_ib32                  ({24'h0, WbMasterI2cDat_b8}),
    .AckI2cToWb_i                     (WbMasterI2cAck),
    .StbI2cToWb_oq                    (WbMasterI2cStb),
    //--
    .DatDdr3a_ib32                    (WbMasterDdr3aDat_b32),
    .AckDdr3a_i                       (WbMasterDdr3aAck),
    .StbDdr3a_oq                      (WbMasterDdr3aStb),
    //--
    .DatDdr3b_ib32                    (WbMasterDdr3bDat_b32),
    .AckDdr3b_i                       (WbMasterDdr3bAck),
    .StbDdr3b_oq                      (WbMasterDdr3bStb)
);

//==== Example Registers ====//

// Control Registers Bank:
Generic4OutputRegs #(
    .Reg0Default     (32'hBABE0000),
    .Reg0AutoClrMask (32'hFFFFFFFF),
    .Reg1Default     (32'h00000000),
    .Reg1AutoClrMask (32'hFFFFFFFF),
    .Reg2Default     (32'h00000000),
    .Reg2AutoClrMask (32'hFFFFFFFF),
    .Reg3Default     (32'h00000000),
    .Reg3AutoClrMask (32'hFFFFFFFF))
i_ControlRegs (
    .Clk_ik          (a_Clk_k),
    .Rst_irq         (AppReset_iqr),
    .Cyc_i           (WbMasterCyc_i),
    .Stb_i           (WbStbCtrlReg),
    .We_i            (WbMasterWr_i),
    .Adr_ib2         (WbMasterAdr_ib25[1:0]),
    .Dat_ib32        (WbMasterDat_ib32),
    .Dat_oab32       (WbDatCtrlReg_b32),
    .Ack_oa          (WbAckCtrlReg),
    //--
    .Reg0Value_ob32  (CtrlReg0Value_b32),
    .Reg1Value_ob32  (CtrlReg1Value_b32),
    .Reg2Value_ob32  (CtrlReg2Value_b32),
    .Reg3Value_ob32  ());

// Status Registers Bank:
Generic4InputRegs i_StatusRegs (
    .Clk_ik         (a_Clk_k),
    .Rst_irq        (AppReset_iqr),
    .Cyc_i          (WbMasterCyc_i),
    .Stb_i          (WbStbStatReg),
    .Adr_ib2        (WbMasterAdr_ib25[1:0]),
    .Dat_oab32      (WbDatStatReg_b32),
    .Ack_oa         (WbAckStatReg),
    //--
    .Reg0Value_ib32 (Ddr3StatusReg_b32),
    .Reg1Value_ib32 (32'hACDCDEAD),
    .Reg2Value_ib32 (32'hBEEFCAFE),
    .Reg3Value_ib32 (32'h0));

//==== WhiteRabbit ====//

// Comment: Note!! The WhiteRabbit interface is still to be added...

// Ethernet/WhiteRabbit SFP ID Reader:
SfpIdReader #(
    .g_SfpWbBaseAddress (16'ha00),
    .g_WbAddrWidth (     12))
i_EthSfpVendorIdReader (
    .Clk_ik        (a_Clk_k),
    .SfpPlugged_i  (EthSfpPresent_iq),
    .SfpIdValid_o  (EthSfpIdValid),
    .SfpPN_ob128   (EthSfpPN_b128),
    .WbCyc_o       (EthSfpWbMasterCyc),
    .WbStb_o       (EthSfpWbMasterStb),
    .WbAddr_ob     (EthSfpWbMasterAddr_b12),
    .WbData_ib8    (EthSfpWbMasterDataMiSo_b8),
    .WbAck_i       (EthSfpWbMasterAck));

// Dual Master Arbiter for the I2C Slaves on the I2C Muxes:
WbBus2M1S #(
    .g_DataWidth ( 8),
    .g_AddrWidth (12))
i_WbBus2M1S(
    .Clk_ik      (a_Clk_k),
    .CycM1_i     (EthSfpWbMasterCyc),
    .StbM1_i     (EthSfpWbMasterStb),
    .WeM1_i      (1'b0),
    .AddrM1_ib   (EthSfpWbMasterAddr_b12),
    .DataM1_ib   (/* Not connected */),
    .AckM1_o     (EthSfpWbMasterAck),
    .DataM1_ob   (EthSfpWbMasterDataMiSo_b8),
    .CycM2_i     (WbMasterCyc_i),
    .StbM2_i     (WbMasterI2cStb),
    .WeM2_i      (WbMasterWr_i),
    .AddrM2_ib   (WbMasterAdr_ib25[11:0]),
    .DataM2_ib   (WbMasterDat_ib32[ 7:0]),
    .AckM2_o     (WbMasterI2cAck),
    .DataM2_ob   (WbMasterI2cDat_b8),
    .CycS_o      (I2cWbCyc_o),
    .StbS_o      (I2cWbStb_o),
    .WeS_o       (I2cWbWe_o),
    .AddrS_ob    (I2cWbAdr_ob12),
    .DataS_ob    (I2cWbDat_ob8),
    .AckS_i      (I2cWbAck_i),
    .DataS_ib    (I2cWbDat_ib8));

// Ethernet/WhiteRabbit SFP Status Registers Bank:
Generic4InputRegs i_EthSfpStatusRegs (
    .Clk_ik         (a_Clk_k),
    .Rst_irq        (AppReset_iqr),
    .Cyc_i          (WbMasterCyc_i),
    .Stb_i          (WbStbEthSfpStatReg),
    .Adr_ib2        (WbMasterAdr_ib25[1:0]),
    .Dat_oab32      (WbDatEthSfpStatReg_b32),
    .Ack_oa         (WbAckEthSfpStatReg),
    //--
    .Reg0Value_ib32 (EthSfpPN_b128[ 31: 0]),
    .Reg1Value_ib32 (EthSfpPN_b128[ 63:32]),
    .Reg2Value_ib32 (EthSfpPN_b128[ 95:64]),
    .Reg3Value_ib32 (EthSfpPN_b128[127:96]));

//==== GBT-FPGA ====//

// GBT Reference Clock Scheme:
GbtRefClkScheme #(
    .g_BstRefClkSchemeEn       (1'b0)) // Comment: Note!! BST Ref Clk fails after adding DDR3 (To be fixed!!!)
i_GbtRefClkScheme (
    // WishBone Interface:
    .Clk_ik                    (a_Clk_k),
    .Rst_ir                    (AppReset_iqr),
    .Cyc_i                     (WbMasterCyc_i),
    .Stb_i                     (WbStbGbtRefClkSch),
    .We_i                      (WbMasterWr_i),
    .Adr_ib2                   (WbMasterAdr_ib25[1:0]),
    .Dat_ib32                  (WbMasterDat_ib32),
    .Dat_ob32                  (WbDatGbtRefClkSch_b32),
    .Ack_o                     (WbAckGbtRefClkSch),
    // GBT MGT Reference Clock (PllRef (Si5338)) I2C Interface:
    .PllRefSda_ioz             (PllRefSda_ioz),
    .PllRefScl_iokz            (PllRefScl_iokz),
    // BST Control:
    .BstSfpPresent_i           (BstSfpPresent_iq),
    .BstSfpTxFault_i           (BstSfpTxFault_iq),
    .BstSfpLos_i               (BstSfpLos_iq),
    .StatusBstSfpTxDisable_i   (StatusBstSfpTxDisable_iq),
    .StatusBstSfpRateSelect_i  (StatusBstSfpRateSelect_iq),
    .BstSfpTxDisable_o         (BstSfpTxDisable_o),  // Comment: The BST SFP TX is set to DISABLED to switch off the laser, since it is only used as RX
    .BstSfpRateSelect_o        (BstSfpRateSelect_o), // Comment: Line rate select to low bandwidth (Line Rate < 4.25 Gbps)
    .BstOn_i                   (BstOn_i),
    .BunchClkFlag_i            (BunchClkFlag_i),
    .SelBstClk_i               (1'b0), // Comment: Note!! In the VFC-HD Base project, SelBstClk is only controlled through WishBone
    .SyncTxFrameClkPll_i       (1'b0), // Comment: Note!! In the VFC-HD Base project, SyncTxFrameClkPll is only controlled through WishBone
    .LockedFreeRunningPll_oa   (),     // Comment: Note!! In the VFC-HD Base project, LockedFreeRunningPll is only monitored through WishBone
    .LockedGbtTxFrameClkPll_oa (),     // Comment: Note!! In the VFC-HD Base project, LockedGbtTxFrameClkPll is only monitored through WishBone
    // Input Clocks:
    .FreeRunningClk20MHz_ik    (Clk20VCOx_ik),
    .FreeRunningClk160MHzFb_ik (ClkFb_ikb4[0]),
    .BstClk_ik                 (BstClk_ik),
    .BstClkFb_ik               (ClkFb_ikb4[1]),
    .GbtRefClk120MHz_ik        (PllRefClkOut_ik),
    // Output Clocks:
    .FreeRunningClk160MHzFb_ok (ClkFb_okb4[0]),
    .BstClkFb_ok               (ClkFb_okb4[1]),
    .BstOrFreeRunningMuxClk_ok (PllSourceMuxOut_ok),
    .TxFrameClk40Mhz_ok        (GbtTxFrameClk40MHz_k), // Comment: Note!! TX logic of ALL GBT Links must be clocked by GbtTxFrameClk40Mhz_k (a_GbtUserClk_k)
    // GBT Reset:
    .GbtReset_or               (GbtReset_r));

assign TurnClkFlagDly_ob12 = 12'h0;

// Application SFPs GBT Bank (4x GBT Links):
VfcHdGbtCoreX4 #(
    .g_GbtBankId             (1'b0),
    .g_TxLatOp               (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
    .g_RxLatOp               (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
    .g_TxWideBus             (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
    .g_RxWideBus             (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
    .g_TxElinksDataAlignEn   ({1'b1, 1'b1, 1'b1, 1'b1}),
    .g_RxElinksDataAlignEn   ({1'b1, 1'b1, 1'b1, 1'b1}),
    .g_TxRxMatchFlagsEn      (1'b1))
i_AppSfpGbtBank (
    // WishBone Interface:
    .Clk_ik                  (a_Clk_k),
    .Rst_ir                  (AppReset_iqr),
    .Cyc_i                   (WbMasterCyc_i),
    .Stb_i                   (WbStbGbtCtrl),
    .We_i                    (WbMasterWr_i),
    .Adr_ib7                 (WbMasterAdr_ib25[6:0]),
    .Dat_ib32                (WbMasterDat_ib32),
    .Dat_ob32                (WbDatGbtCtrl_b32),
    .Ack_o                   (WbAckGbtCtrl),
    // SFPs Control:
    .SfpPresent_ib4          ({AppSfp4Present_iq,          AppSfp3Present_iq,          AppSfp2Present_iq,          AppSfp1Present_iq}),
    .SfpTxFault_ib4          ({AppSfp4TxFault_iq,          AppSfp3TxFault_iq,          AppSfp2TxFault_iq,          AppSfp1TxFault_iq}),
    .SfpLos_ib4              ({AppSfp4Los_iq,              AppSfp3Los_iq,              AppSfp2Los_iq,              AppSfp1Los_iq}),
    .SfpTxDisable_oqb4       ({AppSfp4TxDisable_o,         AppSfp3TxDisable_o,         AppSfp2TxDisable_o,         AppSfp1TxDisable_o}),
    .SfpRateSelect_oqb4      ({AppSfp4RateSelect_o,        AppSfp3RateSelect_o,        AppSfp2RateSelect_o,        AppSfp1RateSelect_o}),
    .StatusSfpTxDisable_ib4  ({StatusAppSfp4TxDisable_iq,  StatusAppSfp3TxDisable_iq,  StatusAppSfp2TxDisable_iq,  StatusAppSfp1TxDisable_iq}),
    .StatusSfpRateSelect_ib4 ({StatusAppSfp4RateSelect_iq, StatusAppSfp3RateSelect_iq, StatusAppSfp2RateSelect_iq, StatusAppSfp1RateSelect_iq}),
    // Clocks Scheme:
    .GbtRefClk120MHz_ik      (PllRefClkOut_ik),
    //--
    .TxFrameClk40Mhz_ik      (GbtTxFrameClk40MHz_k),   // Comment: Note!! TX logic of ALL GBT Links must be clocked by GbtTxFrameClk40MHz_k (a_GbtUserClk_k)
    .RxFrameClk40Mhz_okb4    (GbtRxFrameClk40Mhz_kb4), // Comment: Note!! RX logic of each GBT Link may be clocked by its dedicated GbtRxFrameClk40Mhz_kb4 or in some cases by GbtTxFrameClk40MHz_k (a_GbtUserClk_k)
    // GBT Resets Scheme:
    .GbtReset_ir             (GbtReset_r),
    .TxGbtReset_irb4         (4'h0),
    .RxGbtReset_irb4         (4'h0),
    // Serial Lanes:
    .MgtTx_ob4               (AppSfpTx_ob4),
    .MgtRx_ib4               (AppSfpRx_ib4),
    // Parallel Data:
    .TxDataScIc_i4b2         (TxDataScIcAppSfpGbtUserData_4b2), // Comment: Note!! TxDataScIc must be set to 2'b11 when not used
    .TxDataScEc_i4b2         (TxDataScEcAppSfpGbtUserData_4b2),
    .TxData_i4b80            (TxDataAppSfpGbtUserData_4b80),
    .TxDataWb_i4b32          ('{default: 32'h0}), // Comment: WideBus data not used when using GBT encoding
    //--
    .RxDataScIc_o4b2         (RxDataScIcAppSfpGbtUserData_4b2),
    .RxDataScEc_o4b2         (RxDataScEcAppSfpGbtUserData_4b2),
    .RxData_o4b80            (RxDataAppSfpGbtUserData_4b80),
    .RxDataWb_o4b32          (), // Comment: WideBus data not used when using GBT encoding
    // Control:
    .GbtTxReady_ob4          (),
    .GbtRxReady_ob4          (GbtRxReadyAppSfpGbtUserData_b4),
    .TxIsDataSel_ib4         (TxIsDataSelAppSfpGbtUserData_b4),
    .RxIsDataFlag_ob4        (RxIsDataFlagAppSfpGbtUserData_b4),
    // Latency Test Flags:
    .TxMatchFlag_o           (TxMatchFlagAppSfpGbtUserData),
    .RxMatchFlag_ob4         (RxMatchFlagAppSfpGbtUserData_b4));

// Example User Data:
gbt_pattern_generator i_TxDataGenAppSfpGbtUserData (
    .RESET_I                 (GbtReset_r),
    .TX_FRAMECLK_I           (a_GbtUserClk_k), // Comment: Note!! TX logic of ALL GBT Links must be clocked by GbtTxFrameClk40Mhz_k (a_GbtUserClk_k)
    .TX_ENCODING_SEL_I       (2'b00),           // Comment: GBT encoding
    .TEST_PATTERN_SEL_I      (TestPatterSelAppSfpGbtUserData_b2),
    .TX_DATA_O               (TxDataAppSfpGbtUserData_b84),
    .TX_EXTRA_DATA_WIDEBUS_O ());


always @(posedge a_GbtUserClk_k) ErrInjAppSfpGbtUserData_qb2        <= #1 {ErrInjAppSfpGbtUserData_qb2[0], ErrInjAppSfpGbtUserData};
always @(posedge a_GbtUserClk_k) ErrInjAppSfpGbtUserData_q          <= #1 (ErrInjAppSfpGbtUserData_qb2 == 2'b01) ? 1'b1 : 1'b0;
always @(posedge a_GbtUserClk_k) TxDataErrInjAppSfpGbtUserData_qb84 <= #1 ErrInjAppSfpGbtUserData_q ? {TxDataAppSfpGbtUserData_b84[83:1], ~TxDataAppSfpGbtUserData_b84[0]} : TxDataAppSfpGbtUserData_b84;

generate for (i=1;i<=4;i=i+1) begin: Gen_AppSfpGbtUserData

    assign TxDataScIcAppSfpGbtUserData_4b2[i] = TxDataErrInjAppSfpGbtUserData_qb84[83:82]; // Comment: Note!! TxDataScIc must be set to 2'b11 when not used
    assign TxDataScEcAppSfpGbtUserData_4b2[i] = TxDataErrInjAppSfpGbtUserData_qb84[81:80];
    assign TxDataAppSfpGbtUserData_4b80   [i] = TxDataErrInjAppSfpGbtUserData_qb84[79: 0];

    gbt_pattern_checker i_RxDataCheckAppSfpGbtUserData (
        .RESET_I                              (GbtReset_r),
        .RX_FRAMECLK_I                        (a_GbtUserClk_k), // Comment: Note!! RX logic of each GBT Link may be clocked by its dedicated GbtRxFrameClk40Mhz_kb4 or in some cases by GbtTxFrameClk40Mhz_k (a_GbtUserClk_k)
        .RX_DATA_I                            ({RxDataScIcAppSfpGbtUserData_4b2[i], RxDataScEcAppSfpGbtUserData_4b2[i], RxDataAppSfpGbtUserData_4b80[i]}),
        .RX_EXTRA_DATA_WIDEBUS_I              (32'h0), // Comment: WideBus data not used when using GBT encoding
        .GBT_RX_READY_I                       (GbtRxReadyAppSfpGbtUserData_b4[i]),
        .SCEC_CHECK_EN_I                      (ScEcCheckEnAppSfpGbtUserData_b4[i]),
        .RX_ENCODING_SEL_I                    (2'b00), // Comment: GBT encoding
        .TEST_PATTERN_SEL_I                   (TestPatterSelAppSfpGbtUserData_b2),
        .TOGGLING_DATA_CHECK_SYNC_I           (TogglingDataCheckSyncAppSfpGbtUserData_b4[i]),
        .RESET_GBTRXREADY_LOST_FLAG_I         (ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4[i]),
        .RESET_DATA_ERRORS_I                  (ResetGbtRxDataErrorsAppSfpGbtUserData_b4[i]),
        .GBTRXREADY_LOST_FLAG_O               (GbtRxRdyLostFlagAppSfpGbtUserData_b4[i]),
        .RXDATA_ERRORSEEN_FLAG_O              (RxDataErrFlagAppSfpGbtUserData_b4[i]),
        .RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O (), // Comment: WideBus data not used when using GBT encoding
        .RXDATA_ERROR_CNT                     (RxDataErrorCntAppSfpGbtUserData_64b4[i]),
        .RXDATA_FRAME_CNT                     (RxDataFrameCntAppSfpGbtUserData_64b4[i]));

end endgenerate

// Example User Data Registers Banks:
Generic4OutputRegs #(
    .Reg0Default     (32'h00000000),
    .Reg0AutoClrMask (32'hFFFFFFFF),
    .Reg1Default     (32'h00000000),
    .Reg1AutoClrMask (32'hFFFFFFFF),
    .Reg2Default     (32'h00000000),
    .Reg2AutoClrMask (32'hFFFFFFFF),
    .Reg3Default     (32'h00000000),
    .Reg3AutoClrMask (32'hFFFFFFFF))
i_AppSfpGbtUserDataCtrlRegs (
    .Clk_ik          (a_Clk_k),
    .Rst_irq         (AppReset_iqr),
    .Cyc_i           (WbMasterCyc_i),
    .Stb_i           (WbStbAppSfpGbtUserDataCtrlReg),
    .We_i            (WbMasterWr_i),
    .Adr_ib2         (WbMasterAdr_ib25[1:0]),
    .Dat_ib32        (WbMasterDat_ib32),
    .Dat_oab32       (WbDatAppSfpGbtUserDataCtrlReg_b32),
    .Ack_oa          (WbAckAppSfpGbtUserDataCtrlReg),
    //--
    .Reg0Value_ob32  (AppSfpGbtUserDataCtrlRegs_b32),
    .Reg1Value_ob32  (),
    .Reg2Value_ob32  (),
    .Reg3Value_ob32  ());

always @(posedge a_GbtUserClk_k) AppSfpGbtUserDataCtrlRegs_x2b32 <= #1 '{AppSfpGbtUserDataCtrlRegs_x2b32[0], AppSfpGbtUserDataCtrlRegs_b32};
always @(posedge a_GbtUserClk_k) AppSfpGbtUserDataCtrlRegs_qb32  <= #1   AppSfpGbtUserDataCtrlRegs_x2b32[1];

assign TxIsDataSelAppSfpGbtUserData_b4           = AppSfpGbtUserDataCtrlRegs_qb32[ 3: 0];
assign TestPatterSelAppSfpGbtUserData_b2         = AppSfpGbtUserDataCtrlRegs_qb32[ 5: 4];
//     Reserved                                    AppSfpGbtUserDataCtrlRegs_qb32[ 7: 6]
assign ErrInjAppSfpGbtUserData                   = AppSfpGbtUserDataCtrlRegs_qb32[    8];
//     Reserved                                    AppSfpGbtUserDataCtrlRegs_qb32[11: 9]
assign ScEcCheckEnAppSfpGbtUserData_b4           = AppSfpGbtUserDataCtrlRegs_qb32[15:12];
assign TogglingDataCheckSyncAppSfpGbtUserData_b4 = AppSfpGbtUserDataCtrlRegs_qb32[19:16];
assign ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4 = AppSfpGbtUserDataCtrlRegs_qb32[23:20];
assign ResetGbtRxDataErrorsAppSfpGbtUserData_b4  = AppSfpGbtUserDataCtrlRegs_qb32[27:24];
//     Reserved                                    AppSfpGbtUserDataCtrlRegs_qb32[31:28]

Generic4InputRegs i_AppSfpGbtUserDataStatRegs (
    .Clk_ik         (a_Clk_k),
    .Rst_irq        (AppReset_iqr),
    .Cyc_i          (WbMasterCyc_i),
    .Stb_i          (WbStbAppSfpGbtUserDataStatReg),
    .Adr_ib2        (WbMasterAdr_ib25[1:0]),
    .Dat_oab32      (WbDatAppSfpGbtUserDataStatReg_b32),
    .Ack_oa         (WbAckAppSfpGbtUserDataStatReg),
    //--
    .Reg0Value_ib32 (AppSfpGbtUserDataStatRegs_4b32[0]),
    .Reg1Value_ib32 (AppSfpGbtUserDataStatRegs_4b32[1]),
    .Reg2Value_ib32 (AppSfpGbtUserDataStatRegs_4b32[2]),
    .Reg3Value_ib32 (AppSfpGbtUserDataStatRegs_4b32[3]));

always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) GbtRxReadyAppSfpGbtUserData_x2b4[i] <= #1 {GbtRxReadyAppSfpGbtUserData_x2b4[i][0], GbtRxReadyAppSfpGbtUserData_b4[i]};
always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) GbtRxReadyAppSfpGbtUserData_qb4 [i] <= #1  GbtRxReadyAppSfpGbtUserData_x2b4[i][1];

always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) RxIsDataFlagAppSfpGbtUserData_x2b4[i] <= #1 {RxIsDataFlagAppSfpGbtUserData_x2b4[i][0], RxIsDataFlagAppSfpGbtUserData_b4[i]};
always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) RxIsDataFlagAppSfpGbtUserData_qb4 [i] <= #1  RxIsDataFlagAppSfpGbtUserData_x2b4[i][1];

always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) GbtRxRdyLostFlagAppSfpGbtUserData_4xb2[i] <= #1 {GbtRxRdyLostFlagAppSfpGbtUserData_4xb2[i][0], GbtRxRdyLostFlagAppSfpGbtUserData_b4[i]};
always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) GbtRxRdyLostFlagAppSfpGbtUserData_qb4 [i] <= #1  GbtRxRdyLostFlagAppSfpGbtUserData_4xb2[i][1];

always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) RxDataErrFlagAppSfpGbtUserData_4xb2[i] <= #1 {RxDataErrFlagAppSfpGbtUserData_4xb2[i][0], RxDataErrFlagAppSfpGbtUserData_b4[i]};
always @(posedge a_Clk_k) for (int i=1;i<=4;i=i+1) RxDataErrFlagAppSfpGbtUserData_qb4 [i] <= #1  RxDataErrFlagAppSfpGbtUserData_4xb2[i][1];

assign AppSfpGbtUserDataStatRegs_4b32[0] = {24'h0, RxIsDataFlagAppSfpGbtUserData_qb4,  GbtRxReadyAppSfpGbtUserData_qb4};
assign AppSfpGbtUserDataStatRegs_4b32[1] = {24'h0, RxDataErrFlagAppSfpGbtUserData_qb4, GbtRxRdyLostFlagAppSfpGbtUserData_qb4};
assign AppSfpGbtUserDataStatRegs_4b32[2] = 32'hBEEFCAFE; // Reserved
assign AppSfpGbtUserDataStatRegs_4b32[3] = 32'hBEEFCAFE; // Reserved

Generic16InputRegs i_AppSfpGbtUserDataBertRegs (
    .Clk_ik          (a_Clk_k),
    .Rst_irq         (AppReset_iqr),
    .Cyc_i           (WbMasterCyc_i),
    .Stb_i           (WbStbAppSfpGbtUserDataBertReg),
    .Adr_ib4         (WbMasterAdr_ib25[3:0]),
    .Dat_oab32       (WbDatAppSfpGbtUserDataBertReg_b32),
    .Ack_oa          (WbAckAppSfpGbtUserDataBertReg),
    //--
    .Reg0Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 0]),
    .Reg1Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 1]),
    .Reg2Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 2]),
    .Reg3Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 3]),
    .Reg4Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 4]),
    .Reg5Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 5]),
    .Reg6Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 6]),
    .Reg7Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 7]),
    .Reg8Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 8]),
    .Reg9Value_ib32  (AppSfpGbtUserDataBertRegs_16b32[ 9]),
    .Reg10Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[10]),
    .Reg11Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[11]),
    .Reg12Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[12]),
    .Reg13Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[13]),
    .Reg14Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[14]),
    .Reg15Value_ib32 (AppSfpGbtUserDataBertRegs_16b32[15]));

generate for (i=0;i<=3;i=i+1) begin: Gen_AppSfpGbtUserDataBertCdc
    for (j=0;j<=1;j=j+1) begin: Gen_AppSfpGbtUserDataBertCdcDpram

        generic_dpram_mod #(
            .aw     ( 1),
            .dw     (32))
        i_ErrorCntDpram (
            .rclk  (a_Clk_k),
            .rrst  (AppReset_iqr),
            .rce   (1'b1),
            .oe    (1'b1),
            .raddr (1'b0),
            .dout  (AppSfpGbtUserDataBertRegs_16b32[(2*i)+j]),
            .wclk  (a_GbtUserClk_k),
            .wrst  (GbtReset_r),
            .wce   (1'b1),
            .we    (1'b1),
            .waddr (1'b0),
            .di    (RxDataErrorCntAppSfpGbtUserData_64b4[i+1][((32*(j+1))-1):(32*j)]));

        generic_dpram_mod #(
            .aw     ( 1),
            .dw     (32))
        i_FrameCntDpram (
            .rclk  (a_Clk_k),
            .rrst  (AppReset_iqr),
            .rce   (1'b1),
            .oe    (1'b1),
            .raddr (1'b0),
            .dout  (AppSfpGbtUserDataBertRegs_16b32[((2*i)+8)+j]),
            .wclk  (a_GbtUserClk_k),
            .wrst  (GbtReset_r),
            .wce   (1'b1),
            .we    (1'b1),
            .waddr (1'b0),
            .di    (RxDataFrameCntAppSfpGbtUserData_64b4[i+1][((32*(j+1))-1):(32*j)]));

    end
end endgenerate

//==== Signals Forwarding ====//

always @* case(DipSw_ib8[2:1])
    2'h0: TestIo1 = BstClk_ik;
    2'h1: TestIo1 = BunchClkFlag_i;
    2'h2: TestIo1 = TurnClkFlag_i;
    2'h3: TestIo1 = TxMatchFlagAppSfpGbtUserData;
endcase
assign TestIo1_io = TestIo1;

always @* case(DipSw_ib8[4:3])
    2'h0: TestIo2 = GbtTxFrameClk40MHz_k;
    2'h1: TestIo2 = GbtRxFrameClk40Mhz_kb4[1];
    2'h2: TestIo2 = TurnClkFlag_i;
    2'h3: TestIo2 = RxMatchFlagAppSfpGbtUserData_b4[1];
endcase
assign TestIo2_io = TestIo2;

//assign GpIo1DirOut_o  = 1'b1;
//assign GpIo2DirOut_o  = 1'b1;
//assign GpIo34DirOut_o = 1'b1;
//assign GpIo1EnTerm_o  = 1'b0;
//assign GpIo2EnTerm_o  = 1'b0;
//assign GpIo3EnTerm_o  = 1'b0;
//assign GpIo4EnTerm_o  = 1'b0;
//assign GpIo_iob4[1]   = BstClk_ik;
//assign GpIo_iob4[2]   = BunchClkFlag_i;
//assign GpIo_iob4[3]   = TurnClkFlag_i;
//assign GpIo_iob4[4]   = GbtRxFrameClk40Mhz_kb4[1];

//****************************
// DDR3 interface:
//****************************

logic Ddr3AvlClk;

t_WbInterface       #(.g_DataWidth(32), .g_AddressWidth(25))                          WbDdr3a_t  (a_Clk_k, AppReset_iqr);
t_WbInterface       #(.g_DataWidth(32), .g_AddressWidth(25))                          WbDdr3b_t  (a_Clk_k, AppReset_iqr);
t_AvalonMmInterface #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11)) AvlDdr3a_t (.Clk_k(Ddr3AvlClk), .Reset_r(1'b0));
t_AvalonMmInterface #(.g_AddressWidth(26), .g_DataWidth(128), .g_BurstCountWidth(11)) AvlDdr3b_t (.Clk_k(Ddr3AvlClk), .Reset_r(1'b0));

assign WbDdr3a_t.Cyc = WbMasterCyc_i;
assign WbDdr3a_t.Stb = WbMasterDdr3aStb;
assign WbDdr3a_t.Adr_b = WbMasterAdr_ib25;
assign WbDdr3a_t.Sel_b = 4'b1111;
assign WbDdr3a_t.We = WbMasterWr_i;
assign WbDdr3a_t.DatMoSi_b = WbMasterDat_ib32;
assign WbMasterDdr3aAck = WbDdr3a_t.Ack;
assign WbMasterDdr3aDat_b32 = WbDdr3a_t.DatMiSo_b;

assign WbDdr3b_t.Cyc = WbMasterCyc_i;
assign WbDdr3b_t.Stb = WbMasterDdr3bStb;
assign WbDdr3b_t.Adr_b = WbMasterAdr_ib25;
assign WbDdr3b_t.Sel_b = 4'b1111;
assign WbDdr3b_t.We = WbMasterWr_i;
assign WbDdr3b_t.DatMoSi_b = WbMasterDat_ib32;
assign WbMasterDdr3bAck = WbDdr3b_t.Ack;
assign WbMasterDdr3bDat_b32 = WbDdr3b_t.DatMiSo_b;

//localparam c_Log2Ddr3PageSize = 18; //Comment: WB addressable space of 256k LW
localparam c_Log2Ddr3PageSize = 10; //Comment: WB addressable space of 256k LW

VfcDdr3WbToAvl #(.g_Log2PageSize(c_Log2Ddr3PageSize))
    i_WbToAvlDdrA(
        .WbSlave_iot     ( WbDdr3a_t  ),
        .AvlMaster_iot   ( AvlDdr3a_t ),
        .PageSelector_ib (CtrlReg1Value_b32[28-c_Log2Ddr3PageSize-1:0]));

VfcDdr3WbToAvl #(.g_Log2PageSize(c_Log2Ddr3PageSize))
    i_WbToAvlDdrB(
        .WbSlave_iot     ( WbDdr3b_t  ),
        .AvlMaster_iot   ( AvlDdr3b_t ),
        .PageSelector_ib (CtrlReg1Value_b32[28-c_Log2Ddr3PageSize-1:0]));

//Ddr3Controller i_Ddr3Controller(
VfcDdr3 i_VfcDdr3(
    .RefClk125Mhz_ik         (   Sys125MhzClk_ik         ),
    .AvlBurstIntClock_ok     (   Ddr3AvlClk              ),
    .ResetGlobalA_irn        ( ~ CtrlReg0Value_b32[0]    ),
    .ResetSoftA_irn          ( ~ CtrlReg0Value_b32[1]    ),
    .AvlBurstIntDdr3a_iot    (   AvlDdr3a_t              ),
    .InitDoneA_o             (   Ddr3StatusReg_b32[0]    ),
    .CalSuccessA_o           (   Ddr3StatusReg_b32[1]    ),
    .CalFailA_o              (   Ddr3StatusReg_b32[2]    ),
    .ResetGlobalB_irn        ( ~ CtrlReg0Value_b32[2]    ),
    .ResetSoftB_irn          ( ~ CtrlReg0Value_b32[3]    ),
    .AvlBurstIntDdr3b_iot    (   AvlDdr3b_t              ),
    .InitDoneB_o             (   Ddr3StatusReg_b32[3]    ),
    .CalSuccessB_o           (   Ddr3StatusReg_b32[4]    ),
    .CalFailB_o              (   Ddr3StatusReg_b32[5]    ),
    .Ddr3aCk_ok              (   Ddr3aCk_ok              ),
    .Ddr3aCk_okn             (   Ddr3aCk_okn             ),
    .Ddr3aCke_o              (   Ddr3aCke_o              ),
    .Ddr3aReset_orn          (   Ddr3aReset_orn          ),
    .Ddr3aRas_on             (   Ddr3aRas_on             ),
    .Ddr3aCas_on             (   Ddr3aCas_on             ),
    .Ddr3aCs_on              (   Ddr3aCs_on              ),
    .Ddr3aWe_on              (   Ddr3aWe_on              ),
    .Ddr3aOdt_o              (   Ddr3aOdt_o              ),
    .Ddr3aBa_ob3             (   Ddr3aBa_ob3             ),
    .Ddr3aAdr_ob16           (   Ddr3aAdr_ob16           ),
    .Ddr3aDm_ob2             (   Ddr3aDm_ob2             ),
    .Ddr3aDqs_iob2           (   Ddr3aDqs_iob2           ),
    .Ddr3aDqs_iob2n          (   Ddr3aDqs_iob2n          ),
    .Ddr3aDq_iob16           (   Ddr3aDq_iob16           ),
    .Ddr3bCk_ok              (   Ddr3bCk_ok              ),
    .Ddr3bCk_okn             (   Ddr3bCk_okn             ),
    .Ddr3bCke_o              (   Ddr3bCke_o              ),
    .Ddr3bReset_orn          (   Ddr3bReset_orn          ),
    .Ddr3bRas_on             (   Ddr3bRas_on             ),
    .Ddr3bCas_on             (   Ddr3bCas_on             ),
    .Ddr3bCs_on              (   Ddr3bCs_on              ),
    .Ddr3bWe_on              (   Ddr3bWe_on              ),
    .Ddr3bOdt_o              (   Ddr3bOdt_o              ),
    .Ddr3bBa_ob3             (   Ddr3bBa_ob3             ),
    .Ddr3bAdr_ob16           (   Ddr3bAdr_ob16           ),
    .Ddr3bDm_ob2             (   Ddr3bDm_ob2             ),
    .Ddr3bDqs_iob2           (   Ddr3bDqs_iob2           ),
    .Ddr3bDqs_iob2n          (   Ddr3bDqs_iob2n          ),
    .Ddr3bDq_iob16           (   Ddr3bDq_iob16           ),
    .OctSeriesControl_ib16   (   OctSeriesControl_ib16   ),
    .OctParallelControl_ib16 (   OctParallelControl_ib16 ));


// unused bits
assign Ddr3StatusReg_b32[31:6] = 26'b0;

endmodule
