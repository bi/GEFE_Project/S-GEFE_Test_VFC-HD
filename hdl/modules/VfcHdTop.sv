//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: VfcHdTop.sv
//
// Language: SystemVerilog 2013
//
// Targeted device:
//
//     - Vendor:  Intel FPGA (Altera)
//     - Model:   Arria V GX
//
// Description:
//
//     Top level HDL file targeted to the VFC-HD v3
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module VfcHdTop #(                           parameter     g_Synthesis = 1'b1)
//========================================  I/O ports  =======================================//
(
    // Note: chip_pin attribute for buses has the same order of element as logic vector:
    //   Example:
    //     (* chip_pin = "A2, A1, A0" *) input [2:0] Data_ib3,
    //     --> Data_ib3[0] ~ A0
    //     --> Data_ib3[1] ~ A1
    //     --> Data_ib3[2] ~ A2

    // VME Interface:
    (* useioff = 1 *)(* chip_pin = "AE29" *) input         VmeAs_in,
    (* useioff = 1 *)(* chip_pin = "AW31,AU31,AV31,AG30,AH30,AD29" *)
                                             input  [ 5:0] VmeAm_ib6,
    (* useioff = 1 *)(* chip_pin = "AU28,AV28,AP28,AR28,AC27,AD27,AM28,AB27,AB28,AD28,AE28,AH28,AJ28,AK29,AL29,AF28,AG28,AB29,AC29,AN29,AP29,AN30,AP30,AT29,AU29,AU30,AV30,AR30,AT30,AK30,AL30" *)
                                             inout  [31:1] VmeA_iob31,
    (* useioff = 1 *)(* chip_pin = "AW30" *) inout         VmeLWord_ion,
    (* useioff = 1 *)(* chip_pin = "AK27" *) output        VmeAOe_oen,
    (* useioff = 1 *)(* chip_pin = "AJ27" *) output        VmeADir_o,
    (* useioff = 1 *)(* chip_pin = "AN22,AP22" *)
                                             input  [ 1:0] VmeDs_inb2,
    (* useioff = 1 *)(* chip_pin = "AW28" *) input         VmeWrite_in,
    (* useioff = 1 *)(* chip_pin = "AE22,AF22,AG23,AH23,AV21,AW21,AV22,AW22,AT22,AU22,AK23,AL23,AD22,AE23,AN23,AP23,AT23,AU23,AN24,AP24,AW23,AW24,AG24,AH24,AE24,AF24,AK24,AL24,AT24,AU24,AD23,AD24" *)
                                             inout  [31:0] VmeD_iob32,
    (* useioff = 1 *)(* chip_pin = "AW20" *) output        VmeDOe_oen,
    (* useioff = 1 *)(* chip_pin = "AW19" *) output        VmeDDir_o,
    (* useioff = 1 *)(* chip_pin = "AL22" *) output        VmeDtAckOe_o,
    (* useioff = 1 *)(* chip_pin = "AT20,AU20,AG22,AH22,AR21,AT21,AK22" *)
                                             output [ 7:1] VmeIrq_ob7,
    (* useioff = 1 *)(* chip_pin = "AN21" *) input         VmeIack_in,
    (* useioff = 1 *)(* chip_pin = "AM21" *) input         VmeIackIn_in,
    (* useioff = 1 *)(* chip_pin = "AK21" *) output        VmeIackOut_on,
    (* useioff = 1 *)(* chip_pin = "AL20" *) input         VmeSysClk_ik,
    (* useioff = 1 *)(* chip_pin = "AK20" *) input         VmeSysReset_irn,
    // P2 RTM:
    (* useioff = 1 *)(* chip_pin = "F32,J32,C33,A33,B31,C31,F30,F29,J29,L30,J34,H31,B34,C32,A31,J30,E31,F28,H28,AF21" *)
                                             inout  [19:0] P2DataP_iob20,
    (* useioff = 1 *)(* chip_pin = "G32,K32,D33,B33,A30,D31,G30,G29,K29,M30,K34,J31,A35,D32,A32,K30,F31,G28,J28,AE21" *)
                                             inout  [19:0] P2DataN_iob20,
    // P0 Timing:
    (* useioff = 1 *)(* chip_pin = "AW26,AE26,AN26,AP26,AG25,AE25,AE27,AF27" *)
                                             input  [ 7:0] P0HwHighByte_ib8,
    (* useioff = 1 *)(* chip_pin = "AD25,AC22,AN20,AW27,AW33,AW32,AR31,AT31" *)
                                             input  [ 7:0] P0HwLowByte_ib8,
    (* useioff = 1 *)(* chip_pin = "AR27" *) output        DaisyChain1Cntrl_o,
    (* useioff = 1 *)(* chip_pin = "AP27" *) output        DaisyChain2Cntrl_o,
    (* useioff = 1 *)(* chip_pin = "AV19" *) input         VmeP0BunchClk_ik,
    (* useioff = 1 *)(* chip_pin = "AU19" *) input         VmeP0Tclk_ik,
    // SFP MGT Lanes:
    (* useioff = 1 *)(* chip_pin = "AW37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx1_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AT39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx2_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AP39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx3_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AM39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx4_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AU37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx1_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AR37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx2_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AN37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx3_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AL37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx4_ob, // Comment: Differential signal
//  (* useioff = 1 *)(* chip_pin = "AH39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  BstSfpRx_i,   // Comment: Differential signal
//  (* useioff = 1 *)(* chip_pin = "AG37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output BstSfpTx_o,   // Comment: Differential signal
//  (* useioff = 1 *)(* chip_pin = "AE1"  *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  EthSfpRx_i,   // Comment: Differential signal
//  (* useioff = 1 *)(* chip_pin = "AD3"  *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output EthSfpTx_o,   // Comment: Differential signal
// DDR3:
    (* chip_pin = "AF16" *)                 output        Ddr3aCk_ok,
    (* chip_pin = "AE17" *)                 output        Ddr3aCk_okn,
    (* chip_pin = "AM16" *)                 output        Ddr3aCke_o,
    (* chip_pin = "AN15" *)                 output        Ddr3aReset_orn,
    (* chip_pin = "AD17" *)                 output        Ddr3aRas_on,
    (* chip_pin = "AR18" *)                 output        Ddr3aCas_on,
    (* chip_pin = "AL17" *)                 output        Ddr3aCs_on,
    (* chip_pin = "AP18" *)                 output        Ddr3aWe_on,
    (* chip_pin = "AD19" *)                 output        Ddr3aOdt_o,
    (* chip_pin = "AC18,AD18,AE18" *)       output [ 2:0] Ddr3aBa_ob3,
    (* chip_pin = "AU17,AG18,AL18,AM18,AG17,AH17,AN17,AP17,AR16,AT16,AU16,AV16,AJ16,AK16,AN16,AP16" *)
                                            output [15:0] Ddr3aAdr_ob16,
    (* chip_pin = "AU13,AD16" *)            output [ 1:0] Ddr3aDm_ob2,
    (* chip_pin = "AF15,AH16" *)            inout  [ 1:0] Ddr3aDqs_iob2,
    (* chip_pin = "AE16,AG16" *)            inout  [ 1:0] Ddr3aDqs_iob2n,
    (* chip_pin = "AT14,AU14,AL14,AN14,AP14,AH14,AD15,AE15,AW14,AW15,AL15,AV13,AW13,AH15,AT15,AU15" *)
                                            inout  [15:0] Ddr3aDq_iob16,
    (* chip_pin = "AM10" *)                 output        Ddr3bCk_ok,
    (* chip_pin = "AL10" *)                 output        Ddr3bCk_okn,
    (* chip_pin = "AL11" *)                 output        Ddr3bCke_o,
    (* chip_pin = "AR13" *)                 output        Ddr3bReset_orn,
    (* chip_pin = "AK12" *)                 output        Ddr3bRas_on,
    (* chip_pin = "AU11" *)                 output        Ddr3bCas_on,
    (* chip_pin = "AJ12" *)                 output        Ddr3bCs_on,
    (* chip_pin = "AT11" *)                 output        Ddr3bWe_on,
    (* chip_pin = "AV10" *)                 output        Ddr3bOdt_o,
    (* chip_pin = "AL12,AC13,AD13" *)       output [ 2:0] Ddr3bBa_ob3,
    (* chip_pin = "AF13,AV9,AW9,AP11,AC12,AD11,AF12,AG12,AT9,AU9,AG11,AH11,AD12,AE12,AP10,AR10" *)
                                            output [15:0] Ddr3bAdr_ob16,
    (* chip_pin = "AK10,AU12" *)            output [ 1:0] Ddr3bDm_ob2,
    (* chip_pin = "AW5,AW12" *)             inout  [ 1:0] Ddr3bDqs_iob2,
    (* chip_pin = "AW6,AV12" *)             inout  [ 1:0] Ddr3bDqs_iob2n,
    (* chip_pin = "AW8,AM9,AW7,AR9,AV6,AT8,AU7,AU8,AE14,AE13,AM13,AW10,AW11,AP12,AH13,AJ13" *)
                                            inout  [15:0] Ddr3bDq_iob16,
    (* chip_pin = "AP7" *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD \"SSTL-135\"" *)
                                            input         OctRzqin_i,
    // FMC connector:
    (* chip_pin = "M34"  *)(* useioff = 1 *) input         FmcPrsntM2c_in,
    (* chip_pin = "AM31" *)(* useioff = 1 *) input         FmcPgM2c_i,
    (* chip_pin = "AL31" *)(* useioff = 1 *) output        FmcPgC2m_o,
    (* chip_pin = "AP33" *)(* useioff = 1 *) output        FmcTck_ok,
    (* chip_pin = "AK32" *)(* useioff = 1 *) output        FmcTms_o,
    (* chip_pin = "AN33" *)(* useioff = 1 *) output        FmcTdi_o,
    (* chip_pin = "AL32" *)(* useioff = 1 *) input         FmcTdo_i,
    (* chip_pin = "AK31" *)(* useioff = 1 *) output        FmcTrstL_orn,
    (* chip_pin = "AP32" *)(* useioff = 1 *) inout         FmcScl_iok,
    (* chip_pin = "AN32" *)(* useioff = 1 *) inout         FmcSda_io,
    (* chip_pin = "AM34" *)(* useioff = 1 *) input         FmcClkDir_i,
    (* chip_pin = "AP34" *)(* useioff = 1 *) input         FmcClk0M2cCmos_ik,
    (* chip_pin = "AK34" *)(* useioff = 1 *) input         FmcClk1M2cCmos_ik,
    (* chip_pin = "B22"  *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcClk2Bidir_iok,      // Comment: Differential signal
//  (* chip_pin = "B22"  *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) output FmcClk2Bidir_iok,      // Comment: Differential signal
    (* chip_pin = "H6"   *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcClk3Bidir_iok,      // Comment: Differential signal
//  (* chip_pin = "H6"   *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) output FmcClk3Bidir_iok,      // Comment: Differential signal
//  (* chip_pin = "AE31" *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcGbtClk0M2cLeft_ik,  // Comment: Differential signal
//  (* chip_pin = "AC31" *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcGbtClk1M2cLeft_ik,  // Comment: Differential signal
//  (* chip_pin = "AB9"  *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcGbtClk0M2cRight_ik, // Comment: Differential signal
//  (* chip_pin = "Y9"   *)(* useioff = 1 *) (* altera_attribute = "-name IO_STANDARD LVDS" *) input  FmcGbtClk1M2cRight_ik, // Comment: Differential signal
    (* chip_pin = "N34"  *)(* useioff = 1 *) inout         FmcLaP0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C20"  *)(* useioff = 1 *) inout         FmcLaP1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D24"  *)(* useioff = 1 *) inout         FmcLaP2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D25"  *)(* useioff = 1 *) inout         FmcLaP3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F24"  *)(* useioff = 1 *) inout         FmcLaP4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C28"  *)(* useioff = 1 *) inout         FmcLaP5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "A29"  *)(* useioff = 1 *) inout         FmcLaP6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F26"  *)(* useioff = 1 *) inout         FmcLaP7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "E22"  *)(* useioff = 1 *) inout         FmcLaP8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "B27"  *)(* useioff = 1 *) inout         FmcLaP9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C26"  *)(* useioff = 1 *) inout         FmcLaP10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M29"  *)(* useioff = 1 *) inout         FmcLaP11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F23"  *)(* useioff = 1 *) inout         FmcLaP12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A25"  *)(* useioff = 1 *) inout         FmcLaP13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B24"  *)(* useioff = 1 *) inout         FmcLaP14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M32"  *)(* useioff = 1 *) inout         FmcLaP15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D30"  *)(* useioff = 1 *) inout         FmcLaP16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "G21"  *)(* useioff = 1 *) inout         FmcLaP17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A22"  *)(* useioff = 1 *) inout         FmcLaP18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N30"  *)(* useioff = 1 *) inout         FmcLaP19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N22"  *)(* useioff = 1 *) inout         FmcLaP20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P27"  *)(* useioff = 1 *) inout         FmcLaP21_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N24"  *)(* useioff = 1 *) inout         FmcLaP22_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C23"  *)(* useioff = 1 *) inout         FmcLaP23_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R29"  *)(* useioff = 1 *) inout         FmcLaP24_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M26"  *)(* useioff = 1 *) inout         FmcLaP25_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A20"  *)(* useioff = 1 *) inout         FmcLaP26_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B28"  *)(* useioff = 1 *) inout         FmcLaP27_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R28"  *)(* useioff = 1 *) inout         FmcLaP28_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "L33"  *)(* useioff = 1 *) inout         FmcLaP29_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R26"  *)(* useioff = 1 *) inout         FmcLaP30_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "L28"  *)(* useioff = 1 *) inout         FmcLaP31_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T26"  *)(* useioff = 1 *) inout         FmcLaP32_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M27"  *)(* useioff = 1 *) inout         FmcLaP33_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N33"  *)(* useioff = 1 *) inout         FmcLaN0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D20"  *)(* useioff = 1 *) inout         FmcLaN1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "E24"  *)(* useioff = 1 *) inout         FmcLaN2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "E25"  *)(* useioff = 1 *) inout         FmcLaN3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G24"  *)(* useioff = 1 *) inout         FmcLaN4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D28"  *)(* useioff = 1 *) inout         FmcLaN5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "A28"  *)(* useioff = 1 *) inout         FmcLaN6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G26"  *)(* useioff = 1 *) inout         FmcLaN7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F22"  *)(* useioff = 1 *) inout         FmcLaN8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C27"  *)(* useioff = 1 *) inout         FmcLaN9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D26"  *)(* useioff = 1 *) inout         FmcLaN10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N29"  *)(* useioff = 1 *) inout         FmcLaN11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "G23"  *)(* useioff = 1 *) inout         FmcLaN12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B25"  *)(* useioff = 1 *) inout         FmcLaN13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C24"  *)(* useioff = 1 *) inout         FmcLaN14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N32"  *)(* useioff = 1 *) inout         FmcLaN15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D29"  *)(* useioff = 1 *) inout         FmcLaN16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "H21"  *)(* useioff = 1 *) inout         FmcLaN17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A21"  *)(* useioff = 1 *) inout         FmcLaN18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P30"  *)(* useioff = 1 *) inout         FmcLaN19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P22"  *)(* useioff = 1 *) inout         FmcLaN20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R27"  *)(* useioff = 1 *) inout         FmcLaN21_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P24"  *)(* useioff = 1 *) inout         FmcLaN22_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D23"  *)(* useioff = 1 *) inout         FmcLaN23_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T29"  *)(* useioff = 1 *) inout         FmcLaN24_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N26"  *)(* useioff = 1 *) inout         FmcLaN25_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B21"  *)(* useioff = 1 *) inout         FmcLaN26_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C29"  *)(* useioff = 1 *) inout         FmcLaN27_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T28"  *)(* useioff = 1 *) inout         FmcLaN28_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M33"  *)(* useioff = 1 *) inout         FmcLaN29_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T27"  *)(* useioff = 1 *) inout         FmcLaN30_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "M28"  *)(* useioff = 1 *) inout         FmcLaN31_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T25"  *)(* useioff = 1 *) inout         FmcLaN32_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "N27"  *)(* useioff = 1 *) inout         FmcLaN33_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C34"  *)(* useioff = 1 *) inout         FmcHaP0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G34"  *)(* useioff = 1 *) inout         FmcHaP1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "M16"  *)(* useioff = 1 *) inout         FmcHaP2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F14"  *)(* useioff = 1 *) inout         FmcHaP3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F20"  *)(* useioff = 1 *) inout         FmcHaP4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F17"  *)(* useioff = 1 *) inout         FmcHaP5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "N19"  *)(* useioff = 1 *) inout         FmcHaP6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "N18"  *)(* useioff = 1 *) inout         FmcHaP7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C19"  *)(* useioff = 1 *) inout         FmcHaP8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "F19"  *)(* useioff = 1 *) inout         FmcHaP9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "R19"  *)(* useioff = 1 *) inout         FmcHaP10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P16"  *)(* useioff = 1 *) inout         FmcHaP11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E18"  *)(* useioff = 1 *) inout         FmcHaP12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A18"  *)(* useioff = 1 *) inout         FmcHaP13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R15"  *)(* useioff = 1 *) inout         FmcHaP14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B16"  *)(* useioff = 1 *) inout         FmcHaP15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A17"  *)(* useioff = 1 *) inout         FmcHaP16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E34"  *)(* useioff = 1 *) inout         FmcHaP17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R14"  *)(* useioff = 1 *) inout         FmcHaP18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D16"  *)(* useioff = 1 *) inout         FmcHaP19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C17"  *)(* useioff = 1 *) inout         FmcHaP20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E15"  *)(* useioff = 1 *) inout         FmcHaP21_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "G16"  *)(* useioff = 1 *) inout         FmcHaP22_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B30"  *)(* useioff = 1 *) inout         FmcHaP23_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D34"  *)(* useioff = 1 *) inout         FmcHaN0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "H34"  *)(* useioff = 1 *) inout         FmcHaN1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "N16"  *)(* useioff = 1 *) inout         FmcHaN2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G14"  *)(* useioff = 1 *) inout         FmcHaN3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G20"  *)(* useioff = 1 *) inout         FmcHaN4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G17"  *)(* useioff = 1 *) inout         FmcHaN5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "P19"  *)(* useioff = 1 *) inout         FmcHaN6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "P18"  *)(* useioff = 1 *) inout         FmcHaN7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D19"  *)(* useioff = 1 *) inout         FmcHaN8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "G19"  *)(* useioff = 1 *) inout         FmcHaN9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "T19"  *)(* useioff = 1 *) inout         FmcHaN10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R16"  *)(* useioff = 1 *) inout         FmcHaN11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F18"  *)(* useioff = 1 *) inout         FmcHaN12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A19"  *)(* useioff = 1 *) inout         FmcHaN13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T15"  *)(* useioff = 1 *) inout         FmcHaN14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C16"  *)(* useioff = 1 *) inout         FmcHaN15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A16"  *)(* useioff = 1 *) inout         FmcHaN16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F34"  *)(* useioff = 1 *) inout         FmcHaN17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "T14"  *)(* useioff = 1 *) inout         FmcHaN18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E16"  *)(* useioff = 1 *) inout         FmcHaN19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D17"  *)(* useioff = 1 *) inout         FmcHaN20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F15"  *)(* useioff = 1 *) inout         FmcHaN21_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "H16"  *)(* useioff = 1 *) inout         FmcHaN22_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C30"  *)(* useioff = 1 *) inout         FmcHaN23_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C6"   *)(* useioff = 1 *) inout         FmcHbP0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D13"  *)(* useioff = 1 *) inout         FmcHbP1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "M7"   *)(* useioff = 1 *) inout         FmcHbP2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "P12"  *)(* useioff = 1 *) inout         FmcHbP3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C11"  *)(* useioff = 1 *) inout         FmcHbP4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "B12"  *)(* useioff = 1 *) inout         FmcHbP5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "A6"   *)(* useioff = 1 *) inout         FmcHbP6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "A9"   *)(* useioff = 1 *) inout         FmcHbP7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "M12"  *)(* useioff = 1 *) inout         FmcHbP8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "B10"  *)(* useioff = 1 *) inout         FmcHbP9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D12"  *)(* useioff = 1 *) inout         FmcHbP10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C8"   *)(* useioff = 1 *) inout         FmcHbP11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A13"  *)(* useioff = 1 *) inout         FmcHbP12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "C14"  *)(* useioff = 1 *) inout         FmcHbP13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E10"  *)(* useioff = 1 *) inout         FmcHbP14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "P13"  *)(* useioff = 1 *) inout         FmcHbP15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A11"  *)(* useioff = 1 *) inout         FmcHbP16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E6"   *)(* useioff = 1 *) inout         FmcHbP17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "K6"   *)(* useioff = 1 *) inout         FmcHbP18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A7"   *)(* useioff = 1 *) inout         FmcHbP19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F8"   *)(* useioff = 1 *) inout         FmcHbP20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D9 "  *)(* useioff = 1 *) inout         FmcHbP21_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D6"   *)(* useioff = 1 *) inout         FmcHbN0_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "E13"  *)(* useioff = 1 *) inout         FmcHbN1_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "N7"   *)(* useioff = 1 *) inout         FmcHbN2_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "R12"  *)(* useioff = 1 *) inout         FmcHbN3_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "D11"  *)(* useioff = 1 *) inout         FmcHbN4_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C12"  *)(* useioff = 1 *) inout         FmcHbN5_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "B6"   *)(* useioff = 1 *) inout         FmcHbN6_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "A8"   *)(* useioff = 1 *) inout         FmcHbN7_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "N12"  *)(* useioff = 1 *) inout         FmcHbN8_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "C10"  *)(* useioff = 1 *) inout         FmcHbN9_io,  // Comment: Differential standards must be either input OR output
    (* chip_pin = "E12"  *)(* useioff = 1 *) inout         FmcHbN10_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D8"   *)(* useioff = 1 *) inout         FmcHbN11_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A12"  *)(* useioff = 1 *) inout         FmcHbN12_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "D14"  *)(* useioff = 1 *) inout         FmcHbN13_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "F10"  *)(* useioff = 1 *) inout         FmcHbN14_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "R13"  *)(* useioff = 1 *) inout         FmcHbN15_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "A10"  *)(* useioff = 1 *) inout         FmcHbN16_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E7"   *)(* useioff = 1 *) inout         FmcHbN17_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "L6"   *)(* useioff = 1 *) inout         FmcHbN18_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "B7"   *)(* useioff = 1 *) inout         FmcHbN19_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "G8"   *)(* useioff = 1 *) inout         FmcHbN20_io, // Comment: Differential standards must be either input OR output
    (* chip_pin = "E9"   *)(* useioff = 1 *) inout         FmcHbN21_io, // Comment: Differential standards must be either input OR output
//  (* chip_pin = "P3"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m0_o, // Comment: Differential signal
//  (* chip_pin = "T3"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m1_o, // Comment: Differential signal
//  (* chip_pin = "V3"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m2_o, // Comment: Differential signal
//  (* chip_pin = "Y3"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m3_o, // Comment: Differential signal
//  (* chip_pin = "AM3"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m4_o, // Comment: Differential signal
//  (* chip_pin = "AT3"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m5_o, // Comment: Differential signal
//  (* chip_pin = "AE37" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m6_o, // Comment: Differential signal
//  (* chip_pin = "AA37" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m7_o, // Comment: Differential signal
//  (* chip_pin = "AC37" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m8_o, // Comment: Differential signal
//  (* chip_pin = "AP3"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output FmcDpC2m9_o, // Comment: Differential signal
//  (* chip_pin = "R1"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c0_i, // Comment: Differential signal
//  (* chip_pin = "U1"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c1_i, // Comment: Differential signal
//  (* chip_pin = "W1"   *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c2_i, // Comment: Differential signal
//  (* chip_pin = "AA1"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c3_i, // Comment: Differential signal
//  (* chip_pin = "AN1"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c4_i, // Comment: Differential signal
//  (* chip_pin = "AU1"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c5_i, // Comment: Differential signal
//  (* chip_pin = "AF39" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c6_i, // Comment: Differential signal
//  (* chip_pin = "AB39" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c7_i, // Comment: Differential signal
//  (* chip_pin = "AD39" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c8_i, // Comment: Differential signal
//  (* chip_pin = "AR1"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  FmcDpM2c9_i, // Comment: Differential signal
    // FMC Voltage Control
    (* chip_pin = "AU27" *)(* useioff = 1 *) output        VadjCs_o ,
    (* chip_pin = "AT27" *)(* useioff = 1 *) output        VadjSclk_ok,
    (* chip_pin = "AN27" *)(* useioff = 1 *) output        VadjDin_o,
    (* chip_pin = "AM27" *)(* useioff = 1 *) output        VfmcEnable_oen,
    // ADC Voltage monitoring:
    (* chip_pin = "AG27" *)(* useioff = 1 *) input         VAdcDout_i,
    (* chip_pin = "AC25" *)(* useioff = 1 *) output        VAdcDin_o,
    (* chip_pin = "AB25" *)(* useioff = 1 *) output        VAdcCs_o,
    (* chip_pin = "AL33" *)(* useioff = 1 *) output        VAdcSclk_ok,
    // BST Interface:
    (* chip_pin = "AH27" *)(* useioff = 1 *) input         BstDataIn_i,
    (* chip_pin = "AU32" *)(* useioff = 1 *) input         CdrClkOut_ik,
    (* chip_pin = "AT32" *)(* useioff = 1 *) input         CdrDataOut_i,
    // Clocks Scheme:
    (* chip_pin = "AW29" *)(* useioff = 1 *) output        OeSi57x_oe,
    (* chip_pin = "AM33" *)(* useioff = 1 *) input         Si57xClk_ik,
    (* chip_pin = "AK7, AT7, AW4, AR6" *)(* altera_attribute = "-name IO_STANDARD \"SSTL-135\"" *)
                                             input  [3:0]  ClkFb_ikb4,
    (* chip_pin = "AJ7, AR7, AV4, AP6" *)(* altera_attribute = "-name IO_STANDARD \"SSTL-135\"; -name OUTPUT_TERMINATION \"SERIES 40 OHM WITH CALIBRATION\"" *)
                                             output [3:0]  ClkFb_okb4,
    (* chip_pin = "AD20" *)(* useioff = 1 *) input         Clk20VCOx_ik,
    (* chip_pin = "AF25" *)(* useioff = 1 *) output        PllDac20Sync_o,
    (* chip_pin = "AC24" *)(* useioff = 1 *) output        PllDac25Sync_o,
    (* chip_pin = "AH26" *)(* useioff = 1 *) output        PllDacSclk_ok,
    (* chip_pin = "AG26" *)(* useioff = 1 *) output        PllDacDin_o,
    (* chip_pin = "AL34" *)(* useioff = 1 *) inout         PllRefSda_ioz,
    (* chip_pin = "AK33" *)(* useioff = 1 *) inout         PllRefScl_iokz,
    (* chip_pin = "AJ33" *)(* useioff = 1 *) input         PllRefInt_i,
    (* chip_pin = "AC21" *)(* useioff = 1 *) output        PllSourceMuxOut_ok,
    (* chip_pin = "AG32" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD LVDS" *) input PllRefClkOut_ik,   //Comment: Differential reference clock for the MGT lines
    (* chip_pin = "AF8"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD LVDS" *) input GbitTrxClkRefR_ik, //Comment: Differential reference clock for the MGT lines
    // I2C Mux & IO expanders:
    (* chip_pin = "AN25" *)(* useioff = 1 *) inout         I2cMuxSda_ioz,
    (* chip_pin = "AM25" *)(* useioff = 1 *) inout         I2cMuxScl_iokz,
    (* chip_pin = "AT25" *)(* useioff = 1 *) input         I2CIoExpIntApp12_in,
    (* chip_pin = "AR25" *)(* useioff = 1 *) input         I2CIoExpIntApp34_in,
    (* chip_pin = "AT26" *)(* useioff = 1 *) input         I2CIoExpIntBstEth_in,
    (* chip_pin = "AU26" *)(* useioff = 1 *) input         I2CIoExpIntBlmIn_in,
    (* chip_pin = "AK25" *)(* useioff = 1 *) input         I2CMuxIntLos_in,
    // WR PROM:
    (* chip_pin = "AD26" *)(* useioff = 1 *) inout         WrPromSda_io,
    (* chip_pin = "AH25" *)(* useioff = 1 *) output        WrPromScl_ok,
    // DIP Switch (SW1):
    (* chip_pin = "T23,R23,N23,M23,K23,J23,J24,H24" *)(* useioff = 1 *)
                                             input  [ 8:1] DipSw_ib8, //Comment: contains also the part formely used as No GA
    // PCB Revision Resistor Network:
    (* chip_pin = "T21,R21,A24,A23,M22,L22,T22,R22" *)(* useioff = 1 *)
                                             input  [ 7:0] PcbRev_ib7,
    // TestIo MMCX Connectors:
    (* chip_pin = "AP20" *)(* useioff = 1 *) inout         TestIo1_io,
    (* chip_pin = "AH20" *)(* useioff = 1 *) inout         TestIo2_io,
    // GPIO LEMO Connectors:
    (* chip_pin = "AV24,AL26,AG21,AH21" *) (* useioff = 1 *)
                                             inout  [ 4:1] GpIo_iob4,
    // Miscellaneous:
    (* chip_pin = "AW25" *)(* useioff = 1 *) input         PushButtonN_in,
    (* chip_pin = "AV27" *)(* useioff = 1 *) inout         TempIdDq_ioz,
    (* chip_pin = "AD21" *)(* useioff = 1 *) output        ResetFpgaConfigN_orn
);

//=======================================  Declarations  =====================================//

wire [  7:0] AppVersion_b8, AppReleaseDay_b8, AppReleaseMonth_b8, AppReleaseYear_b8;
wire [351:0] AppInfo_b352;
wire         AppReset_r, ResetRequest_r;
wire         Sys125MhzClk_k;
wire         Wb1SysToAppCyc, Wb1SysToAppStb, Wb1SysToAppWr, Wb1AppToSysAck;
wire [ 24:0] Wb1SysToAppAdr_b25;
wire [ 31:0] Wb1SysToAppDat_b32, Wb1AppToSysDat_b32;
wire         Wb2AppToSysCyc, Wb2AppToSysStb, Wb2AppToSysWr, Wb2SysToAppAck;
wire [ 24:0] Wb2AppToSysAdr_b25;
wire [ 31:0] Wb2AppToSysDat_b32, Wb2SysToAppDat_b32;
wire [  8:1] Led_b8;
wire         BstOn, BstClk_k, BunchClkFlag, TurnClkFlag;
wire [ 11:0] TurnClkFlagDly_b12;
wire [  7:0] BstByteAddress_b8;
wire [  7:0] BstByte_b8;
wire         BstByteStrobe, BstByteError;
wire [ 23:0] InterruptRequest_b24;
wire         GpIo1DirOut, GpIo1DirOutStatus, GpIo2DirOut, GpIo2DirOutStatus, GpIo34DirOut, GpIo34DirOutStatus;
wire         GpIo1EnTerm, GpIo1EnTermStatus, GpIo2EnTerm, GpIo2EnTermStatus, GpIo3EnTerm, GpIo3EnTermStatus, GpIo4EnTerm, GpIo4EnTermStatus;
wire [  7:0] BlmIn_b8;
wire [  4:1] AppSfpPresent_b4, AppSfpTxFault_b4, AppSfpLos_b4, AppSfpTxDisable_b4, AppSfpRateSelect_b4, StatusAppSfpTxDisable_b4, StatusAppSfpRateSelect_b4;
wire         BstSfpPresent, BstSfpTxFault, BstSfpLos, BstSfpTxDisable, BstSfpRateSelect, StatusBstSfpTxDisable, StatusBstSfpRateSelect;
wire         EthSfpPresent, EthSfpTxFault, EthSfpLos, EthSfpTxDisable, EthSfpRateSelect, StatusEthSfpTxDisable, StatusEthSfpRateSelect;
wire         CdrLos, CdrLol;
wire         I2cWbCyc, I2cWbStb, I2cWbWe, I2cWbAck;
wire [ 11:0] I2cWbAdr_b12;
wire [  7:0] I2cWbDatMoSi_b8, I2cWbDatMiSo_b8;
logic  [3:0] ClkFbOutput_kb4;
logic        ArriaTermSDO;
logic [15:0] OctParallelControl_b16;
logic [15:0] OctSeriesControl_b16;

//=====================================  ARRIA V IO required primitives  ====================================//

arriav_termination i_arriav_termination (
	.rzqin         (OctRzqin_i   ),
	.serdataout    (ArriaTermSDO ));

arriav_termination_logic i_arriav_termination_logic(
	.serdata                    (ArriaTermSDO          ),
	.parallelterminationcontrol (OctParallelControl_b16),
	.seriesterminationcontrol   (OctSeriesControl_b16  ));

genvar i;

generate
    for (i = 0; i < 4; i++) begin: gen_ClkFbIob
        arriav_io_obuf #(
            .bus_hold          ("false"         ),
            .open_drain_output ("false"         ),
            .lpm_type          ("arriav_io_obuf")
        ) i_ClkFbIob (
            .i                         (ClkFbOutput_kb4[i]),
            .o                         (ClkFb_okb4[i]),
            .obar                      (),
            .oe                        (1'b1),
            .dynamicterminationcontrol (1'b0),
            .parallelterminationcontrol(OctParallelControl_b16),
            .seriesterminationcontrol  (OctSeriesControl_b16)
        );
    end
endgenerate


//=====================================  Top Level Logic  ====================================//

//==== System Module ====//

VfcHdSystem #(
    .g_Synthesis                (g_Synthesis))
i_VfcHdSystem (
    // External Ports:
    .VmeAs_in                   (VmeAs_in),
    .VmeAm_ib6                  (VmeAm_ib6),
    .VmeA_iob31                 (VmeA_iob31),
    .VmeLWord_ion               (VmeLWord_ion),
    .VmeAOe_oen                 (VmeAOe_oen),
    .VmeADir_o                  (VmeADir_o),
    .VmeDs_inb2                 (VmeDs_inb2),
    .VmeWrite_in                (VmeWrite_in),
    .VmeD_iob32                 (VmeD_iob32),
    .VmeDOe_oen                 (VmeDOe_oen),
    .VmeDDir_o                  (VmeDDir_o),
    .VmeDtAckOe_o               (VmeDtAckOe_o),
    .VmeIrq_ob7                 (VmeIrq_ob7),
    .VmeIack_in                 (VmeIack_in),
    .VmeIackIn_in               (VmeIackIn_in),
    .VmeIackOut_on              (VmeIackOut_on),
    .VmeSysClk_ik               (VmeSysClk_ik),
    .VmeSysReset_irn            (VmeSysReset_irn),
    .I2cMuxSda_ioz              (I2cMuxSda_ioz),
    .I2cMuxScl_iokz             (I2cMuxScl_iokz),
    .I2CIoExpIntApp12_ian       (I2CIoExpIntApp12_in),
    .I2CIoExpIntApp34_ian       (I2CIoExpIntApp34_in),
    .I2CIoExpIntBstEth_ian      (I2CIoExpIntBstEth_in),
    .I2CIoExpIntBlmIn_ian       (I2CIoExpIntBlmIn_in),
    .I2CMuxIntLos_ian           (I2CMuxIntLos_in),
    .BstDataIn_i                (BstDataIn_i),
    .CdrClkOut_ik               (CdrClkOut_ik),
    .CdrDataOut_i               (CdrDataOut_i),
    .VAdcDout_i                 (VAdcDout_i),
    .VAdcDin_o                  (VAdcDin_o),
    .VAdcCs_o                   (VAdcCs_o),
    .VAdcSclk_ok                (VAdcSclk_ok),
    .GbitTrxClkRefR_ik          (GbitTrxClkRefR_ik),
    .Clk20VCOx_ik               (Clk20VCOx_ik),
    .VadjCs_o                   (VadjCs_o),
    .VadjSclk_ok                (VadjSclk_ok),
    .VadjDin_o                  (VadjDin_o),
    .PcbRev_ib7                 (PcbRev_ib7),
    .TempIdDq_ioz               (TempIdDq_ioz),
    .ResetFpgaConfigN_orn       (ResetFpgaConfigN_orn),
    // System <-> Application Interface:
    .Sys125MhzClk_ok            (Sys125MhzClk_k),
    .AppVersion_ib8             (AppVersion_b8),
    .AppReleaseDay_ib8          (AppReleaseDay_b8),
    .AppReleaseMonth_ib8        (AppReleaseMonth_b8),
    .AppReleaseYear_ib8         (AppReleaseYear_b8),
    .AppInfo_ib352              (AppInfo_b352),
    .AppReset_oqr               (AppReset_r),
    .WbMasterCyc_oq             (Wb1SysToAppCyc),
    .WbMasterStb_oq             (Wb1SysToAppStb),
    .WbMasterAdr_oqb25          (Wb1SysToAppAdr_b25),
    .WbMasterWr_oq              (Wb1SysToAppWr),
    .WbMasterDat_oqb32          (Wb1SysToAppDat_b32),
    .WbMasterDat_ib32           (Wb1AppToSysDat_b32),
    .WbMasterAck_i              (Wb1AppToSysAck),
    .WbSlaveCyc_i               (Wb2AppToSysCyc),
    .WbSlaveStb_i               (Wb2AppToSysStb),
    .WbSlaveAdr_ib25            (Wb2AppToSysAdr_b25),
    .WbSlaveWr_i                (Wb2AppToSysWr),
    .WbSlaveDat_ib32            (Wb2AppToSysDat_b32),
    .WbSlaveDat_ob32            (Wb2SysToAppDat_b32),
    .WbSlaveAck_o               (Wb2SysToAppAck),
    .Led_ib8                    (Led_b8),
    .BstOn_o                    (BstOn),
    .BstClk_ok                  (BstClk_k),
    .BunchClkFlag_o             (BunchClkFlag),
    .TurnClkFlag_o              (TurnClkFlag),
    .TurnClkFlagDly_ib12        (TurnClkFlagDly_b12),
    .BstByteAddress_ob8         (BstByteAddress_b8),
    .BstByte_ob8                (BstByte_b8),
    .BstByteStrobe_o            (BstByteStrobe),
    .BstByteError_o             (BstByteError),
    .InterruptRequest_ib24      (InterruptRequest_b24),
    .BlmIn_ob8                  (BlmIn_b8),
    .GpIo1DirOut_i              (GpIo1DirOut),
    .GpIo1DirOutStatus_o        (GpIo1DirOutStatus),
    .GpIo2DirOut_i              (GpIo2DirOut),
    .GpIo2DirOutStatus_o        (GpIo2DirOutStatus),
    .GpIo34DirOut_i             (GpIo34DirOut),
    .GpIo34DirOutStatus_o       (GpIo34DirOutStatus),
    .GpIo1EnTerm_i              (GpIo1EnTerm),
    .GpIo1EnTermStatus_o        (GpIo1EnTermStatus),
    .GpIo2EnTerm_i              (GpIo2EnTerm),
    .GpIo2EnTermStatus_o        (GpIo2EnTermStatus),
    .GpIo3EnTerm_i              (GpIo3EnTerm),
    .GpIo3EnTermStatus_o        (GpIo3EnTermStatus),
    .GpIo4EnTerm_i              (GpIo4EnTerm),
    .GpIo4EnTermStatus_o        (GpIo4EnTermStatus),
    .AppSfp1Present_oq          (AppSfpPresent_b4[1]),
    .AppSfp1TxFault_oq          (AppSfpTxFault_b4[1]),
    .AppSfp1Los_oq              (AppSfpLos_b4[1]),
    .AppSfp1TxDisable_i         (AppSfpTxDisable_b4[1]),
    .AppSfp1RateSelect_i        (AppSfpRateSelect_b4[1]),
    .StatusAppSfp1TxDisable_oq  (StatusAppSfpTxDisable_b4[1]),
    .StatusAppSfp1RateSelect_oq (StatusAppSfpRateSelect_b4[1]),
    .AppSfp2Present_oq          (AppSfpPresent_b4[2]),
    .AppSfp2TxFault_oq          (AppSfpTxFault_b4[2]),
    .AppSfp2Los_oq              (AppSfpLos_b4[2]),
    .AppSfp2TxDisable_i         (AppSfpTxDisable_b4[2]),
    .AppSfp2RateSelect_i        (AppSfpRateSelect_b4[2]),
    .StatusAppSfp2TxDisable_oq  (StatusAppSfpTxDisable_b4[2]),
    .StatusAppSfp2RateSelect_oq (StatusAppSfpRateSelect_b4[2]),
    .AppSfp3Present_oq          (AppSfpPresent_b4[3]),
    .AppSfp3TxFault_oq          (AppSfpTxFault_b4[3]),
    .AppSfp3Los_oq              (AppSfpLos_b4[3]),
    .AppSfp3TxDisable_i         (AppSfpTxDisable_b4[3]),
    .AppSfp3RateSelect_i        (AppSfpRateSelect_b4[3]),
    .StatusAppSfp3TxDisable_oq  (StatusAppSfpTxDisable_b4[3]),
    .StatusAppSfp3RateSelect_oq (StatusAppSfpRateSelect_b4[3]),
    .AppSfp4Present_oq          (AppSfpPresent_b4[4]),
    .AppSfp4TxFault_oq          (AppSfpTxFault_b4[4]),
    .AppSfp4Los_oq              (AppSfpLos_b4[4]),
    .AppSfp4TxDisable_i         (AppSfpTxDisable_b4[4]),
    .AppSfp4RateSelect_i        (AppSfpRateSelect_b4[4]),
    .StatusAppSfp4TxDisable_oq  (StatusAppSfpTxDisable_b4[4]),
    .StatusAppSfp4RateSelect_oq (StatusAppSfpRateSelect_b4[4]),
    .BstSfpPresent_oq           (BstSfpPresent),
    .BstSfpTxFault_oq           (BstSfpTxFault),
    .BstSfpLos_oq               (BstSfpLos),
    .BstSfpTxDisable_i          (BstSfpTxDisable),
    .BstSfpRateSelect_i         (BstSfpRateSelect),
    .StatusBstSfpTxDisable_oq   (StatusBstSfpTxDisable),
    .StatusBstSfpRateSelect_oq  (StatusBstSfpRateSelect),
    .EthSfpPresent_oq           (EthSfpPresent),
    .EthSfpTxFault_oq           (EthSfpTxFault),
    .EthSfpLos_oq               (EthSfpLos),
    .EthSfpTxDisable_i          (EthSfpTxDisable),
    .EthSfpRateSelect_i         (EthSfpRateSelect),
    .StatusEthSfpTxDisable_oq   (StatusEthSfpTxDisable),
    .StatusEthSfpRateSelect_oq  (StatusEthSfpRateSelect),
    .CdrLos_oq                  (CdrLos),
    .CdrLol_oq                  (CdrLol),
    .I2cWbCyc_i                 (I2cWbCyc),
    .I2cWbStb_i                 (I2cWbStb),
    .I2cWbWe_i                  (I2cWbWe),
    .I2cWbAdr_ib12              (I2cWbAdr_b12),
    .I2cWbDat_ib8               (I2cWbDatMoSi_b8),
    .I2cWbDat_ob8               (I2cWbDatMiSo_b8),
    .I2cWbAck_o                 (I2cWbAck));


//==== Application Module ====//

VfcHdApplication i_VfcHdApplication (
    // External Ports:
    .BstSfpRx_i                 (BstSfpRx_i),
    .BstSfpTx_o                 (BstSfpTx_o),
    .EthSfpRx_i                 (EthSfpRx_i),
    .EthSfpTx_o                 (EthSfpTx_o),
    .AppSfpRx_ib4               ({AppSfpRx4_ib,AppSfpRx3_ib,AppSfpRx2_ib,AppSfpRx1_ib}),
    .AppSfpTx_ob4               ({AppSfpTx4_ob,AppSfpTx3_ob,AppSfpTx2_ob,AppSfpTx1_ob}),
    .Ddr3aCk_ok                 (Ddr3aCk_ok),
    .Ddr3aCk_okn                (Ddr3aCk_okn),
    .Ddr3aCke_o                 (Ddr3aCke_o),
    .Ddr3aReset_orn             (Ddr3aReset_orn),
    .Ddr3aRas_on                (Ddr3aRas_on),
    .Ddr3aCas_on                (Ddr3aCas_on),
    .Ddr3aCs_on                 (Ddr3aCs_on),
    .Ddr3aWe_on                 (Ddr3aWe_on),
    .Ddr3aOdt_o                 (Ddr3aOdt_o),
    .Ddr3aBa_ob3                (Ddr3aBa_ob3),
    .Ddr3aAdr_ob16              (Ddr3aAdr_ob16),
    .Ddr3aDm_ob2                (Ddr3aDm_ob2),
    .Ddr3aDqs_iob2              (Ddr3aDqs_iob2),
    .Ddr3aDqs_iob2n             (Ddr3aDqs_iob2n),
    .Ddr3aDq_iob16              (Ddr3aDq_iob16),
    .Ddr3bCk_ok                 (Ddr3bCk_ok),
    .Ddr3bCk_okn                (Ddr3bCk_okn),
    .Ddr3bCke_o                 (Ddr3bCke_o),
    .Ddr3bReset_orn             (Ddr3bReset_orn),
    .Ddr3bRas_on                (Ddr3bRas_on),
    .Ddr3bCas_on                (Ddr3bCas_on),
    .Ddr3bCs_on                 (Ddr3bCs_on),
    .Ddr3bWe_on                 (Ddr3bWe_on),
    .Ddr3bOdt_o                 (Ddr3bOdt_o),
    .Ddr3bBa_ob3                (Ddr3bBa_ob3),
    .Ddr3bAdr_ob16              (Ddr3bAdr_ob16),
    .Ddr3bDm_ob2                (Ddr3bDm_ob2),
    .Ddr3bDqs_iob2              (Ddr3bDqs_iob2),
    .Ddr3bDqs_iob2n             (Ddr3bDqs_iob2n),
    .Ddr3bDq_iob16              (Ddr3bDq_iob16),
    .TestIo1_io                 (TestIo1_io),
    .TestIo2_io                 (TestIo2_io),
    .VfmcEnable_oen             (VfmcEnable_oen),
    .FmcPrsntM2c_in             (FmcPrsntM2c_in),
    .FmcPgM2c_i                 (FmcPgM2c_i),
    .FmcPgC2m_o                 (FmcPgC2m_o),
    .FmcTck_ok                  (FmcTck_ok),
    .FmcTms_o                   (FmcTms_o),
    .FmcTdi_o                   (FmcTdi_o),
    .FmcTdo_i                   (FmcTdo_i),
    .FmcTrstL_orn               (FmcTrstL_orn),
    .FmcScl_iok                 (FmcScl_iok),
    .FmcSda_io                  (FmcSda_io),
    .FmcClkDir_i                (FmcClkDir_i),
    .FmcClk0M2cCmos_ik          (FmcClk0M2cCmos_ik),
    .FmcClk1M2cCmos_ik          (FmcClk1M2cCmos_ik),
    .FmcClk2Bidir_iok           (FmcClk2Bidir_iok),
    .FmcClk3Bidir_iok           (FmcClk3Bidir_iok),
    .FmcGbtClk0M2cLeft_ik       (FmcGbtClk0M2cLeft_ik),
    .FmcGbtClk1M2cLeft_ik       (FmcGbtClk1M2cLeft_ik),
    .FmcGbtClk0M2cRight_ik      (FmcGbtClk0M2cRight_ik),
    .FmcGbtClk1M2cRight_ik      (FmcGbtClk1M2cRight_ik),
    .FmcLaP_iob34               ({FmcLaP33_io,FmcLaP32_io,FmcLaP31_io,FmcLaP30_io,FmcLaP29_io,FmcLaP28_io,FmcLaP27_io,FmcLaP26_io,FmcLaP25_io,FmcLaP24_io,FmcLaP23_io,FmcLaP22_io,FmcLaP21_io,FmcLaP20_io,FmcLaP19_io,FmcLaP18_io,FmcLaP17_io,FmcLaP16_io,FmcLaP15_io,FmcLaP14_io,FmcLaP13_io,FmcLaP12_io,FmcLaP11_io,FmcLaP10_io,FmcLaP9_io,FmcLaP8_io,FmcLaP7_io,FmcLaP6_io,FmcLaP5_io,FmcLaP4_io,FmcLaP3_io,FmcLaP2_io,FmcLaP1_io,FmcLaP0_io}),
    .FmcLaN_iob34               ({FmcLaN33_io,FmcLaN32_io,FmcLaN31_io,FmcLaN30_io,FmcLaN29_io,FmcLaN28_io,FmcLaN27_io,FmcLaN26_io,FmcLaN25_io,FmcLaN24_io,FmcLaN23_io,FmcLaN22_io,FmcLaN21_io,FmcLaN20_io,FmcLaN19_io,FmcLaN18_io,FmcLaN17_io,FmcLaN16_io,FmcLaN15_io,FmcLaN14_io,FmcLaN13_io,FmcLaN12_io,FmcLaN11_io,FmcLaN10_io,FmcLaN9_io,FmcLaN8_io,FmcLaN7_io,FmcLaN6_io,FmcLaN5_io,FmcLaN4_io,FmcLaN3_io,FmcLaN2_io,FmcLaN1_io,FmcLaN0_io}),
    .FmcHaP_iob24               ({FmcHaP23_io,FmcHaP22_io,FmcHaP21_io,FmcHaP20_io,FmcHaP19_io,FmcHaP18_io,FmcHaP17_io,FmcHaP16_io,FmcHaP15_io,FmcHaP14_io,FmcHaP13_io,FmcHaP12_io,FmcHaP11_io,FmcHaP10_io,FmcHaP9_io,FmcHaP8_io,FmcHaP7_io,FmcHaP6_io,FmcHaP5_io,FmcHaP4_io,FmcHaP3_io,FmcHaP2_io,FmcHaP1_io,FmcHaP0_io}),
    .FmcHaN_iob24               ({FmcHaN23_io,FmcHaN22_io,FmcHaN21_io,FmcHaN20_io,FmcHaN19_io,FmcHaN18_io,FmcHaN17_io,FmcHaN16_io,FmcHaN15_io,FmcHaN14_io,FmcHaN13_io,FmcHaN12_io,FmcHaN11_io,FmcHaN10_io,FmcHaN9_io,FmcHaN8_io,FmcHaN7_io,FmcHaN6_io,FmcHaN5_io,FmcHaN4_io,FmcHaN3_io,FmcHaN2_io,FmcHaN1_io,FmcHaN0_io}),
    .FmcHbP_iob22               ({FmcHbP21_io,FmcHbP20_io,FmcHbP19_io,FmcHbP18_io,FmcHbP17_io,FmcHbP16_io,FmcHbP15_io,FmcHbP14_io,FmcHbP13_io,FmcHbP12_io,FmcHbP11_io,FmcHbP10_io,FmcHbP9_io,FmcHbP8_io,FmcHbP7_io,FmcHbP6_io,FmcHbP5_io,FmcHbP4_io,FmcHbP3_io,FmcHbP2_io,FmcHbP1_io,FmcHbP0_io}),
    .FmcHbN_iob22               ({FmcHbN21_io,FmcHbN20_io,FmcHbN19_io,FmcHbN18_io,FmcHbN17_io,FmcHbN16_io,FmcHbN15_io,FmcHbN14_io,FmcHbN13_io,FmcHbN12_io,FmcHbN11_io,FmcHbN10_io,FmcHbN9_io,FmcHbN8_io,FmcHbN7_io,FmcHbN6_io,FmcHbN5_io,FmcHbN4_io,FmcHbN3_io,FmcHbN2_io,FmcHbN1_io,FmcHbN0_io}),
    .FmcDpC2m_ob10              ({FmcDpC2m9_o,FmcDpC2m8_o,FmcDpC2m7_o,FmcDpC2m6_o,FmcDpC2m5_o,FmcDpC2m4_o,FmcDpC2m3_o,FmcDpC2m2_o,FmcDpC2m1_o,FmcDpC2m0_o}),
    .FmcDpM2c_ib10              ({FmcDpM2c9_i,FmcDpM2c8_i,FmcDpM2c7_i,FmcDpM2c6_i,FmcDpM2c5_i,FmcDpM2c4_i,FmcDpM2c3_i,FmcDpM2c2_i,FmcDpM2c1_i,FmcDpM2c0_i}),
    .OeSi57x_oe                 (OeSi57x_oe),
    .Si57xClk_ik                (Si57xClk_ik),
    .ClkFb_ikb4                 (ClkFb_ikb4),
    .ClkFb_okb4                 (ClkFbOutput_kb4),
    .Clk20VCOx_ik               (Clk20VCOx_ik),
    .PllDac20Sync_o             (PllDac20Sync_o),
    .PllDac25Sync_o             (PllDac25Sync_o),
    .PllDacSclk_ok              (PllDacSclk_ok),
    .PllDacDin_o                (PllDacDin_o),
    .PllRefSda_ioz              (PllRefSda_ioz),
    .PllRefScl_iokz             (PllRefScl_iokz),
    .PllRefInt_i                (PllRefInt_i),
    .PllSourceMuxOut_ok         (PllSourceMuxOut_ok),
    .PllRefClkOut_ik            (PllRefClkOut_ik),
    .GbitTrxClkRefR_ik          (GbitTrxClkRefR_ik),
    .DipSw_ib8                  (DipSw_ib8), //Comment: Contains also the part formely used as No GA
    .P2DataP_iob20              (P2DataP_iob20),
    .P2DataN_iob20              (P2DataN_iob20),
    .P0HwHighByte_ib8           (P0HwHighByte_ib8),
    .P0HwLowByte_ib8            (P0HwLowByte_ib8),
    .DaisyChain1Cntrl_o         (DaisyChain1Cntrl_o),
    .DaisyChain2Cntrl_o         (DaisyChain2Cntrl_o),
    .VmeP0BunchClk_ik           (VmeP0BunchClk_ik),
    .VmeP0Tclk_ik               (VmeP0Tclk_ik),
    .GpIo_iob4                  (GpIo_iob4),
    .PushButtonN_in             (PushButtonN_in),
    .WrPromSda_io               (WrPromSda_io),
    .WrPromScl_ok               (WrPromScl_ok),
    // Application <-> System Interface:
    .Sys125MhzClk_ik            (Sys125MhzClk_k),
    .AppVersion_ob8             (AppVersion_b8),
    .AppReleaseDay_ob8          (AppReleaseDay_b8),
    .AppReleaseMonth_ob8        (AppReleaseMonth_b8),
    .AppReleaseYear_ob8         (AppReleaseYear_b8),
    .AppInfo_ob352              (AppInfo_b352),
    .AppReset_iqr               (AppReset_r),
    .WbMasterCyc_i              (Wb1SysToAppCyc),
    .WbMasterStb_i              (Wb1SysToAppStb),
    .WbMasterAdr_ib25           (Wb1SysToAppAdr_b25),
    .WbMasterWr_i               (Wb1SysToAppWr),
    .WbMasterDat_ib32           (Wb1SysToAppDat_b32),
    .WbMasterDat_ob32           (Wb1AppToSysDat_b32),
    .WbMasterAck_o              (Wb1AppToSysAck),
    .WbSlaveCyc_o               (Wb2AppToSysCyc),
    .WbSlaveStb_o               (Wb2AppToSysStb),
    .WbSlaveAdr_ob25            (Wb2AppToSysAdr_b25),
    .WbSlaveWr_o                (Wb2AppToSysWr),
    .WbSlaveDat_ob32            (Wb2AppToSysDat_b32),
    .WbSlaveDat_ib32            (Wb2SysToAppDat_b32),
    .WbSlaveAck_i               (Wb2SysToAppAck),
    .Led_ob8                    (Led_b8),
    .BstOn_i                    (BstOn),
    .BstClk_ik                  (BstClk_k),
    .BunchClkFlag_i             (BunchClkFlag),
    .TurnClkFlag_i              (TurnClkFlag),
    .TurnClkFlagDly_ob12        (TurnClkFlagDly_b12),
    .BstByteAddress_ib8         (BstByteAddress_b8),
    .BstByte_ib8                (BstByte_b8),
    .BstByteStrobe_i            (BstByteStrobe),
    .BstByteError_i             (BstByteError),
    .InterruptRequest_ob24      (InterruptRequest_b24),
    .BlmIn_ib8                  (BlmIn_b8),
    .GpIo1DirOut_o              (GpIo1DirOut),
    .GpIo1DirOutStatus_i        (GpIo1DirOutStatus),
    .GpIo2DirOut_o              (GpIo2DirOut),
    .GpIo2DirOutStatus_i        (GpIo2DirOutStatus),
    .GpIo34DirOut_o             (GpIo34DirOut),
    .GpIo34DirOutStatus_i       (GpIo34DirOutStatus),
    .GpIo1EnTerm_o              (GpIo1EnTerm),
    .GpIo1EnTermStatus_i        (GpIo1EnTermStatus),
    .GpIo2EnTerm_o              (GpIo2EnTerm),
    .GpIo2EnTermStatus_i        (GpIo2EnTermStatus),
    .GpIo3EnTerm_o              (GpIo3EnTerm),
    .GpIo3EnTermStatus_i        (GpIo3EnTermStatus),
    .GpIo4EnTerm_o              (GpIo4EnTerm),
    .GpIo4EnTermStatus_i        (GpIo4EnTermStatus),
    .AppSfp1Present_iq          (AppSfpPresent_b4[1]),
    .AppSfp1TxFault_iq          (AppSfpTxFault_b4[1]),
    .AppSfp1Los_iq              (AppSfpLos_b4[1]),
    .AppSfp1TxDisable_o         (AppSfpTxDisable_b4[1]),
    .AppSfp1RateSelect_o        (AppSfpRateSelect_b4[1]),
    .StatusAppSfp1TxDisable_iq  (StatusAppSfpTxDisable_b4[1]),
    .StatusAppSfp1RateSelect_iq (StatusAppSfpRateSelect_b4[1]),
    .AppSfp2Present_iq          (AppSfpPresent_b4[2]),
    .AppSfp2TxFault_iq          (AppSfpTxFault_b4[2]),
    .AppSfp2Los_iq              (AppSfpLos_b4[2]),
    .AppSfp2TxDisable_o         (AppSfpTxDisable_b4[2]),
    .AppSfp2RateSelect_o        (AppSfpRateSelect_b4[2]),
    .StatusAppSfp2TxDisable_iq  (StatusAppSfpTxDisable_b4[2]),
    .StatusAppSfp2RateSelect_iq (StatusAppSfpRateSelect_b4[2]),
    .AppSfp3Present_iq          (AppSfpPresent_b4[3]),
    .AppSfp3TxFault_iq          (AppSfpTxFault_b4[3]),
    .AppSfp3Los_iq              (AppSfpLos_b4[3]),
    .AppSfp3TxDisable_o         (AppSfpTxDisable_b4[3]),
    .AppSfp3RateSelect_o        (AppSfpRateSelect_b4[3]),
    .StatusAppSfp3TxDisable_iq  (StatusAppSfpTxDisable_b4[3]),
    .StatusAppSfp3RateSelect_iq (StatusAppSfpRateSelect_b4[3]),
    .AppSfp4Present_iq          (AppSfpPresent_b4[4]),
    .AppSfp4TxFault_iq          (AppSfpTxFault_b4[4]),
    .AppSfp4Los_iq              (AppSfpLos_b4[4]),
    .AppSfp4TxDisable_o         (AppSfpTxDisable_b4[4]),
    .AppSfp4RateSelect_o        (AppSfpRateSelect_b4[4]),
    .StatusAppSfp4TxDisable_iq  (StatusAppSfpTxDisable_b4[4]),
    .StatusAppSfp4RateSelect_iq (StatusAppSfpRateSelect_b4[4]),
    .BstSfpPresent_iq           (BstSfpPresent),
    .BstSfpTxFault_iq           (BstSfpTxFault),
    .BstSfpLos_iq               (BstSfpLos),
    .BstSfpTxDisable_o          (BstSfpTxDisable),
    .BstSfpRateSelect_o         (BstSfpRateSelect),
    .StatusBstSfpTxDisable_iq   (StatusBstSfpTxDisable),
    .StatusBstSfpRateSelect_iq  (StatusBstSfpRateSelect),
    .EthSfpPresent_iq           (EthSfpPresent),
    .EthSfpTxFault_iq           (EthSfpTxFault),
    .EthSfpLos_iq               (EthSfpLos),
    .EthSfpTxDisable_o          (EthSfpTxDisable),
    .EthSfpRateSelect_o         (EthSfpRateSelect),
    .StatusEthSfpTxDisable_iq   (StatusEthSfpTxDisable),
    .StatusEthSfpRateSelect_iq  (StatusEthSfpRateSelect),
    .CdrLos_iq                  (CdrLos),
    .CdrLol_iq                  (CdrLol),
    .I2cWbCyc_o                 (I2cWbCyc),
    .I2cWbStb_o                 (I2cWbStb),
    .I2cWbWe_o                  (I2cWbWe),
    .I2cWbAdr_ob12              (I2cWbAdr_b12),
    .I2cWbDat_ob8               (I2cWbDatMoSi_b8),
    .I2cWbDat_ib8               (I2cWbDatMiSo_b8),
    .I2cWbAck_i                 (I2cWbAck),
    .OctSeriesControl_ib16      (OctSeriesControl_b16),
    .OctParallelControl_ib16    (OctParallelControl_b16)
);

endmodule
